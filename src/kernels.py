import torch.nn as nn
import torch

class GaussianKernel(nn.Module):
    def __init__(self, dim, bias=False):
        super().__init__()
        self.use_bias = bias
        if bias:
            self.bias = 2*torch.ones(1,dim).cuda() # nn.Parameter(torch.randn(1,dim))
        else:
            self.bias = torch.zeros(1, dim)
        self.log_sigma2 = nn.Parameter(torch.zeros(1))
    
    def forward(self, x, y):
        dist2 = torch.sum((x-y-self.bias)**2, dim=1)
        # return torch.exp(-.5*dist2/torch.exp(self.log_sigma2))
        return torch.exp(-.5*dist2)

class StudentKernel(nn.Module):
    def __init__(self, dim, bias=False):
        super().__init__()
        self.use_bias = bias
        if bias:
            self.bias = nn.Parameter(torch.randn(1,dim))
        else:
            self.bias = torch.zeros(1, dim)
        self.log_shifted_nu = nn.Parameter(0)

    def forward(self, x, y):
        nu = 1 + torch.exp(self.log_shifted_nu)
        dist2 = torch.sum((x-y-self.bias)**2)
        return torch.exp(-(nu+1)/2*torch.log(1+dist2/nu))