from torch.utils.data import Dataset, IterableDataset
import random

class EdgePairWithCommonNodeNoNegative(Dataset):
    def __init__(self, graph, node_mapping=None):
        self.graph = graph
        if node_mapping is None:
            node_mapping = {'id_to_key':{i:i for i in range(graph.order())},'key_to_id':{i:i for i in range(graph.order())}}
        self.node_mapping = node_mapping

    def __len__(self):
        return len(self.graph)
    
    def __getitem__(self, i):
        anchor_key = self.node_mapping['id_to_key'][i]
        # select edges with different synaptic weight in the undirected graph
        adjacent_edges = [e for e in self.graph.edges if anchor_key in e]
        w1, w2 = 0, 0
        e1, e2 = None, None
        while w1 == w2:
            e1, e2 = random.sample(adjacent_edges, 2)
            w1 = self.graph.get_edge_data(*e1)['weight']
            w2 = self.graph.get_edge_data(*e2)['weight']
        # recover the corresponding edges in the directed graph
        positive_edge, negative_edge = (e1, e2) if w1 > w2 else (e2, e1)
        positive_edge = (self.node_mapping['key_to_id'][positive_edge[0]], self.node_mapping['key_to_id'][positive_edge[1]])
        negative_edge = (self.node_mapping['key_to_id'][negative_edge[0]], self.node_mapping['key_to_id'][negative_edge[1]])

        return positive_edge, negative_edge
    
class EdgePairWithCommonNodeDataset(Dataset):
    def __init__(self, graph, node_mapping=None):
        self.graph = graph
        if node_mapping is None:
            node_mapping = {'id_to_key':{i:i for i in range(graph.order())},'key_to_id':{i:i for i in range(graph.order())}}
        self.node_mapping = node_mapping
        self.node_list = list(self.graph.nodes())

    def __len__(self):
        return len(self.graph)
    
    def __getitem__(self, i):
        anchor_key = self.node_mapping['id_to_key'][i]
        target_key = random.choice(self.node_list)
        e1 = random.choice([(anchor_key, target_key), (target_key, anchor_key)])
        if self.graph.has_edge(*e1):
            w1 = self.graph.get_edge_data(*e1)['weight']
            w2 = w1
            while w2 == w1:
                t2 = random.choice(self.node_list)
                e2 = random.choice([(anchor_key, t2), (target_key, t2)])
                w2 = self.graph.get_edge_data(*e2)['weight'] if self.graph.has_edge(*e2) else 0
        else:
            w1 = 0
            adjacent_edges = [e for e in self.graph.edges if anchor_key in e]
            e2 = tuple(random.choice(adjacent_edges))
            w2 = self.graph.get_edge_data(*e2)['weight']
        # recover the corresponding edges in the directed graph
        positive_edge, negative_edge = (e1, e2) if w1 > w2 else (e2, e1)
        positive_edge = (self.node_mapping['key_to_id'][positive_edge[0]], self.node_mapping['key_to_id'][positive_edge[1]])
        negative_edge = (self.node_mapping['key_to_id'][negative_edge[0]], self.node_mapping['key_to_id'][negative_edge[1]])

        return positive_edge, negative_edge
    
class EdgePairWithCommonNodeDatasetFull(Dataset):
    def __init__(self, graph, node_mapping=None):
        self.graph = graph
        if node_mapping is None:
            node_mapping = {'id_to_key':{i:i for i in range(graph.order())},'key_to_id':{i:i for i in range(graph.order())}}
        self.node_mapping = node_mapping
        self.node_list = list(self.graph.nodes())
        import itertools
        self.indices = []
        for u, v, w in itertools.permutations(self.graph.nodes(), 3):
            e1 = (u, v)
            e2 = (u, w)
            w1 = self.graph.get_edge_data(*e1)['weight'] if self.graph.has_edge(*e1) else 0
            w2 = self.graph.get_edge_data(*e2)['weight'] if self.graph.has_edge(*e2) else 0
            if w1 != w2:
                self.indices.append((u,v,w))
        print(len(self))

    def __len__(self):
        return len(self.indices)
    
    def __getitem__(self, i):
        u, v, w = self.indices[i]
        e1 = (u, v)
        e2 = (u, w)
        w1 = self.graph.get_edge_data(*e1)['weight'] if self.graph.has_edge(*e1) else 0
        w2 = self.graph.get_edge_data(*e2)['weight'] if self.graph.has_edge(*e2) else 0
        
        # recover the corresponding edges in the directed graph
        positive_edge, negative_edge = (e1, e2) if w1 > w2 else (e2, e1)
        positive_edge = (self.node_mapping['key_to_id'][positive_edge[0]], self.node_mapping['key_to_id'][positive_edge[1]])
        negative_edge = (self.node_mapping['key_to_id'][negative_edge[0]], self.node_mapping['key_to_id'][negative_edge[1]])

        return positive_edge, negative_edge
    
class EdgePairDataset(IterableDataset):
    def __init__(self, graph, node_mapping=None):
        self.graph = graph
        if node_mapping is None:
            node_mapping = {'id_to_key':{i:i for i in range(graph.order())},'key_to_id':{i:i for i in range(graph.order())}}
        self.node_mapping = node_mapping
        self.node_list = list(self.graph.nodes())

    def __len__(self):
        return len(self.graph)
    
    def __iter__(self):
        return self
    
    def __next__(self):
        e1 = random.sample(self.node_list)
        if self.graph.has_edge(*e1):
            w1 = self.graph.get_edge_data(*e1)['weight']
            w2 = w1
            while w2 == w1:
                e2 = random.sample(self.node_list)
                w2 = self.graph.get_edge_data(*e2)['weight'] if self.graph.has_edge(*e2) else 0
        else:
            w1 = 0
            e2 = random.choice(self.graphe.edges)
            w2 = self.graph.get_edge_data(*e2)['weight']
        # recover the corresponding edges in the directed graph
        positive_edge, negative_edge = (e1, e2) if w1 > w2 else (e2, e1)
        positive_edge = (self.node_mapping['key_to_id'][positive_edge[0]], self.node_mapping['key_to_id'][positive_edge[1]])
        negative_edge = (self.node_mapping['key_to_id'][negative_edge[0]], self.node_mapping['key_to_id'][negative_edge[1]])

        return positive_edge, negative_edge
    
class BinaryEdgeDataset(Dataset):
    def __init__(self, graph, node_mapping=None):
        self.graph = graph
        if node_mapping is None:
            node_mapping = {'id_to_key':{i:i for i in range(graph.order())},'key_to_id':{i:i for i in range(graph.order())}}
        self.node_mapping = node_mapping

    def __len__(self):
        return len(self.graph)
    
    def __getitem__(self, i):
        j = random.choice(range(self.graph.order()))
        anchor_key, target_key = self.node_mapping['id_to_key'][i], self.node_mapping['id_to_key'][j]
        return (i,j), (1 if self.graph.has_edge(anchor_key,target_key) else 0)

class EdgeDataset(Dataset):
    def __init__(self, graph, node_mapping=None):
        self.graph = graph
        if node_mapping is None:
            node_mapping = {'id_to_key':{i:i for i in range(graph.order())},'key_to_id':{i:i for i in range(graph.order())}}
        self.node_mapping = node_mapping

    def __len__(self):
        return len(self.graph)
    
    def __getitem__(self, i):
        j = random.choice(range(self.graph.order()))
        anchor_key, target_key = self.node_mapping['id_to_key'][i], self.node_mapping['id_to_key'][j]
        return (i,j), (self.graph.get_edge_data(anchor_key, target_key)['weight'] if self.graph.has_edge(anchor_key,target_key) else 0)
        
# if __name__ == '__main__':
#     from torch.utils.data import DataLoader
#     from synthetic_graphs import *

#     N, M = 15, 12
#     graph, _ = synfire_chain(N, M)
#     dataset = EdgeTripletDataset(graph)
#     dataloader = DataLoader(dataset, shuffle=False, batch_size=N*M)

#     for _ in dataloader:
#         pass
        
