import torch.nn as nn
import torch.nn.functional as F
import torch
import numpy as np

class Spline(nn.Module):
    def __init__(self, n_points, vals=None):
        super().__init__()
        self.n_points = n_points
        self.control_points = np.linspace(0,1,n_points)
        self.boundary_points = np.zeros(n_points-1)
        for i in range(n_points-1):
            self.boundary_points[i] = self.control_points[i+(i>0)] if (i == 0 or i == n_points-2) else .5*(self.control_points[i]+self.control_points[i+1])
        self.control_points = torch.Tensor(self.control_points)
        self.boundary_points = torch.Tensor(self.boundary_points)     
        self.compute_matrix()                                                                      
        self.log_diff_values = nn.Parameter(torch.log(torch.diff(vals if vals is not None else self.boundary_points)))
    
    def compute_matrix(self):
        n = self.n_points
        cp = self.control_points
        cp2 = cp**2
        bp = self.boundary_points
        bp2 = bp**2

        M = torch.zeros((3*(n-2), 3*(n-2)))
        M[0,:3] = torch.Tensor([1,cp[0], cp2[0]])
        M[n-1,-3:] = torch.Tensor([1,cp[-1], cp2[-1]])
        for k in range(1,n-1):
            M[k,3*(k-1):3*k] = torch.Tensor([1, cp[k], cp2[k]])
        for k in range(n-3):
            M[n+k,3*k:3*(k+1)] = torch.Tensor([1, bp[k+1], bp2[k+1]])
            M[n+k,3*(k+1):3*(k+2)] = torch.Tensor([-1, -bp[k+1], -bp2[k+1]])
        for k in range(n-3):
            M[2*n-3+k,3*k+1:3*k+3] = torch.Tensor([1,2*bp[k+1]])
            M[2*n-3+k,3*(k+1)+1:3*(k+1)+3] = torch.Tensor([-1,-2*bp[k+1]])

        self.matrix = M.inverse()[:,:n]

    def forward(self, x):
        i = torch.clip(torch.searchsorted(self.boundary_points, x, right=True)-1, max=self.n_points-3) # clip deals with the case where x = 1
        i = torch.concatenate((3*i,3*i+1,3*i+2), dim=1)
        val = torch.cumsum(F.pad(torch.softmax(self.log_diff_values, dim=0), (1,0)), dim=0)
        coeffs = self.matrix.matmul(val).reshape(1,-1).repeat((x.shape[0],1))
        coeffs = torch.gather(coeffs, 1 ,i)
        powx = torch.concatenate((torch.ones(x.shape[0], 1), x, x**2), dim=1)

        return torch.einsum('bi, bi -> b', coeffs, powx)
    

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    th = torch.linspace(0, np.pi/2, 4)
    vals = np.sin(th)**2
    spline = Spline(4, vals)
    x = torch.linspace(0,1,100).reshape(-1,1)
    plt.figure()
    plt.plot(2/np.pi*th, vals, 'or')
    plt.plot(x, spline(x).detach())
    plt.show()

    # test of gradient descent
    for i in range(201):
        print(i)
        spline(x).sum().backward()
        with torch.no_grad():
            spline.log_diff_values -= 0.1*spline.log_diff_values.grad
        if not(i%100):
            plt.figure()
            plt.plot(2/np.pi*th, vals, 'or')
            plt.plot(x, spline(x).detach())
            plt.plot(spline.control_points[1:].detach(), torch.cumsum(F.softmax(spline.log_diff_values, dim=0), dim=0).detach())
            plt.show()
        spline.log_diff_values.grad.zero_()

    
            
