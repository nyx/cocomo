import os
from torch.utils.data import DataLoader
import torch
from torch.optim import SGD, Adam, LBFGS
from synthetic_graphs import *
from dataloaders import *
from single_space_model import Model
from kernels import GaussianKernel
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from torch.utils.tensorboard import SummaryWriter

device = 'cuda' if torch.cuda.is_available() else 'cpu'

N, M = 15, 12
graph, _ = synfire_chain(N, M)
dataset = EdgePairWithCommonNodeDataset(graph)
dataloader = DataLoader(dataset, shuffle=False, batch_size=len(dataset))

model = Model(graph.order(), 3, GaussianKernel(3, bias=True), activation=torch.sigmoid)
model.init_embedding(graph)
model.to(device)
optim = Adam(model.parameters())
# optim = LBFGS(model.parameters(), lr=1, line_search_fn='strong_wolfe')
n_epochs = 100000

writer = SummaryWriter(comment='EXP001')
slow_interval = n_epochs//20
fast_interval = n_epochs//1000
loss_history = []
bias_history = []
embedding_snap = []
def fast_logging(epoch):
    bias_history.append(model.kernel.bias.detach().cpu().numpy().flatten())
    embedding_snap.append(model.x.weight.cpu().detach().numpy())
def slow_logging(epoch):
    with torch.no_grad():
        dot_product_matrix = torch.zeros((len(graph), len(graph)))
        for i in torch.arange(len(graph), dtype=int).reshape(-1,1):
            dot_product_matrix[i.item(),:] = model.apply_kernel(i.repeat(len(graph)), torch.arange(len(graph))).cpu()

    writer.add_image('kernel_product', dot_product_matrix, epoch, dataformats='HW')
    bias = model.kernel.bias.detach().cpu().numpy().flatten()
    embedding = model.x.weight.cpu().detach().numpy()
    fig = plt.figure()
    plt.scatter(embedding[:,0], embedding[:,1], c=[i//M for i in range(len(graph))], cmap='Set1')
    plt.arrow(0,0,bias[0], bias[1])
    plt.axis('equal')
    writer.add_figure('embedding', fig, epoch)
    plt.close()

def closure(e1, e2): # for LBFGS
    def f():
        optim.zero_grad()
        loss = -model.triplet_loss(e1, e2).mean()
        loss.backward()
        return loss
    return f

try:
    for epoch in range(n_epochs):
        sum_loss = 0
        if not(epoch%100) and epoch:
            print(f'epoch {epoch}/{n_epochs}')
        for e1, e2 in dataloader:
            optim.step(closure=closure(e1,e2))
            # model.center_rescale_embedding()
            with torch.no_grad():
                sum_loss += -model.triplet_loss(e1, e2).mean().cpu().numpy()
        epoch_loss = sum_loss/len(dataloader)+1
        writer.add_scalar('loss', epoch_loss, epoch)

        if not(epoch%fast_interval) or epoch == n_epochs-1:
            fast_logging(epoch)
        if not(epoch%slow_interval) or epoch == n_epochs-1:
            slow_logging(epoch)
except KeyboardInterrupt:
    print("Final logging. Interrupt again to skip.")
    fast_logging(epoch)
    slow_logging(epoch)

print("Final logging.")
fig = plt.figure()
pointcloud = plt.scatter(embedding_snap[0][:,0], embedding_snap[0][:,1], c=[i//12 for i in range(len(graph))])
arrow = plt.arrow(0, 0, 0, 0)
maxs = 1.05*np.max(np.concatenate(embedding_snap, axis=0), axis=0)
mins = 1.05*np.min(np.concatenate(embedding_snap, axis=0), axis=0)
plt.xlim(mins[0], maxs[0])
plt.ylim(mins[1], maxs[1])

def anim(i):
    pointcloud.set_offsets(embedding_snap[i])
    arrow.set_data(dx=bias_history[i][0], dy=bias_history[i][1])
    return pointcloud, arrow

ani = FuncAnimation(fig, anim, len(embedding_snap), interval=50, repeat=True)
ani.save(os.path.join(writer.log_dir, 'latent_space_animation.mp4'))
plt.close()
writer.close()