import numpy as np
from scipy.spatial.distance import cdist
import sgd
import theano
import theano.tensor as T
import topogen
import sklearn.datasets
import util
import losses
import kernels
import cPickle as pickle
import exputil
import time
import pandas as pd
import seaborn as sns
import os
import sklearn.metrics
sns.set_style('whitegrid')
from ruffus import * 

OUTDIR = "results"

td = lambda x : os.path.join(OUTDIR, x)

def data_name_to_file(dataname):
    return "data/%s.cleandata.pickle" % dataname

def create_exp(expname, datanames):
    for dataname in datanames:
        
        infile = data_name_to_file(dataname)
        outfile = td("%s.%s.results.pickle" % (expname, dataname))
        
        yield infile, outfile, expname, dataname


def basic_gauss_mouse_params():
    for g in create_exp("basic_gauss_mouse", 
                               ["retina"]):
        yield g

@files(basic_gauss_mouse_params)
def basic_gauss_mouse(infile, outfile, expname, dataname):
    
    filebase = td("%s.%s" % (expname, dataname))
    data = pickle.load(open(infile, 'r'))
    data.keys()
    connection_matrix = (data['synapse_con_mat'] > 0).astype(float)

    obs = connection_matrix
    lossobj = losses.Logistic(0.5)
    
    ITERS = 1000
    allres = []

    for gauss_sigma in [1.0, 2.0, 4.0]:

        kern_func = kernels.gaussian(gauss_sigma) # , 1.0) # , 3.0)

        kfold_cv = 10

        N = len(obs)
        XINIT_N = 3
        for D in [2, 4, 8, 16]:
            for xinit in np.random.normal(0, 1, (XINIT_N, N, D)):
                theta_init = np.ones(D)
                t1 = time.time()
                res = exputil.run_cv_exp_bfgs(kern_func, lossobj.cost(), 
                                              xinit, theta_init, obs, 
                                              lossobj.obs_forward, kfold_cv, 
                                              iters=ITERS, theta_inference=False, 
                                              verbose=False, verbose_iter=10)
                duration = time.time() - t1


                exp_index = len(allres)
                res_filename = filebase + ".%04d.pickle" % exp_index
                pickle.dump(res, open(res_filename, 'w'))
                
                allres.append({'xinit' : xinit, 
                               'D' : D, 
                               'time' : duration, 
                               'gauss_sigma' : gauss_sigma})
    pickle.dump(allres, open(outfile, 'w'))



def basic_gauss_synthetic_params():
    for g in create_exp("basic_gauss_synth", 
                        ["block_sym", "block_asym", "latentspace_ring_2d"]):
        yield g

@jobs_limit(2)
@files(basic_gauss_synthetic_params)
def basic_gauss_synthetic(infile, outfile, expname, dataname):
    
    filebase = td("%s.%s" % (expname, dataname))
    data = pickle.load(open(infile, 'r'))
    data.keys()
    connection_matrix = (data['synapse_con_mat'] > 0).astype(float)

    obs = connection_matrix
    lossobj = losses.Logistic(0.5)
    
    ITERS = 1000
    allres = []

    for gauss_sigma in [1.0, 2.0, 4.0]:

        kern_func = kernels.gaussian(gauss_sigma) # , 1.0) # , 3.0)

        kfold_cv = 10

        N = len(obs)
        XINIT_N = 3
        for D in [2, 4, 8, 16]:
            for xinit in np.random.normal(0, 1, (XINIT_N, N, D)):
                theta_init = np.ones(D)
                t1 = time.time()
                res = exputil.run_cv_exp_bfgs(kern_func, lossobj.cost(), 
                                              xinit, theta_init, obs, 
                                              lossobj.obs_forward, kfold_cv, 
                                              iters=ITERS, theta_inference=False, 
                                              verbose=False, verbose_iter=10)
                duration = time.time() - t1


                exp_index = len(allres)
                res_filename = filebase + ".%04d.pickle" % exp_index
                pickle.dump(res, open(res_filename, 'w'))
                
                allres.append({'xinit' : xinit, 
                               'D' : D, 
                               'time' : duration, 
                               'gauss_sigma' : gauss_sigma})
    pickle.dump(allres, open(outfile, 'w'))



def basic_gaussos_mouse_params():
    for g in create_exp("basic_gaussos_mouse", 
                               ["retina"]):
        yield g

@files(basic_gaussos_mouse_params)
def basic_gaussos_mouse(infile, outfile, expname, dataname):
    
    filebase = td("%s.%s" % (expname, dataname))
    data = pickle.load(open(infile, 'r'))
    data.keys()
    connection_matrix = (data['synapse_con_mat'] > 0).astype(float)

    obs = connection_matrix
    lossobj = losses.Logistic(0.5)
    
    ITERS = 1000
    allres = []

    for gauss_sigma in [1.0, 2.0, 4.0]:

        kern_func = kernels.gauss_os(gauss_sigma) # , 1.0) # , 3.0)

        kfold_cv = 10

        N = len(obs)
        XINIT_N = 3
        for D in [2, 4, 8, 16]:
            for xinit in np.random.normal(0, 1, (XINIT_N, N, D)):
                theta_init = np.ones(D)*0.1
                t1 = time.time()
                res = exputil.run_cv_exp_bfgs(kern_func, lossobj.cost(), 
                                              xinit, theta_init, obs, 
                                              lossobj.obs_forward, kfold_cv, 
                                              iters=ITERS, theta_inference=True, 
                                              verbose=False, verbose_iter=10)
                duration = time.time() - t1


                exp_index = len(allres)
                res_filename = filebase + ".%04d.pickle" % exp_index
                pickle.dump(res, open(res_filename, 'w'))
                
                allres.append({'xinit' : xinit, 
                               'D' : D, 
                               'time' : duration, 
                               'gauss_sigma' : gauss_sigma})
    pickle.dump(allres, open(outfile, 'w'))



def basic_gauss_params():
    for g in create_exp("basic_gauss", 
                               ["retina", "medulla"]):
        yield g


@files(basic_gauss_params)
def basic_gauss(infile, outfile, expname, dataname):
    
    filebase = td("%s.%s" % (expname, dataname))
    data = pickle.load(open(infile, 'r'))
    data.keys()
    connection_matrix = (data['synapse_con_mat'] > 0).astype(float)

    obs = connection_matrix
    lossobj = losses.Logistic(0.5)
    
    ITERS = 1000
    allres = []

    for gauss_sigma in [2.0, 4.0]:

        kern_func = kernels.gaussian(gauss_sigma) # , 1.0) # , 3.0)

        kfold_cv = 10

        N = len(obs)
        XINIT_N = 3
        for D in [2, 4, 8, 16, 32, 48]:
            for xinit in np.random.normal(0, 1, (XINIT_N, N, D)):
                theta_init = np.ones(D)
                t1 = time.time()
                res = exputil.run_cv_exp_bfgs(kern_func, lossobj.cost(), 
                                              xinit, theta_init, obs, 
                                              lossobj.obs_forward, kfold_cv, 
                                              iters=ITERS, theta_inference=False, 
                                              verbose=False, verbose_iter=10)
                duration = time.time() - t1


                exp_index = len(allres)
                res_filename = filebase + ".%04d.pickle" % exp_index
                pickle.dump(res, open(res_filename, 'w'))
                
                allres.append({'xinit' : xinit, 
                               'D' : D, 
                               'time' : duration, 
                               'gauss_sigma' : gauss_sigma})
    pickle.dump(allres, open(outfile, 'w'))


@transform(td("*.pickle"), 
           regex("(.\d\d\d\d).pickle"),
           r"\1.summary.pickle")
def compute_stats(infile, outfile):

    print infile, outfile

    res = pickle.load(open(infile, 'r'))
    resdata = []
    xvals = []
    thetavals = []
    for cvi, r in enumerate(res):
        true_vals = r['true_vals']
        est_params = r['est_params']
        roc_auc = sklearn.metrics.roc_auc_score(true_vals, est_params)
        pr_auc = sklearn.metrics.average_precision_score(true_vals, est_params)

        xvals.append(r['x_est'])
        thetavals.append(r.get('theta_est', 'None'))
        #thetavals.append(r['theta_est'])
        resdata.append({'cvi' : cvi, 
                        'roc_auc' : roc_auc, 
                        'pr_auc' : pr_auc}) 
    df = pd.DataFrame(resdata)
    df['filename'] = infile
    pickle.dump({'df' : df, 'xvals' : xvals, 'thetavals' : thetavals},
                open(outfile, 'w'), -1)

if __name__ == "__main__":
    pipeline_run([#basic_gauss_mouse, basic_gaussos_mouse, 
        #basic_gauss, 
        basic_gauss_synthetic, 
        compute_stats], multiprocess=16)
