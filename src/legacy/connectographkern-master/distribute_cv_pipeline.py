"""

export DEFAULT_RUFFUS_HISTORY_FILE=~/.ruffus_history.sqlite



"""

from ruffus import * 
import os
import pandas as pd
import copy
import cPickle as pickle
import numpy as np
import exputil

import irm
from irm import cvpipelineutil
from glob import glob

RUN_DIST = True # True
if RUN_DIST:
    from distributed import Executor

tdin = lambda x : os.path.join("data", x)
tdout = lambda x : os.path.join("dist.out", x)

EXPERIMENTS = {
    # 'bbnonconj_1000' : {'files' : ['KC-rosette-KC.MBON-14-A.20.0.cv_10.pickle', 
    #                                'KC-rosette-KC.MBON-14-B.20.0.cv_10.pickle', 
    #                                'KC-rosette-KC.MBON-07-A.20.0.cv_10.pickle', 
    #                                'KC-rosette-KC.MBON-07-B.20.0.cv_10.pickle', 
    #                                'KC-rosette-KC.MBON-18.20.0.cv_10.pickle'], 
    #                     'runner' : 'sbm',
    #                     'seeds' : range(10), 
    #                     'runner_config' : {'model' : 'bbnonconj', 
    #                                        'kernel_config' : 'slow_anneal_1000'}}, 
    # 'bbnonconj_mbons_th40_1000' : {'files' : ['KC-rosette-KC.MBON-14-A.20.0.cv_10.pickle', 
    #                                           'KC-rosette-KC.MBON-14-B.20.0.cv_10.pickle', 
    #                                           'KC-rosette-KC.MBON-07-A.20.0.cv_10.pickle', 
    #                                           'KC-rosette-KC.MBON-07-B.20.0.cv_10.pickle', 
    #                                           'KC-rosette-KC.MBON-18.20.0.cv_10.pickle'], 
    #                                'runner' : 'sbm',
    #                                'seeds' : range(10), 
    #                                'runner_config' : {'model' : 'bbnonconj', 
    #                                                   'kernel_config' : 'slow_anneal_1000'}}, 
    'bbconj_tensor_1000' : {'files' : ['KC-rosette-KC.main_mbon.20.0.cv_10.pickle'], 
                        'runner' : 'sbm',
                        'seeds' : range(10), 
                        'runner_config' : {'model' : 'bbconj', 
                                           'kernel_config' : 'slow_anneal_conj_1000'}}, 
    # : ran in 229 min on our 8-slave c4.8xlarge:
    'bbconj_tensor_subtypes_1000' : {'files' : ['KC-rosette-KC.main_mbon.20.0.cv_10.*.pickle'], 
                                     'runner' : 'sbm',
                                     'seeds' : range(10), 
                                     'runner_config' : {'model' : 'bbconj', 
                                                        'kernel_config' : 'slow_anneal_conj_1000'}}, 
    'bbnonconj_tensor_subtypes_1000' : {'files' : ['KC-rosette-KC.main_mbon.20.1.cv_10.*.pickle'], 
                                     'runner' : 'sbm',
                                     'seeds' : range(10), 
                                     'runner_config' : {'model' : 'bbnonconj', 
                                                        'kernel_config' : 'slow_anneal_1000'}}, 
    # 'bbnonconj_debug_10' : {'files' : ['KC-rosette-KC.MBON-14-A.20.0.cv_10.pickle'], 
    #                         'runner' : 'sbm',
    #                         'seeds' : range(1), 
    #                         'runner_config' : {'model' : 'bbnonconj', 
    #                                        'kernel_config' : 'debug_10'}}, 
    # 'bbnonconj_allcell_1000' : {'files' : ['KC-rosette-KC.*.cv_10.pickle'], 
    #                             'runner' : 'sbm',
    #                             'seeds' : range(10), 
    #                             'runner_config' : {'model' : 'bbnonconj', 
    #                                                'kernel_config' : 'slow_anneal_1000'}}, 
    # 'mf_lr_debug' : {'files' : ['KC-rosette-KC.MBON-14-A.cv_10.pickle'], 
    #                'runner' : 'mf',
    #                'seeds' : range(1, 30), 
    #                'runner_config' : {'model' : 'lr', 
    #                                 'method' : 'lsnmf',
    #                                   'model_args' : {}}}
}

# for mbon in ['MBON-14-A','MBON-14-B',
#              'MBON-07-A','MBON-07-B',
#              'MBON-18']:

#     infile = 'KC-rosette-KC.{}.40.0.cv_10.pickle'.format(mbon)

#     d = {'files' : [infile], 
#          'runner' : 'sbm',
#          'seeds' : range(10), 
#          'runner_config' : {'model' : 'bbnonconj', 
#                             'kernel_config' : 'slow_anneal_1000'}}
#     key = "bbnonconj_1000_th40_{}".format(mbon)
#     EXPERIMENTS[key] = d


# add all the NMF types:
nmf_types = {'lsnmf' : {'seed' : "nndsvd"}, 
             'pnmf' : {'seed' : "nndsvd"},
             'bmf' : {'seed' : 'nndsvd'},
             'icm' : {'seed' : 'nndsvd'},
             'nsnmf' : {'seed' : 'nndsvd'},
             'pmf' : {'seed' : 'nndsvd'},
             'snmf' : {'seed' : 'nndsvd'},
}

# for m, args in nmf_types.iteritems():
#     key = "mf_{}_allcell".format(m)
#     EXPERIMENTS[key] = {'files' : ['KC-rosette-KC.*.cv_10.pickle'], 
#                         'runner' : 'mf',
#                         'seeds' : range(1, 30), 
#                         'runner_config' : {'model' : 'nmf', 
#                                            'method' : m, 
#                                            'model_args' : args}}
    

def create_conj_kernel(iterations):
    k = irm.runner.default_kernel_anneal(iterations=iterations)

    k_replace = k[0][1]['subkernels'][0]
    assert k_replace[0] == 'nonconj_gibbs'
    k[0][1]['subkernels'][0] = irm.runner.default_kernel_config()[0]
    print "there are", len(k[0][1]['subkernels']), "subkernels"
    print k[0][1]['subkernels'][0]
    return k

    
kernel_configs = {'debug_10' : {'ITERS' : 10, 
                                'kernels' : irm.runner.default_kernel_anneal(iterations=7)},
                  'slow_anneal_1000' : {'ITERS' : 1000, 
                                        'cores' : 4, 
                                        'kernels' : irm.runner.default_kernel_anneal(iterations=700)},
                  
                  'slow_anneal_conj_1000' : {'ITERS' : 1000, 
                                             'cores' : 4, 
                                             'kernels' : create_conj_kernel(700)}

}


def sbm_runner(data_mat, cv, seed, runner_config):
    
    model = runner_config['model']
    kc_name = runner_config['kernel_config']

    np.random.seed(seed)
    print "smb_runner data_mat.shape=", data_mat.shape, "cv.shape=", cv.shape
    assert data_mat.shape == cv.shape
    assert cv.dtype == np.uint8

    print "there are", np.sum(data_mat.astype(float)), "ones" 
    
    RELATION_N = 1 if len(data_mat.shape) == 2 else data_mat.shape[0]
    NODES_N = data_mat.shape[1] 
    print "RUNNING WITH RELATION_N=", RELATION_N

    if model == 'bbnonconj' or model == 'bbconj':
        if model == 'bbnonconj':
            model_name = "BetaBernoulliNonConj"
        else:
            model_name = "BetaBernoulli"
        assert data_mat.dtype == np.uint8
        
        if RELATION_N == 1:
            init, data = irm.irmio.default_graph_init(data_mat, model_name)
        else:
            init, data = irm.irmio.default_graph_init(data_mat[0].copy(), 
                                                      model_name, 
                                                      extra_conn=data_mat[1:].copy())
            

        
        init['domains']['d1']['assignment'] = np.random.permutation(NODES_N) % 50 
        
        HPS = {'alpha' : 1.0, 'beta' : 1.0}
        for i in range(RELATION_N):
            init['relations']['R{:d}'.format(i+1)]['hps'] = HPS.copy()

    else:
        raise NotImplementedError("unknown model")

    if RELATION_N == 1:
        data['relations']['R{:d}'.format(i+1)]['observed'] = cv.copy()
    else:
        for i in range(RELATION_N):
            data['relations']['R{:d}'.format(i+1)]['observed'] = cv[i].copy()

    for k, v in data['relations'].iteritems():
        print " relation", k, "data.shape=", v['data'].shape, v['data'].dtype, v['observed'].shape, v['observed'].dtype

    # DEBUG 
    pickle.dump({'data' : data, 
                 'init' : init}, open('debug.pickle', 'w'), -1)

    kc = kernel_configs[kc_name]

    s = cvpipelineutil.run_exp_pure(data, init, kc_name, 
                                    seed, kc)

    res = s['res']

    return res




def mf_runner(data_mat, cv_observed, seed_rank, runner_config):
    """
    Matrix Factorization Runner
    """

    model = runner_config['model']
    model_args = runner_config['model_args']

    assert data_mat.shape == cv.shape
    assert cv.dtype == np.uint8

    print "there are", np.sum(data_mat.astype(float)), "ones" 

    cv_mask_flat = cv_observed.flatten()
    idx = np.argwhere(cv_mask_flat==0)
    conmat_float = conmat.astype(np.float32).copy()
    conmat_zeroed = conmat_float.copy()
    conmat_zeroed[cv_observed == 0] = 0
     
    if model == 'lr':
        u, s, v = np.linalg.svd(conmat_zeroed)
        est = np.zeros_like(conmat_zeroed)
        for k in range(seed_rank):
            est += s[k] * np.outer(u[:, k], v[k])
        other_data = {'u' : u, 's' : s, 'v' : v}
    elif model == 'nmf':
        
        nmf = nimfa.methods[model_args['method']](conmat_zeroed, rank=k+1, **model_args) # , prior=np.random.rand(10))
        nmf_fit = nmf()
        est = np.array(nmf_fit.fitted())
        other_data = {'coef' : np.array(nmf_fit.coef()), 
                      'basis' : np.array(nmf_fit.basis())}

    return {'model' : model, 
            'est' : est, 
            'other_data' : other_data}


def params():
    for exp_name, ec in EXPERIMENTS.iteritems():
        infiles = sum([glob(tdin(f)) for f in ec['files']], [])
        outfile = tdout('%s.results.pickle' % exp_name)
        yield infiles, outfile, exp_name, ec
        


@mkdir('dist.out')
@files(params)
def run_exp(infiles, outfile, exp_name, exp_config):
    # load infiles


    def run_exp((filename, data, cv_i, cv, seed)):
        if exp_config['runner'] == 'sbm':
            res = sbm_runner(data, cv, seed, exp_config['runner_config'])

            return (filename, cv_i, seed, res)

    print "loading data"
    datasets = [(f, pickle.load(open(f, 'r'))) for f in infiles]

    print "done", len(datasets)
    if RUN_DIST:
        executor = Executor('127.0.0.1:8786')
        print executor

    all_exp = []
    all_res = []
    for f, data in datasets:
        for cv_i, cv in enumerate(data['cvs']):
            for seed in exp_config['seeds']:
                print "submitting", f, cv_i, seed, data['conmat'].shape, cv.shape

                e = (f, data['conmat'].copy(), 
                     cv_i, cv.copy(), seed)
                #run_exp(e)
                if RUN_DIST:
                    r = executor.submit(run_exp, e)
                else:
                    r = run_exp(e)
                all_res.append(r)
                all_exp.append((f, cv_i, seed))
    print "Running experiment"

    if RUN_DIST:
        job_res = [e.result() for e in all_res]
    else:
        job_res = all_res
    
    
    pickle.dump({'infiles' : infiles, 
                 'job_res' : job_res, 
                 'all_exp' : all_exp, 
                 'exp_name' : exp_name}, 
                open(outfile, 'w'), -1)

if __name__ == "__main__":
    #pipeline_printout(target_tasks=[run_exp], checksum_level=0)
    pipeline_run(target_tasks=[run_exp], checksum_level=0)
