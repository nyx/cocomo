import simplejson
from ruffus import * 
import os
import pandas as pd
import copy
import cPickle as pickle
import numpy as np



DATA_DIR = "data"

def td(x):
    return os.path.join(DATA_DIR, x)

@mkdir(DATA_DIR)
@files("../data.original/medulla/annotations-body.json", 
       td("medulla.neurondf.pickle"))
def load_neurons(infile, outfile):

    neuron_json  = simplejson.load(open(infile))
    neurondf = pd.DataFrame(neuron_json['data'])

    neurondf['idx'] = np.arange(len(neurondf))

    pickle.dump({'neurondf' : neurondf}, 
                open(outfile, 'w'), -1)

                 

@files("../data.original/medulla/annotations-synapse.json", 
       td("medulla.synapsesdf.pickle"))
def load_synapses(infile, outfile):
    
    synapse_json  = simplejson.load(open(infile, 'r'))
    synapse_data = synapse_json['data']

    
    # turn this into two dataframes that can be joined
    tbars = []
    connections = []

    for tbar_i, tbar in enumerate(synapse_data):
        tbar_info = copy.deepcopy(tbar['T-bar'])

        tbar_info['id'] = int(tbar_i)
        tbar_info['loc_x'] = int(tbar_info['location'][0])
        tbar_info['loc_y'] = int(tbar_info['location'][1])
        tbar_info['loc_z'] = int(tbar_info['location'][2])
        del tbar_info['location']
        tbars.append(tbar_info)

        for p in tbar['partners']:
            p['loc_x'] = p['location'][0]
            p['loc_y'] = p['location'][1]
            p['loc_z'] = p['location'][2]
            p['tbar_id']= int(tbar_i)
            del p['location']
            connections.append(p)


    tbar_df = pd.DataFrame(tbars)
    connections_df = pd.DataFrame(connections)


    pickle.dump({'tbar_df' : tbar_df, 
                 'connections_df' : connections_df}, 
                open(outfile, 'w'), -1)

@files([load_neurons, load_synapses], td("medulla.cleandata.pickle"))
def filter_and_mat((infile_neurons, infile_synapses), outfile):
    n = pickle.load(open(infile_neurons, 'r'))
    s = pickle.load(open(infile_synapses, 'r'))
    
    neurondf = n['neurondf']
    neurondf_subset = neurondf.dropna(subset=['name']) # drop all unnamed cells
    neurondf_subset['idx'] = np.arange(len(neurondf_subset))
    neurondf_subset.index = neurondf_subset['body ID']
    print "len(neurondf_subset)", len(neurondf_subset)

    tbar_df = s['tbar_df']
    connections_df = s['connections_df']
    print len(connections_df)
    
    dfmerged = pd.merge( connections_df, tbar_df, left_on='tbar_id', right_on='id')
    

    conns = dfmerged.groupby(['body ID_x', 'body ID_y']).size()
    conns = conns.reset_index()
    print len(conns)

    neuronlist = neurondf_subset.index.values.tolist()


    con_sub = conns[conns['body ID_y'].isin(neuronlist)]
    con_sub = con_sub[con_sub['body ID_x'].isin(neuronlist)]

    print "Creating a ", len(neuronlist), "by", len(neuronlist), "matrix"
    mat = np.zeros((len(neuronlist), len(neuronlist)), dtype=np.int16)

    for row_i, row in con_sub.iterrows():
        pre = row['body ID_x']
        post = row['body ID_y']
        pre_idx = neurondf_subset.get_value(pre, 'idx')
        post_idx = neurondf_subset.get_value(post, 'idx')
        #print pre_idx, post_idx
        if mat[pre_idx, post_idx] != 0:
            print "whoops"
        print pre_idx, post_idx, row[0]
        mat[pre_idx, post_idx] = row[0]
    pickle.dump({'synapse_con_mat' : mat, 
                 'neurondf'  : neurondf_subset}, 
                open(outfile, 'w'), -1)
                 


if __name__ == "__main__":
    pipeline_run([load_neurons, load_synapses, filter_and_mat])
