import numpy as np
import cPickle as pickle
import matplotlib
matplotlib.use('Agg')
from matplotlib import pylab
from ruffus import * 
import pandas
import sqlite3
import os
import pandas as pd

PAPER_MAX_CONTACT_AREA = 5.0 # microns, to eliminate touching somata

MOUSE_RETINA_DB='neurodata.preprocess/mouseretina/mouseretina.db'


DATA_DIR = "data"

def td(x):
    return os.path.join(DATA_DIR, x)


def create_adj_mat(con, area_thold, cell_data):
    """
    returns (upper-triangular) contact area matrix, cell_ids in order

    """
    
    df = pandas.io.sql.read_sql("select from_id, to_id, area, sum(area) as contact_area, count(area) as contact_count from contacts  where area < %f group by from_id, to_id" % area_thold, 
                                  con)
    
    CELL_N = len(cell_data)
    id_to_pos = {id: pos for pos, id in enumerate(cell_data.index.values)}

    area_mat = np.zeros((CELL_N, CELL_N), dtype=np.float32)

    # for cell_id_1 in cell_data.index.values:
    #     print cell_id_1
    #     for cell_id_2 in cell_data.index.values:
    #         row = df[(df['from_id'] == cell_id_1) & (df['to_id'] == cell_id_2)]
    #         if len(row) > 0:
    #             area_mat[id_to_pos[cell_id_1], 
    #                      id_to_pos[cell_id_2]] = row.iloc[0]['contact_area']
    for c_i, c_row in df.iterrows():
        i1 = id_to_pos.get(c_row['from_id'], -1)
        i2 = id_to_pos.get(c_row['to_id'], -1)
        if i1 >= 0 and i2 >= 0:
            area_mat[i1, i2] = c_row['contact_area']

 
    return area_mat, cell_data.index.values


@files(MOUSE_RETINA_DB, "adjmat.byid.png")
def plot_adj(infile, outfile):
    """
    
    """

    con = sqlite3.connect(infile)
    cell_data = pandas.io.sql.read_frame("select * from cells order by cell_id", 
                                         con, index_col="cell_id")


    area_mat, cell_ids = create_adj_mat(con, PAPER_MAX_CONTACT_AREA, cell_data)

    CELL_N = len(cell_ids)

    lower_triangular_idx = np.tril_indices(CELL_N)

    assert area_mat[lower_triangular_idx].sum() == 0

    area_mat += area_mat.T


    p = np.random.permutation(CELL_N)
    area_mat_p = area_mat[p, :]
    area_mat_p = area_mat_p[:, p]

    f = pylab.figure(figsize=(8, 8))
    ax = f.add_subplot(1, 1, 1)

    ax.imshow(area_mat_p  > 0.2, interpolation='nearest', 
              cmap=pylab.cm.Greys)
    
    f.savefig(outfile, dpi=600)

@files(MOUSE_RETINA_DB, td("retina.cleandata.pickle"))
def filter_and_mat(infile, outfile):
    """
    
    """

    con = sqlite3.connect(infile)
    cell_data = pd.io.sql.read_sql("select cells.cell_id, cells.type_id, x, y, z, designation, coarse from cells inner join somapositions on cells.cell_id == somapositions.cell_id  inner join types on cells.type_id == types.type_id order by cells.cell_id ", 
                                   con, index_col='cell_id')


    area_mat, cell_ids = create_adj_mat(con, PAPER_MAX_CONTACT_AREA, cell_data)

    CELL_N = len(cell_ids)

    lower_triangular_idx = np.tril_indices(CELL_N)

    assert area_mat[lower_triangular_idx].sum() == 0

    area_mat += area_mat.T
    pickle.dump({"synapse_con_mat" : area_mat, 
                 'neurondf' : cell_data, 
                 'latent_params' : ['x', 'y', 'z', 'type_id']}, 
                open(outfile, 'w'), -1)

if __name__ == "__main__":
    pipeline_run([plot_adj, filter_and_mat])
