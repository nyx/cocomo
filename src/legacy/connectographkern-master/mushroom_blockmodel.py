"""
Various basic runners and experiments for the mushroom body block model
"""

import numpy as np
import pandas as pd
import cPickle as pickle
import irm
from irm import cvpipelineutil
from ruffus import *
import os

td = lambda x : os.path.join('data', x)

def test_param():
    for chain in range(10):

        yield None, "results.out.%d.pickle" % chain, chain

@files(test_param)
def test_run(infile, outfile, chain):
    neurons_data = pickle.load(open("data/mushroombody.neurondf.pickle", 'r'))
    synapse_data = pickle.load(open("data/mushroombody.synapsesdf.pickle", 'r'))
    basicmat_data = pickle.load(open("data/mushroombody.cleandata.pickle", 'r'))

    bin_mat = (basicmat_data['synapse_con_mat'] > 0).astype(np.uint8)

    basicmat_data['synapse_con_mat'] > 0

    seed = chain
    np.random.seed(seed)


    conmat = bin_mat.astype(np.uint8)

    model_name = "BetaBernoulliNonConj"
    init, data = irm.irmio.default_graph_init(conmat, model_name)
    init['domains']['d1']['assignment'] = np.random.permutation(len(conmat)) % 100

    HPS = {'alpha' : 1.0, 'beta' : 1.0}

    init['relations']['R1']['hps'] = HPS

    ITERS = 100
    slow_anneal = irm.runner.default_kernel_anneal(iterations=int(ITERS * 0.7))

    kc ={'ITERS' : ITERS, 
         'kernels' : slow_anneal, 
         'cores' : 8}

    s = cvpipelineutil.run_exp_pure(data, init, 'anneal_slow_%d' % ITERS, 
                                    seed, kc)

    res = s['res']
    scores, state, times, _ = res
    pickle.dump({'res' : res, 
                 'seed' : seed, 
                 'chain' : chain}, 
                open(outfile, 'w'))


def kc_params():
    SAMPLEN = 10

    d = pickle.load(open(td("mushroombody.KC-rosette-KC.mats.pickle"), 'r'))

    a = d['all_results'][(20, True)]
    kc_kc_mats = a['kc_kc_mats']
    kc_rosette_kc_mats = a['kc_rosette_kc_mats']
    MBON_names = kc_rosette_kc_mats.keys()
    for i in range(10):
        for m in MBON_names:
            infile = td("mushroombody.KC-MBON.rosette.df.pickle")
            outfile = td("KC-rosette-KC.%s.samples.%02d.pickle" % (m, i))      
            yield infile, outfile, i, m


@files(kc_params)
def run_kc_inference(infile, outfile, seed, mbon_name):
    """
    For each MBON, look at the KC-KC connectivity through the 
    rosettes
    """

    neurons_data = pickle.load(open(td("mushroombody.neurondf.pickle"), 'r'))
    synapse_data = pickle.load(open(td("mushroombody.synapsesdf.pickle"), 'r'))
    basicmat_data = pickle.load(open(td("mushroombody.cleandata.pickle"), 'r'))
    kc_mbon_rosettes = pickle.load(open(infile, 'r'))
    rosette_df = kc_mbon_rosettes['rosette_df']
    tbar_df = synapse_data['tbar_df']
    connections_df = synapse_data['connections_df']
    neurons_df = neurons_data['neurondf']

    d = pickle.load(open(td("mushroombody.KC-rosette-KC.mats.pickle"), 'r'))

    a = d['all_results'][(20, True)]
    kc_kc_mats = a['kc_kc_mats']
    kc_rosette_kc_mats = a['kc_rosette_kc_mats']
    pre_ids_names_sorted = d['pre_ids_names_sorted']
    pre_ids_to_pos = d['pre_ids_to_pos']

    sort_order = d['sort_order']
    KCN = len(sort_order)
    ITERS = 1000

    np.random.seed(seed)

    samples = {}

    rosette_mat = kc_rosette_kc_mats[mbon_name]
    conn_mat = rosette_mat[:, :, 2:].sum(axis=2)


    conmat = (conn_mat > 0).astype(np.uint8)
    print "there are", np.sum(conn_mat.astype(float)), "ones" 
    model_name = "BetaBernoulliNonConj"
    init, data = irm.irmio.default_graph_init(conmat, model_name)
    init['domains']['d1']['assignment'] = np.random.permutation(len(conmat)) % 50

    HPS = {'alpha' : 1.0, 'beta' : 1.0}

    init['relations']['R1']['hps'] = HPS

    slow_anneal = irm.runner.default_kernel_anneal(iterations=int(0.7*ITERS))

    kc ={'ITERS' : ITERS, 
        'kernels' : slow_anneal}

    s = cvpipelineutil.run_exp_pure(data, init, 'anneal_slow_10', seed, kc)

    res = s['res']
    scores, state, times, _ = res
    samples[mbon_name] = res

    pickle.dump({'samples' : samples}, 
                open(outfile, 'w'))
    

def kc_all_relation_params():
    SAMPLEN = 10

    d = pickle.load(open(td("mushroombody.KC-rosette-KC.mats.pickle"), 'r'))

    a = d['all_results'][(20, True)]
    kc_kc_mats = a['kc_kc_mats']
    kc_rosette_kc_mats = a['kc_rosette_kc_mats']
    MBON_names = kc_rosette_kc_mats.keys()
    for i in range(10):
        infile = td("mushroombody.KC-MBON.rosette.df.pickle")
        outfile = td("KC-rosette-KC.all_relation.samples.%02d.pickle" % ( i))      
        yield infile, outfile, i


@files(kc_all_relation_params)
def run_kc_all_relation_inference(infile, outfile, seed):
    """
    Using one relation per KC, compute a single giant model
    """
    
    neurons_data = pickle.load(open(td("mushroombody.neurondf.pickle"), 'r'))
    synapse_data = pickle.load(open(td("mushroombody.synapsesdf.pickle"), 'r'))
    basicmat_data = pickle.load(open(td("mushroombody.cleandata.pickle"), 'r'))
    kc_mbon_rosettes = pickle.load(open(infile, 'r'))
    rosette_df = kc_mbon_rosettes['rosette_df']
    tbar_df = synapse_data['tbar_df']
    connections_df = synapse_data['connections_df']
    neurons_df = neurons_data['neurondf']

    d = pickle.load(open(td("mushroombody.KC-rosette-KC.mats.pickle"), 'r'))

    a = d['all_results'][(20, True)]
    kc_kc_mats = a['kc_kc_mats']
    kc_rosette_kc_mats = a['kc_rosette_kc_mats']
    pre_ids_names_sorted = d['pre_ids_names_sorted']
    pre_ids_to_pos = d['pre_ids_to_pos']

    sort_order = d['sort_order']
    KCN = len(sort_order)
    ITERS = 1000

    np.random.seed(seed)

    samples = {}

    mbon_names = kc_rosette_kc_mats.keys()
    conmats = []
    for mbon_name in mbon_names:
        rosette_mat = kc_rosette_kc_mats[mbon_name]
        conn_mat = rosette_mat[:, :, 2:].sum(axis=2)
        conmat = (conn_mat > 0).astype(np.uint8)
        print mbon_name,": there are", np.sum(conmat.astype(float)), "ones" 
        conmats.append(conmat)


    model_name = "BetaBernoulliNonConj"
    init, data = irm.irmio.default_graph_init(conmats[0], model_name, 
                                              extra_conn = conmats[1:])
    init['domains']['d1']['assignment'] = np.random.permutation(len(conmat)) % 50

    HPS = {'alpha' : 1.0, 'beta' : 1.0}
    for rk in init['relations'].keys():
        init['relations'][rk]['hps'] = HPS.copy()

    slow_anneal = irm.runner.default_kernel_anneal(iterations=int(0.7*ITERS))

    kc ={'ITERS' : ITERS, 
         'kernels' : slow_anneal}

    s = cvpipelineutil.run_exp_pure(data, init, 'anneal_slow_10', seed, kc)

    res = s['res']
    scores, state, times, _ = res
    samples[mbon_name] = res

    pickle.dump({'samples' : samples, 
                 'mbon_names' : mbon_names, 
                 'conmats' : conmats}, 
                open(outfile, 'w'))
        


def kc_params_per_type():
    SAMPLEN = 10

    d = pickle.load(open(td("mushroombody.KC-rosette-KC.mats.pickle"), 'r'))

    a = d['all_results'][(20, True)]
    kc_kc_mats = a['kc_kc_mats']
    kc_rosette_kc_mats = a['kc_rosette_kc_mats']
    MBON_names = kc_rosette_kc_mats.keys()
    for i in range(10):
        for m in MBON_names:
            infile = td("mushroombody.KC-MBON.rosette.df.pickle")
            outfile = td("KC-rosette-KC.per_type.%s.samples.%02d.pickle" % (m, i))      
            yield infile, outfile, i, m
            break # DEBUG only run first MBON


@files(kc_params_per_type)
def run_kc_per_type_inference(infile, outfile, seed, mbon_name):
    """
    Compute separate relations for each KC subtype and each MBON

    """
    neurons_data = pickle.load(open(td("mushroombody.neurondf.pickle"), 'r'))
    synapse_data = pickle.load(open(td("mushroombody.synapsesdf.pickle"), 'r'))
    basicmat_data = pickle.load(open(td("mushroombody.cleandata.pickle"), 'r'))
    kc_mbon_rosettes = pickle.load(open(infile, 'r'))
    rosette_df = kc_mbon_rosettes['rosette_df']
    tbar_df = synapse_data['tbar_df']
    connections_df = synapse_data['connections_df']
    neurons_df = neurons_data['neurondf']

    d = pickle.load(open(td("mushroombody.KC-rosette-KC.mats.pickle"), 'r'))

    a = d['all_results'][(20, True)]
    kc_kc_mats = a['kc_kc_mats']
    kc_rosette_kc_mats = a['kc_rosette_kc_mats']
    pre_ids_names_sorted = d['pre_ids_names_sorted']
    pre_ids_to_pos = d['pre_ids_to_pos']

    sort_order = d['sort_order']
    KCN = len(sort_order)
    ITERS = 1000

    np.random.seed(seed)

    samples = {}

    rosette_mat = kc_rosette_kc_mats[mbon_name]
    conn_mat = rosette_mat[:, :, 2:].sum(axis=2)


    conmat_main = (conn_mat > 0).astype(np.uint8)

    kc_cells = neurons_df[neurons_df['body ID'].isin(pre_ids_to_pos.keys())]
    kc_cells = kc_cells[kc_cells.name.str.contains("KC-")]
    kc_cells.name.value_counts()

    for kc_type, kcs in kc_cells.groupby('name'):
        # positions for these kcs:
        mat_ids_order = kcs['body ID']
        main_mat_pos = np.array([pre_ids_to_pos[i] for i in mat_ids_order])

        conmat_sub = conmat_main[main_mat_pos]
        conmat_sub = conmat_sub[:, main_mat_pos]

        print "there are", np.sum(conmat_sub.astype(float)), "ones" 
        model_name = "BetaBernoulliNonConj"
        init, data = irm.irmio.default_graph_init(conmat_sub, model_name)
        init['domains']['d1']['assignment'] = np.random.permutation(len(conmat_sub)) % 50

        HPS = {'alpha' : 1.0, 'beta' : 1.0}

        init['relations']['R1']['hps'] = HPS

        slow_anneal = irm.runner.default_kernel_anneal(iterations=int(0.7*ITERS))

        kc ={'ITERS' : ITERS, 
            'kernels' : slow_anneal}

        s = cvpipelineutil.run_exp_pure(data, init, 'anneal_slow_10', seed, kc)

        res = s['res']
        scores, state, times, _ = res
        samples[kc_type] = {'res' : res, 
                            'conmat_sub' : conmat_sub, 
                            'mat_ids_order' : mat_ids_order}

    pickle.dump({'samples' : samples, 
                 'kc_cells' : kc_cells, 
                 'conmat_main' : conmat_main}, 
                open(outfile, 'w'))
    
def kc_params_alt_likelihood():
    SAMPLEN = 10

    d = pickle.load(open(td("mushroombody.KC-rosette-KC.mats.pickle"), 'r'))

    a = d['all_results'][(20, True)]
    kc_kc_mats = a['kc_kc_mats']
    kc_rosette_kc_mats = a['kc_rosette_kc_mats']
    MBON_names = kc_rosette_kc_mats.keys()

    likelihoods = [ 'nich', 'bb']
    for ll in likelihoods:
        for i in range(10):
            for m in MBON_names:
                infile = td("mushroombody.KC-MBON.rosette.df.pickle")
                outfile = td("KC-rosette-KC.%s.%s.samples.%02d.pickle" % (m, ll, i))      
                yield infile, outfile, i, m, ll


@files(kc_params_alt_likelihood)
def run_kc_inference_alt_likelihood(infile, outfile, seed, mbon_name, ll):
    
    neurons_data = pickle.load(open(td("mushroombody.neurondf.pickle"), 'r'))
    synapse_data = pickle.load(open(td("mushroombody.synapsesdf.pickle"), 'r'))
    basicmat_data = pickle.load(open(td("mushroombody.cleandata.pickle"), 'r'))
    kc_mbon_rosettes = pickle.load(open(infile, 'r'))
    rosette_df = kc_mbon_rosettes['rosette_df']
    tbar_df = synapse_data['tbar_df']
    connections_df = synapse_data['connections_df']
    neurons_df = neurons_data['neurondf']

    d = pickle.load(open(td("mushroombody.KC-rosette-KC.mats.pickle"), 'r'))

    a = d['all_results'][(20, True)]
    kc_kc_mats = a['kc_kc_mats']
    kc_rosette_kc_mats = a['kc_rosette_kc_mats']
    pre_ids_names_sorted = d['pre_ids_names_sorted']
    pre_ids_to_pos = d['pre_ids_to_pos']

    sort_order = d['sort_order']
    KCN = len(sort_order)
    ITERS = 1000

    np.random.seed(seed)

    samples = {}

    rosette_mat = kc_rosette_kc_mats[mbon_name]
    conn_mat = rosette_mat[:, :, 2:].sum(axis=2)


    slow_anneal = irm.runner.default_kernel_anneal(iterations=int(0.7*ITERS))

    if ll == 'gp' :
        conmat = (conn_mat).astype(np.int32)        
        model_name = "GammaPoisson"
        HPS = {'alpha' : 1.0, 'beta' : 1.0}
        slow_anneal[0][1]['subkernels'][0] = ('conj_gibbs', {})

    elif ll == 'nich' :
        conmat = (conn_mat).astype(np.float32)        
        model_name = "NormalInverseChiSq"
        HPS = {'mu' : 0.0, 'kappa' : 1.0, 'sigmasq' : 1.0, 'nu' : 1.0}
        slow_anneal[0][1]['subkernels'][0] = ('conj_gibbs', {})
    
    elif ll == 'bb' :
        conmat = (conn_mat>0).astype(np.uint8)        
        model_name = "BetaBernoulli"
        HPS = {'alpha' : 1.0, 'beta' : 1.0}
        slow_anneal[0][1]['subkernels'][0] = ('conj_gibbs', {})
    
    else:
        raise NotImplementedError("model %s not implemented" % ll)

    init, data = irm.irmio.default_graph_init(conmat, model_name)
    init['domains']['d1']['assignment'] = np.random.permutation(len(conmat)) % 50

    init['relations']['R1']['hps'] = HPS



    kc ={'ITERS' : ITERS, 
        'kernels' : slow_anneal}

    s = cvpipelineutil.run_exp_pure(data, init, 'anneal_slow_10', seed, kc)

    res = s['res']
    scores, state, times, _ = res
    samples[mbon_name] = res

    pickle.dump({'samples' : samples}, 
                open(outfile, 'w'))


if __name__ == "__main__":
    pipeline_run([test_run, run_kc_inference,
                  run_kc_all_relation_inference,
                  run_kc_inference_alt_likelihood, 
                  run_kc_per_type_inference], multiprocess=8)
    
