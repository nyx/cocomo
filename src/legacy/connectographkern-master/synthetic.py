import numpy as np
import networkx as nx
import pandas as pd
import losses
import kernels
import topogen
import exputil
"""
Library of synthetic graphs. All are returned as a networkx
graph object. Metadata is captured in nodes and edges. 

Standard metadata:

"""

def stochastic_block_model(blocks, n, block_size_alpha = 10.0, 
                           conn_alpha_beta = (0.2, 0.5), 
                           pmin = 0.05, pmax=0.95, manual_weights= None, 
                           counts = False, 
                           symmetric = False) :
    """
    Stochastic block model. Blocks is the number of
    blocks, n is the number of nodes, alpha
    controls dirichlet dist of class assignment

    pmax = maximum prob
    pmin = minimum prob 

    """

    if manual_weights is None:
        if not counts :
            latent_mat = np.random.beta(conn_alpha_beta[0], 
                                        conn_alpha_beta[1],
                                    (blocks, blocks))
        else:
            latent_mat = np.random.gamma(conn_alpha_beta[0], 
                                         1./conn_alpha_beta[1], 
                                         (blocks, blocks))

        if symmetric:
            latent_mat = (latent_mat + latent_mat.T)/2.0
        latent_mat = np.clip(latent_mat, pmin, pmax)

    else:
        latent_mat = np.zeros((blocks, blocks))
        for k, m in manual_weights.iteritems():
            latent_mat[k[0], k[1]] = m

    pi = np.random.dirichlet(block_size_alpha*np.ones(blocks))
    sizes = np.random.multinomial(n, pi)
    assignments = np.random.permutation(n)
    G=nx.DiGraph(latent_mat = latent_mat)
    pos = 0
    
    for si, s in enumerate(sizes):
        for i in range(s):
            G.add_node(assignments[pos], block=si, 
                       latentpos=si / float(blocks))
            pos += 1
    for n1 in G.nodes():
        for n2 in G.nodes():
            p = latent_mat[G.node[n1]['block'], G.node[n2]['block']]
            if not counts:
                if np.random.rand() < p:
                    G.add_edge(n1, n2)
            else:
                v = np.random.poisson(p)
                if v > 0:
                    G.add_edge(n1, n2, weight=v)
    return G


def chain(n):
    """
    returns directed chain
    """
    G = nx.DiGraph()
    G.add_nodes_from(range(n))
    for i in range(n-1):
        G.add_edge(i, i+1)
    return G


def ring(n):
    """
    returns directed ring
    """
    G = nx.DiGraph()
    for i in range(n):

        G.add_node(i, theta=2.0*np.pi / n * i )
    for i in range(n):
        G.add_edge(i, (i+1) % n)
    return G


def torus(nmajor, nminor):
    """
    A torus with nmajor nodes around the major axis
    and nminor nodes around minor axis
    """
    rings = []
    for m in range(nmajor):
        minor_ring = ring(nminor)
        relabel_mapping = {k : (m, k) for k in range(nminor)}
        newring = nx.relabel_nodes(minor_ring, relabel_mapping, copy=True )
        for i in range(nminor):
            newring.node[newring.nodes()[i]]['major_theta'] = m * np.pi * 2.0 / nmajor

        rings.append(newring)
    G = nx.union_all(rings)
    for m in range(nmajor):
        for n in range(nminor):
            n1 = (m, n)
            n2 = ((m+1) % nmajor, n)
            G.add_edge(n1, n2)
    return G



def lattice(M, N):
    """
    returns lattice with flow
    """
    G = nx.DiGraph()
    for i in range(M):
        for j in range(N):
            G.add_node((i, j))
    for i in range(M):
        for j in range(N-1):
            G.add_edge((i, j), (i, j+1))
    for i in range(M-1):
        for j in range(N):
            G.add_edge((i, j), (i+1, j))
    return G

def get_adj_mat(G):
    return np.array(nx.adjacency_matrix(G).todense())

def get_nodes_to_df(G):
    nodemeta = []
    for ni, n in enumerate(G.nodes()):
        attr = {'node_pos' :  ni, 
                'node_name' : n}
        attr.update(G.node[n])
        nodemeta.append(attr)
    df = pd.DataFrame(nodemeta)
    df.index = df['node_pos']
    return df
    
def latent_space_to_graph(obs, xtrue):
    N = len(xtrue)

    G = nx.DiGraph()
    for n in range(N):
        G.add_node(n, latentpos = xtrue[n])
        
    for i in range(N):
        for j in range(N):
            if obs[i, j]:
                G.add_edge(i, j, weight=obs[i, j])
    return G

def latent_space_ring(N, D=2, gauss_size=1.0, loss_mean=0.5,
                      lossobj = None):
    if lossobj is None:
        lossobj = losses.Logistic(loss_mean)

    kern_func = kernels.gaussian(gauss_size)
    
    kern_f = exputil.make_kern_f(kern_func)


    #xtrue = np.random.normal(0, 1, (N, D))
    xtrue = topogen.sample_sphere(N, D) + np.random.normal(0, 0.0001, (N, D))
    print "xtrue.shape=", xtrue.shape
    kern_val = kern_f(xtrue, None)
    print kern_val.shape
    obs = lossobj.obs_forward(kern_val)
    print obs.shape
    return latent_space_to_graph(obs, xtrue)


from scipy.stats import vonmises
    
def vm(t1, t2, width):
    delta = t2-t1
    return  vonmises.pdf(t1 -t2, 2*np.pi/width )


def multiring_poisson(N, conn_points = (-1,),
              poisson_rate = 1.0, scale=0.2):
    """
    conn_points: How far ahead to we connect (in radians)
    
    """

    paramadj = np.zeros((N, N))
    theta = np.linspace(0, 2*np.pi, N)


    G = nx.DiGraph()
    for n in range(N):
        G.add_node(n, latentpos = theta[n], theta=theta[n])


    p = losses.Poisson(poisson_rate)
    for i in range(N):
        for j in range(N):
            lamb = 0
            for offset in conn_points:
                lamb += vm(theta[i] - offset, theta[j], scale)
            weight = p.obs_forward(lamb)
            if weight > 0:
                G.add_edge(i, j, weight=weight)

    return G


