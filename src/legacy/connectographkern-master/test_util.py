from nose.tools import * 

import numpy as np
import util


def test_kfold():
    M, N = 10, 20

    for K in [5, 10, 17]:
        counts = np.zeros((M, N))
    
        for obs_mat in util.kfoldgen((M, N), K):
            counts[obs_mat] += 1
        np.testing.assert_array_equal(counts, np.ones((M, N)) * (K-1))
    
