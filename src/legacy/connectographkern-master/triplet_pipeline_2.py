import numpy as np

import synthetic
import triplet
import time
import exputil
import cPickle as pickle
from ruffus import * 
import os
import pandas as pd
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
from matplotlib import pylab
import seaborn as sns
sns.set_style("whitegrid", {'axes.grid' : False})
from exputil import dict_product
from triplet import split_rational_quad, STEbase
from kernels import rational_quad
from matplotlib.collections import LineCollection


td = lambda x : os.path.join("data", x)
ALL_DATA = ['ring_poisson_(-1, 1)', 
            'ring_poisson_(1,)', 
            'ring_poisson_(-1,)', 
            'block_asym.count', 
            'block_sym.count', 
            'synfire_1d', 
            'retina', 
            'medulla', 
            'celegans.elec', 
            'celegans.chem', 
            'celegans.elec_bin', 
            'celegans.chem_bin', 
            'mushroombody']

def create_data_params():
    for d in ALL_DATA:
        infile = td(d + ".cleandata.pickle")
        outfile = td(d + ".triples.pickle")
        yield infile, outfile

#@files("data/block_asym.count.cleandata.pickle", td("test.triples.data"))
#@files("data/ring_poisson_(-1, 1).cleandata.pickle", td("ring.triples.data"))
@files(create_data_params)
def create_data(infile, outfile):


    data = pickle.load(open(infile, 'r'))
    data.keys()
    adj  = data['synapse_con_mat'] 
    neurondf = data['neurondf'] 
    
    np.random.seed(0)

    # obs = connection_matrix

    N = adj.shape[0]
    print "The matrix is", N, "by", N
    obsmask = np.random.rand(N, N) < 0.9

    TRIPLET_N = 100000
    TRIPLET_N_TEST = 100000
    t1 = time.time()
    #triplets = triplet.get_random_triplets(TRIPLET_N, adj, obsmask, True)

    triplets = triplet.get_all_triplets_mat(adj, obsmask)
    print "There are", len(triplets), "observed triplets"
    triplets = np.random.permutation(triplets)[:TRIPLET_N]

    t2 = time.time()
    #unobserved_triplets = triplet.get_random_triplets(TRIPLET_N, adj, obsmask, False)

    unobserved_triplets = triplet.get_all_triplets_mat(adj, ~obsmask)
    print "There are", len(unobserved_triplets), "unobserved triplets"
    unobserved_triplets = np.random.permutation(unobserved_triplets)[:TRIPLET_N_TEST]

    t3 = time.time()
    print "Took", t2-t1, "sec to get triplets and", t3-t2, "sec to get unobserved triplets"

    pickle.dump({'triplets' : triplets, 
                 'unobserved_triplets' : unobserved_triplets, 
                 'neurondf' : neurondf, 
                 'latent_params' : data.get('latent_params', []), 
                 'synapse_con_mat' : adj, 
                 'N' : N}, 
                open(outfile, 'w'), -1)

def basic_solver(*args, **kwargs):
    defaults = {'theta_inference' : False, 
                'use_adagrad' : False, 
                'verbose_iter' : kwargs['iters'] / 10}
    defaults.update(kwargs)
    return triplet.run_sgd_full(*args,
                                **defaults)




MODELS = {'ste_rg_2dim' : 
          {'kernel' : ('split_rational_quad', 
                       {'sigma' : [1.0], 
                        'alpha' : [3]}), 
           'loss' : ('STEbase', 
                     {}), 
           
           'solver' : ('basic_solver', {'iters' : [10000], 
                                        'alpha' : [1e-2, 1e-3]}), 
           'model_config' : {'dim' : [2, 4, 6, 8, 10, 12]}}, 

          'ste_rg_dim' : 
          {'kernel' : ('rational_quad', 
                       {'sigma' : [1.0], 
                        'alpha' : [3]}), 
           'loss' : ('STEbase', 
                     {}), 
           
           'solver' : ('basic_solver', {'iters' : [10000], 
                                        'alpha' : [1e-2]}), 
           'model_config' : {'dim' : [2, 4]}}
}
    
EXPERIMENTS = {'all_ste' : {'data' : ALL_DATA, 
                            'model' : 'ste_rg_2dim'},
               'all_ste_nosplit' : {'data' : ALL_DATA, 
                                    'model' : 'ste_rg_dim'}, 
}


def exp_params(): 
    for exp_name, exp in EXPERIMENTS.iteritems():
        data = exp['data']
        model_name = exp['model']
        model = MODELS[model_name]
        kernel = model['kernel']
        loss = model['loss']
        solver = model['solver']
        model_config = model['model_config'] 
        
        for data_name in data:
            for mc_i, mc in enumerate(dict_product(model_config)):
                for kc_i, kc in enumerate(dict_product(kernel[1])):
                    for lc_i, lc in enumerate(dict_product(loss[1])):
                        for s_i, s in enumerate(dict_product(solver[1])):
                            infile = td(data_name + ".triples.pickle")
                            outfile = td("exp.%s.%s.%d.%s_%d.%s_%d.%s_%d.results" % (exp_name, data_name, mc_i, 
                            kernel[0], kc_i, loss[0], lc_i, solver[0], s_i))


                            yield infile, outfile, exp_name, data_name, mc, kc, lc, s
                            
@follows(create_data)           
@files(exp_params)
def run_triplet_experiment(infile, outfile, exp_name, data_name, mc, kc, lc, s): 
    print infile, outfile

    exp = EXPERIMENTS[exp_name]
    model_name = exp['model']
    model = MODELS[model_name]
    kernel_factory = eval(model['kernel'][0])
    loss_factory = eval(model['loss'][0])
    solver = eval(model['solver'][0])
    model_config = model['model_config'] 
        
        
    d = pickle.load(open(infile, 'r'))
    N = d['N']
    triplets = d['triplets']
    unobserved_triplets = d['unobserved_triplets']


    obs = np.array(triplets)
    obsvalid = np.zeros((1, len(obs)))

    dim = mc['dim']
    x =  np.random.normal(0, 1.0, (N, dim))

    loss = loss_factory(**lc)
    
    kern = kernel_factory(**kc)
    kern_f = exputil.make_kern_f(kern)


    theta_init = np.zeros(dim)
    theta_init[:] = 0.2
    t1 = time.time()
    ret = solver(kern, loss.cost(), x.copy(), theta_init,  obs, 
                 obsvalid, **s)
    
    t2 = time.time()
    print "solve took", t2-t1, "Sec"
    
    x = ret['x_est']

    
    kf = kern_f(x, [])
    count = 0
    for i, j, k in unobserved_triplets:
        if kf[i, j] > kf[i, k]:
            count += 1
    triplet_accuracy =  float(count) /  len(unobserved_triplets) 


    pickle.dump({'exp' : exp, 
                 'model_name' : model_name, 
                 'infile' : infile, 
                 'data_name' : data_name, 
                 'model' : model, 
                 'mc'  : mc, 'kc' : kc, 'lc' : lc, 's' : s, 
                 'time' : t2-t1, 
                 'x_est' : x, 
                 'triplet_accuracy' : triplet_accuracy, 
                 'ret' : ret}, 
                open(outfile, 'w'), -1)



@transform(run_triplet_experiment, suffix(".results"), ".plot_latent.pickle")
def plot_latent(infile, outfile):
    d = pickle.load(open(infile, 'r'))
    ret = d['ret']
    datasrc = pickle.load(open(d['infile'], 'r'))
    dfin = datasrc['neurondf']
    latent_params = datasrc.get('latent_params', [])
    outbase = outfile[:-len(".plot_latent.pickle")]

    
    latent_params = datasrc.get('latent_params', [])
    latent_params.append(None)
    for latent_param in latent_params:
        print "plot_latent", infile, "plotting param", latent_param
        x_est =  ret['x_est']

        df = pd.DataFrame(x_est)
        if latent_param is None:
            df['hue'] = np.ones(len(df))
        else:

            df['hue'] = dfin[latent_param]

        discrete = True
        hue_col_name = 'hue'
    
        cm = pylab.cm.hsv

        if df.hue.dtype == np.object:
            unique_vals = np.unique(df.hue).tolist()
            hue = np.zeros(len(df.hue))
            for ai, a in enumerate(df.hue):
                hue[ai] = unique_vals.index(a)
            df.hue = hue
        else:
            if len(np.unique(df.hue)) > 10 :
                discrete = False
                hue_col_name = None # don't use hue

                hue = df.hue / float(np.max(df.hue)) # np.linspace(0, 1, len(df))
                colors = pd.Series([cm(a) for a in hue], index=df.index)


        pylab.figure()

        g = sns.PairGrid(df, hue=hue_col_name, vars=range(x_est.shape[1]))

        def quantile_plot(x, y, **kwargs):

            if not discrete:
                del kwargs['color']
                kwargs['c'] =colors[x.index]
            plt.scatter(x, y,  edgecolor='none', s=4, alpha=0.5,  **kwargs)

        g.map_offdiag(quantile_plot)
        pylab.suptitle("%s dim=%d param=%s" % (infile, x_est.shape[1], 
                                               latent_param))
        pylab.savefig(outbase + '.%s.png' % (latent_param))
    pickle.dump({}, open(outfile, 'w'))

@transform(run_triplet_experiment, suffix(".results"), ".plot_arrows.pickle")
def plot_arrows(infile, outfile):
    d = pickle.load(open(infile, 'r'))
    ret = d['ret']
    datasrc = pickle.load(open(d['infile'], 'r'))
    dfin = datasrc['neurondf']
    latent_params = datasrc.get('latent_params', [])
    outbase = outfile[:-len(".plot_arrows.pickle")]

    
    latent_params = datasrc.get('latent_params', [])
    latent_params.append(None)
    for latent_param in latent_params:
        print "Plotting param", latent_param
        x_est =  ret['x_est']
        D = x_est.shape[1]
        oneD = D // 2

        df = pd.DataFrame(x_est)
        if latent_param is None:
            df['hue'] = np.ones(len(df))
        else:

            df['hue'] = dfin[latent_param]

        discrete = True
        hue_col_name = 'hue'
        if df.hue.dtype == np.object:
            unique_vals = np.unique(df.hue).tolist()
            hue = np.zeros(len(df.hue))
            for ai, a in enumerate(df.hue):
                hue[ai] = unique_vals.index(a)
        else:
        
            if len(np.unique(df.hue)) > 10:
                discrete = False
                hue_col_name = None # don't use hue
            hue = df.hue / float(np.max(df.hue)) # np.linspace(0, 1, len(df))

        cm = pylab.cm.hsv

        for i in range(oneD-1):
            fig = pylab.figure()
            
            ax = fig.add_subplot(1, 1, 1)

            ax.quiver(x_est[:, i + 0], x_est[:, i + 1], 
                      x_est[:, oneD + i ], x_est[:, oneD + i + 1], hue, cmap=cm)
            ax.set_title("(%d, %d) -> (%d, %d)" % (i, i+1, oneD+i, oneD+i+1))
            fig.savefig(outbase + '.arrows.%s.%d.png' % (latent_param, i))

    pickle.dump({}, open(outfile, 'w'))

if __name__ == "__main__":
    pipeline_run([run_triplet_experiment, plot_latent, plot_arrows], multiprocess=24)
    
