import numpy as np
import math

def rotation_matrix(axis, theta):
    """
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by theta radians.
    """
    axis = np.asarray(axis)
    theta = np.asarray(theta)
    axis = axis/math.sqrt(np.dot(axis, axis))
    a = math.cos(theta/2)
    b, c, d = -axis*math.sin(theta/2)
    aa, bb, cc, dd = a*a, b*b, c*c, d*d
    bc, ad, ac, ab, bd, cd = b*c, a*d, a*c, a*b, b*d, c*d
    return np.array([[aa+bb-cc-dd, 2*(bc+ad), 2*(bd-ac)],
                     [2*(bc-ad), aa+cc-bb-dd, 2*(cd+ab)],
                     [2*(bd+ac), 2*(cd-ab), aa+dd-bb-cc]])



def kfoldgen((M, N), k):
    """
    for a graph of shape G, generate a series of 
    cross-validated hold-out matrices
    """
    coords = np.array([(i, j) for i in range(M) for j in range(N)])
    for grp in np.array_split(coords, k):
        m = np.ones((M, N), dtype=np.bool)
        m[grp[:, 0], grp[:, 1]] = False
        yield m
        
