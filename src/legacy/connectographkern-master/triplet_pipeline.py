import numpy as np

import synthetic
import triplet
import time
import exputil
import cPickle as pickle
from ruffus import * 
import os
import pandas as pd
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
from matplotlib import pylab
import seaborn as sns
sns.set_style("whitegrid", {'axes.grid' : False})


td = lambda x : os.path.join("data", x)


DATA = ['ring_poisson_(-1, 1)', 
        'ring_poisson_(1,)', 
        'ring_poisson_(-1,)', 
        'block_asym.count', 
        'block_sym.count', 
        'synfire_1d', 
        'retina', 
        'medulla', 
        'mushroombody']

def create_data_params():
    for d in DATA:
        infile = td(d + ".cleandata.pickle")
        outfile = td(d + ".triples.pickle")
        yield infile, outfile

#@files("data/block_asym.count.cleandata.pickle", td("test.triples.data"))
#@files("data/ring_poisson_(-1, 1).cleandata.pickle", td("ring.triples.data"))
@files(create_data_params)
def create_data(infile, outfile):


    data = pickle.load(open(infile, 'r'))
    data.keys()
    adj  = data['synapse_con_mat'] 
    neurondf = data['neurondf'] 
    
    np.random.seed(0)

    # obs = connection_matrix

    N = adj.shape[0]
    print "The matrix is", N, "by", N
    obsmask = np.random.rand(N, N) < 0.9

    TRIPLET_N = 100000
    TRIPLET_N_TEST = 100000
    t1 = time.time()
    #triplets = triplet.get_random_triplets(TRIPLET_N, adj, obsmask, True)

    triplets = triplet.get_all_triplets_mat(adj, obsmask)
    print "There are", len(triplets), "observed triplets"
    triplets = np.random.permutation(triplets)[:TRIPLET_N]

    t2 = time.time()
    #unobserved_triplets = triplet.get_random_triplets(TRIPLET_N, adj, obsmask, False)

    unobserved_triplets = triplet.get_all_triplets_mat(adj, ~obsmask)
    print "There are", len(unobserved_triplets), "unobserved triplets"
    unobserved_triplets = np.random.permutation(unobserved_triplets)[:TRIPLET_N_TEST]

    t3 = time.time()
    print "Took", t2-t1, "sec to get triplets and", t3-t2, "sec to get unobserved triplets"

    pickle.dump({'triplets' : triplets, 
                 'unobserved_triplets' : unobserved_triplets, 
                 'neurondf' : neurondf, 
                 'latent_params' : data.get('latent_params', []), 
                 'synapse_con_mat' : adj, 
                 'N' : N}, 
                open(outfile, 'w'), -1)


@transform(create_data, suffix(".triples.pickle"), ".results")
def run_exp(infile, outfile):
    d = pickle.load(open(infile, 'r'))
    N = d['N']
    triplets = d['triplets']
    unobserved_triplets = d['unobserved_triplets']

    obs = np.array(triplets)



    obsvalid = np.zeros((1, len(obs)))

    ITERS = 10000
    res = []
    rets = []
    for estD in [2, 4, 6, 8]:    
        for lamb in [1.0]:
            for alpha in [1e-2, 1e-3, 1e-4]:
                x =  np.random.normal(0, 1.0, (N, estD))

                ste = triplet.STEbase()
                kern = triplet.split_rational_quad(lamb, 3)
                kern_f = exputil.make_kern_f(kern)


                obs = np.array(triplets, dtype=np.int32)
                theta_init = np.zeros(estD)
                theta_init[:] = 0.2
                t1 = time.time()
                ret = triplet.run_sgd_full(kern, ste.cost(), x.copy(), theta_init,  obs, 
                                           obsvalid, theta_inference=False, alpha=alpha, 
                                           use_adagrad=False, 
                                           iters=ITERS, verbose_iter = ITERS/10)
                t2 = time.time()
                print "solve took", t2-t1, "Sec"

                x = ret['x_est']


                kf = kern_f(x, [])
                count = 0
                for i, j, k in unobserved_triplets:
                    if kf[i, j] > kf[i, k]:
                        count += 1
                triplet_accuracy =  float(count) /  len(unobserved_triplets) 

                res.append({'lamb' : lamb, 
                            'estD' : estD, 
                            'alpha' : alpha, 
                            'triplet_accuracy' : triplet_accuracy})
                rets.append({'ret' : ret, 
                             'kern_f' : kern_f})
    pickle.dump({'infile' : infile, 
                 'resdf' :  pd.DataFrame(res), 
                 'rets' : rets},
                open(outfile, 'w'))

@transform(run_exp, suffix(".results"), ".plots.pickle")
def plot_result(infile, outfile):
    d = pickle.load(open(infile, 'r'))
    rets = d['rets']
    datasrc = pickle.load(open(d['infile'], 'r'))
    dfin = datasrc['neurondf']
    outbase = outfile[:-len(".plots.pickle")]
    print datasrc.keys()
    
    latent_params = datasrc.get('latent_params', [])
    latent_params.append(None)
    for ri, ret_dict in enumerate(rets):
        for latent_param in latent_params:
            print "Plotting param", latent_param
            ret = ret_dict['ret']
            x_est =  ret['x_est']

            df = pd.DataFrame(x_est)
            if latent_param is None:
                df['hue'] = np.ones(len(df))
            else:

                df['hue'] = dfin[latent_param]

            discrete = True
            hue_col_name = 'hue'
            if len(np.unique(df.hue)) > 10:
                discrete = False
                hue_col_name = None # don't use hue

            cm = pylab.cm.hsv

            hue = df.hue / float(np.max(df.hue)) # np.linspace(0, 1, len(df))
            colors = pd.Series([cm(a) for a in hue], index=df.index)


            pylab.figure()
                
            g = sns.PairGrid(df, hue=hue_col_name, vars=range(x_est.shape[1]))

            def quantile_plot(x, y, **kwargs):

                if not discrete:
                    del kwargs['color']
                    kwargs['c'] =colors[x.index]
                plt.scatter(x, y,  edgecolor='none', s=4, alpha=0.5,  **kwargs)

            g.map_offdiag(quantile_plot)
            pylab.suptitle("%s dim=%d param=%s" % (infile, x_est.shape[1], 
                                                   latent_param))
            pylab.savefig(outbase + '.%s.%d.png' % (latent_param, ri))
    pickle.dump({}, open(outfile, 'w'))

@transform(run_exp, suffix(".results"), ".mp4")
def test_movie(infile, outfile):

    
    FFMpegWriter = manimation.writers['ffmpeg']
    metadata = dict(title='Movie Test', artist='Matplotlib',
                    comment='Movie support!')
    writer = FFMpegWriter(fps=15, metadata=metadata)

    fig = plt.figure(figsize=(6, 6))

    l, = plt.plot([], [], 'k-o')

    plt.xlim(-5, 5)
    plt.ylim(-5, 5)

    x0, y0 = 0, 0

    with writer.saving(fig, outfile, 100):
        for i in range(100):
            x0 += 0.1 * np.random.randn()
            y0 += 0.1 * np.random.randn()
            l.set_data(x0, y0)
            writer.grab_frame()

if __name__ == "__main__":
    pipeline_run([run_exp, #test_movie,
                  plot_result], multiprocess=16)

