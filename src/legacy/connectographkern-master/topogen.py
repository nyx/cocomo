"""
Random functions for generating stereotyped shapes
in R^n inside the unit cube [+-1]

"""
import numpy as np


# generate latent points with a particular topology

def sample_sphere(N, D):
    # generate N points on the D-sphere
    a = np.random.normal(0, 1, (N, D))
    b = np.sqrt(np.sum(a**2, axis=1))
    c = a / b[:, None]
    return c

def s_curve(N, noise=0.0):
    s, _ = sklearn.datasets.make_s_curve(N, noise=noise)
    return s

