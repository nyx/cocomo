import sgd
import numpy as np
import util
import theano.tensor as T
import theano
from multiprocessing import Pool
import pandas as pd
import time
import scipy.sparse.linalg
import itertools
import sklearn.metrics
from numba import jit

def run_cv_exp(kern_func, loss_func, 
               xinit, theta_init, obs, obs_forward, kfold_cv, 
               alpha=0.1, iters=1000, batch_size=None, use_adagrad=False, 
               seed = None, partition_batch=False, 
               theta_inference=True, 
               verbose=False, verbose_iter=100, folds_to_run=None):
    """
    For a given kernel function and loss function run k-fold cross
    validation and return the estimated positions as well as the
    true and estimated connectivity values

    NOTE: Does not actually compute the loss for you
    """


    N, D = xinit.shape

    kern_f = make_kern_f(kern_func)


    cv_results = []

    for oi, obsvalid in enumerate(util.kfoldgen((N, N), kfold_cv)):
        if verbose:
            print "Running CV", oi
        xinit = np.random.normal(0, 1, (N, D))


        res = sgd.run_sgd_full(kern_func, loss_func, 
                               xinit, theta_init, 
                               obs, obsvalid, batch_size=batch_size, 
                               seed = seed, partition_batch=partition_batch,
                               
                               alpha=alpha, iters=iters, use_adagrad=use_adagrad,
                               theta_inference = theta_inference, 
                               verbose=verbose, verbose_iter=verbose_iter )

        kern_est = kern_f(res['x_est'], res['theta_est'])
        obs_est = obs_forward(kern_est)

        missing_pos = np.argwhere(obsvalid  == False)
        true_vals = obs[missing_pos[:, 0], missing_pos[:, 1]]
        est_vals = obs_est[missing_pos[:, 0], missing_pos[:, 1]]
        est_params =  kern_est[missing_pos[:, 0], 
                               missing_pos[:, 1]]
        cv_results.append({'cfold_i' : oi, 
                           'true_vals' : true_vals, 
                           'kern_est' : kern_est, 
                           'est_vals' : est_vals, 
                           'est_params' : est_params, 
                           'missing_pos' : missing_pos, 
                           'x_est' : res['x_est']})
        if folds_to_run is not None and (oi == (folds_to_run -1)):
            break
    return cv_results



def make_kern_f(kern_func):
    X = T.dmatrix('X')
    theta = T.dvector('theta')
    return theano.function([X, theta], 
                           kern_func(X, theta), 
                           on_unused_input='ignore') # some functions don't use theta


def sgd_param_test(kernel_func, log_likelihood_func, 
                   xinits, theta_init, obs, obs_valid, 
                   alphas, batch_ratios, 
                   theta_inference = False, iters=1000, 
                   exp_iters = 1):
    
    N = obs_valid.shape[0]

    reslog = []
    runs = 0
    for alpha in alphas:
        for batch_ratio in batch_ratios:
            for xi, xinit in enumerate(xinits):
                for exp_iter in range(exp_iters):
                
                    batch_size = int(batch_ratio * N)
                    res = sgd.run_sgd_full(kernel_func, log_likelihood_func, 
                                       xinit, theta_init, obs, obs_valid, 
                                       alpha=alpha, 
                                       iters = iters, 
                                       batch_size=batch_size, 
                                       partition_batch=True, 
                                       use_adagrad=False, 
                                       theta_inference  = theta_inference, 
                                       verbose = False, 
                                       verbose_iter = 10)
                    obj_log = [r[1] for r in res['runlog']]

                    reslog.append({'alpha' : alpha, 
                                   'batch_ratio' : batch_ratio, 
                                   'exp_iter' : exp_iter,
                                   'xinit_i' : xi,
                                   'obj_log' : obj_log})
                    runs += 1
    return reslog


def sgd_param_test_par(kernel_func, log_likelihood_func, 
                       xinits, theta_init, obs, obs_valid, 
                       alphas, batch_ratios, 
                       theta_inference = False, iters=1000, 
                       use_adagrad = False,
                       exp_iters = 1, pool_n = 16):
    
    pool = mp.Pool(pool_n)

    N = obs_valid.shape[0]

    reslog = []

    def apply_res((alpha, batch_ratio, xinit)):
        batch_size = int(batch_ratio * N)
        
        try: 
            res = sgd.run_sgd_full(kernel_func, log_likelihood_func, 
                                   xinit, theta_init, obs, obs_valid, 
                                   alpha=alpha, 
                                   iters = iters, 
                                   batch_size=batch_size, 
                                   partition_batch=True, 
                                   use_adagrad=use_adagrad, 
                                   theta_inference  = theta_inference, 
                                   verbose = False, 
                                   verbose_iter = 10)
            obj_log = [r[1] for r in res['runlog']]
        except ValueError:
            return [-np.inf], xinit, theta_init
            
        return obj_log, res['x_est'], res['theta_est']



    runs = 0
    configs = []
    for alpha in alphas:
        for batch_ratio in batch_ratios:
            for xi, xinit in enumerate(xinits):
                for exp_iter in range(exp_iters):
                    configs.append((alpha, batch_ratio, xinit))

                    reslog.append({'alpha' : alpha, 
                                   'batch_ratio' : batch_ratio, 
                                   'exp_iter' : exp_iter, 
                                   'xinit_i' : xi})

                    runs += 1

    rs = pool.map(apply_res, configs)


    #pool.join()
    for ro, r in zip(rs, reslog):
        r['obj_log'] = ro[0]
        r['x_est'] = ro[1]
        r['theta_est'] = ro[2]

    return reslog

def get_best_run_params(res):
    allres = []
    for r in res:
        for oi, o in enumerate(r['obj_log']):
            rc = r.copy()
            del rc['obj_log']
            rc['obj'] = o
            rc['objiter'] = oi*10
            allres.append(rc)
    df = pd.DataFrame(allres)
    maxiter = df.objiter.max()
    df_max = df[df.objiter == maxiter]
    return df_max.sort_values('obj', ascending=False).iloc[0].to_dict()


def run_cv_exp_bfgs(kern_func, loss_func, 
                    xinit, theta_init, obs, obs_forward, kfold_cv, 
                    seed = None, 
                    theta_inference=False, iters=100, svd_xinit = False, 
                    verbose=False, verbose_iter=100, folds_to_run=None, 
                    solve_method = 'L-BFGS-B', 
                    pool_n = 0):
    """
    For a given kernel function and loss function run k-fold cross
    validation and return the estimated positions as well as the
    true and estimated connectivity values

    NOTE: Does not actually compute the loss for you
    """


    N, D = xinit.shape

    kern_f = make_kern_f(kern_func)

    cv_results = []
    if pool_n > 0:
        pool = mp.Pool(pool_n)

    def exp_run(obsvalid):
        if svd_xinit:
            obs_masked = obs.copy()
            obs_masked[~obsvalid] = 0
            t1 = time.time()
            U, s, V = scipy.sparse.linalg.svds(obs_masked, k=D)
            #U, s, V =np.linalg.svd(obs_masked)
            print "svd init took", time.time() - t1
            xinit_use = U[:, :D]
        else:
            xinit_use = xinit.copy()

        res = sgd.run_bfgs(kern_func, loss_func, 
                           xinit_use, theta_init, obs, obsvalid,
                           iters=iters, solve_method = solve_method, 
                           theta_inference=theta_inference)
        return res

    obsvalids = list(util.kfoldgen((N, N), kfold_cv))
    if folds_to_run is not None:
        obsvalids = obsvalids[:folds_to_run]

    if pool_n > 0:
        allres = pool.map(exp_run, obsvalids)
    else:
        allres = map(exp_run, obsvalids)
    
    for oi, res in enumerate(allres):

        kern_est = kern_f(res['x_est'], res['theta_est'])
        obs_est = obs_forward(kern_est)
        obsvalid = obsvalids[oi]

        missing_pos = np.argwhere(obsvalid  == False)
        true_vals = obs[missing_pos[:, 0], missing_pos[:, 1]]
        est_vals = obs_est[missing_pos[:, 0], missing_pos[:, 1]]
        est_params =  kern_est[missing_pos[:, 0], 
                               missing_pos[:, 1]]
        cv_results.append({'cfold_i' : oi, 
                           'true_vals' : true_vals, 
                           'kern_est' : kern_est, 
                           'est_vals' : est_vals, 
                           'est_params' : est_params, 
                           'missing_pos' : missing_pos, 
                           'time' : res['time'], 
                           'x_est' : res['x_est'], 
                           'theta_est' : res['theta_est']})
    if pool_n > 0:
        pool.close()
        pool.join()
    return cv_results


def run_cv_exp_bfgs_dict(*args, **kwargs):
    return args, kwargs

def dict_product(dicts):
    return (dict(itertools.izip(dicts, x)) for x in itertools.product(*dicts.itervalues()))


def create_cv_mask_set(Mshape, nfold):
    """
    create a set of nfold "observed" matrices which each of whcih masks out
    1/nfold of the data
    """


    out = np.ones((nfold, Mshape[0], Mshape[1]), dtype=np.uint8)
    print Mshape, out.shape
    perm = np.random.permutation(Mshape[0] * Mshape[1])
    for cvi, cv in enumerate(np.array_split(perm, nfold)):
        out[cvi].ravel()[cv] = 0
        assert np.sum(out[cvi].flatten()==0) == len(cv)
    return out


def compute_cluster_stats(assignment_matrix):
    """
    assignment matrix has one assigment vector per row

    """
    res = []
    for i in range(len(assignment_matrix)):
        for j in range(len(assignment_matrix)):
            if i > j:
                a1 = assignment_matrix[i]
                a2 = assignment_matrix[j]
                res.append({'i' : i, 'j' : j, 
                            'ari' : sklearn.metrics.adjusted_rand_score(a1, a2), 
                            'homogeneity' : sklearn.metrics.homogeneity_score(a1, a2), 
                           'completeness' : sklearn.metrics.completeness_score(a1, a2)})
    sample_ari_df = pd.DataFrame(res)
    return sample_ari_df


def distance(x, y):
    return np.sqrt(np.sum((x-y)**2))

def compute_distance_pos(v, w, p):
    l2 = np.linalg.norm(v-w)**2 # i.e. |w-v|^2 -  avoid a sqrt
    if l2 == 0.0: 
        return distance(p, v);   #  v == w case
    # Consider the line extending the segment, parameterized as v + t (w - v).
    # We find projection of point p onto the line. 
    # It falls where t = [(p-v) . (w-v)] / |w-v|^2
    # We clamp t from [0,1] to handle points outside the segment vw.
    t = max(0, min(1, np.dot(p - v, w - v) / l2))
    
    projection = v + t * (w - v) #  Projection falls on the segment
    return t, distance(p, projection);

def compute_prob_matrix(tgt_latent, model_name='LogisticDistance', 
                        relation_name = 'R1', domain_name = "d1"):
    """
    Compute the probability of a connection at EVERY LOCATION in the matrix

    Does not depend on the actual observed values of data


    """
    ss = tgt_latent['relations'][relation_name]['ss']
    ass = tgt_latent['domains'][domain_name]['assignment']
    hps = tgt_latent['relations'][relation_name]['hps']
    
    N = len(ass)
    pred = np.zeros((N, N))
    
    for i in range(N):
        for j in range(N):
            c1 = ass[i]
            c2 = ass[j]
            c = ss[(c1, c2)]
            if model_name == "BetaBernoulliNonConj":
                y = c['p']
            else:
                raise NotImplementedError()
            pred[i, j] = y
    return pred


def compute_stats(job_res, cvs, conmat):

    dfres = []
    for ji, j in enumerate(job_res):
        filename, cv_i, seed, res = j
        score, state, log, _ = res
        m = compute_prob_matrix(state, model_name="BetaBernoulliNonConj")

        # get the cv matrix 
        cv_mask_flat = cvs[cv_i].flatten()
        for observed in [True, False]:
            if observed:
                idx = np.argwhere(cv_mask_flat==1)
            else:
                idx = np.argwhere(cv_mask_flat==0)
            preds = m.flatten()[idx]
            truths = conmat.flatten()[idx]
            fpr, tpr, _ = sklearn.metrics.roc_curve(truths, preds)
            roc_auc = sklearn.metrics.roc_auc_score(truths, preds)
            pr_auc = sklearn.metrics.average_precision_score(truths, preds)
            dfres.append({'cv_i' : cv_i, 
                          'seed' : seed, 
                           'preds' : preds, 
                            'truths' : truths, 
                          'observed' : observed,
                          'roc_auc' : roc_auc, 
                          'pr_auc' : pr_auc, 
                             })
    return pd.DataFrame(dfres)



@jit(nopython=True)
def compute_zmatrix(list_of_assignment):
    AV = len(list_of_assignment)
    N = len(list_of_assignment[0])
    
    z = np.zeros((N, N), dtype=np.uint32)
    
    for ai in range(AV):
        a = list_of_assignment[ai]
        for i in range(N):
            for j in range(N):
                if a[i] == a[j]:
                    z[i, j] +=1 
    return z
