import networkx as nx
from ruffus import * 
import time 
import cPickle as pickle
import os
import pandas as pd
import numpy as np
import resistance
from rtree import index
import md5

DATA_DIR = "data/"
td = lambda x: os.path.join(DATA_DIR, x)

def param_swc():
    neurons_data = pickle.load(open("data/mushroombody.neurondf.pickle", 'r'))
    neurons_df = neurons_data['neurondf']
    neuronsdf = neurons_df.set_index('body ID')
    neuronsdf['name_clean'] = neuronsdf.name.apply(lambda x: str(x))
    
    bodyIDs = neuronsdf[neuronsdf.name_clean.str.contains("MBON")].index.values

    for bodyID in bodyIDs:
    
        infile = "../data.original/mushroombody/skeletons/{:d}.swc".format(bodyID)
        outfile = td("graph.{:d}.pickle".format(bodyID))
        yield infile, outfile, bodyID

@files(param_swc)
def swc_to_graph(infile, outfile, bodyID):
    """
    Convert SWC files to undirected NetworkX graphs

    """
    df = pd.read_csv(infile, sep=" ", header=None)
    
    df.columns = ['samplenum', 'struct', 'x', 'y', 'z', 'radius', 'parent']
    df = df.set_index('samplenum')
    


    g = nx.Graph()
    g.add_nodes_from(df.index.values)
    

    edge_id = 0
    edge_id_dict = {}

    for ri, r in df.iterrows():
        if r.parent != -1: # assuming this is a NULL
            g.add_edge(ri, r.parent, id=edge_id)
            edge_id_dict[edge_id] = (ri, r.parent)
            edge_id += 1
        g.node[ri]['x'] = r.x
        g.node[ri]['y'] = r.y
        g.node[ri]['z'] = r.z
        g.node[ri]['radius'] = r.radius
    # add length
    for n1,n2, a in g.edges(data=True):
        n1a = g.node[n1]
        n2a = g.node[n2]
        n1pos = np.array([n1a['x'], n1a['y'], n1a['z']])
        n2pos = np.array([n2a['x'], n2a['y'], n2a['z']])
        d = np.sqrt(np.sum((n1pos - n2pos)**2))
        g[n1][n2]['length'] = d 

    pickle.dump({'G' : g, 
                 'bodyID' : bodyID, 
                 'swc_df' : df, 
                 'edge_id_dict' : edge_id_dict}, 
                open(outfile, 'w'))

@transform(swc_to_graph, suffix(".pickle"), 
           ".length_resistance.pickle")
def compute_node_resistances(infile, outfile, weight_var='length'):
    """
    Compute the node resistance via length( between any two nodes
    
    """
    d = pickle.load(open(infile, 'r'))
    G = d['G']
    N = len(G.nodes())
    all_nodes = G.nodes()

    outfile_npy = os.path.splitext(outfile)[0] + ".npy"
    out_resistance = np.lib.format.open_memmap(outfile_npy, 
                                               shape=(N, N), 
                                               dtype=np.float32, 
                                               mode='w+')
    out_resistance[:] = np.inf

    out_node_pos_dict = {}
    block_pos = 0
    for subg_nodes in nx.connected_components(G):
        subg = G.subgraph(subg_nodes)

        assert nx.is_tree(subg)

        dist, node_to_pos_dict = resistance.graph_path_distance_fast(subg, weight_var=weight_var)

        subN = len(subg_nodes)
        assert subN == len(node_to_pos_dict)
        out_resistance[block_pos:block_pos + subN, 
                       block_pos:block_pos + subN] = dist

        for n, p in node_to_pos_dict.iteritems():
            out_node_pos_dict[n] = p + block_pos

        block_pos += subN
    pickle.dump({'out_node_pos_dict' : out_node_pos_dict, 
                 'npy_filename' : outfile_npy}, 
                open(outfile, 'w'))


def distance(x, y):
    return np.sqrt(np.sum((x-y)**2))

def compute_distance_pos(v, w, p):
    l2 = np.linalg.norm(v-w)**2 # i.e. |w-v|^2 -  avoid a sqrt
    if l2 == 0.0: 
        return distance(p, v);   #  v == w case
    # Consider the line extending the segment, parameterized as v + t (w - v).
    # We find projection of point p onto the line. 
    # It falls where t = [(p-v) . (w-v)] / |w-v|^2
    # We clamp t from [0,1] to handle points outside the segment vw.
    t = max(0, min(1, np.dot(p - v, w - v) / l2))
    
    projection = v + t * (w - v) #  Projection falls on the segment
    return t, distance(p, projection);


@transform(swc_to_graph, suffix(".pickle"), ".tbar_edge.pickle")
def find_closest_edge_for_tbar(infile, outfile):

    synapse_data = pickle.load(open("data/mushroombody.synapsesdf.pickle", 'r'))
    tbar_df = synapse_data['tbar_df']
    connections_df = synapse_data['connections_df']
    
    dfmerged = pd.merge( connections_df, tbar_df, left_on='tbar_id', 
                         right_on='id', suffixes=('_post', '_pre'))
    dfmerged.convergent = dfmerged.convergent == 'convergent'




    d = pickle.load(open(infile, 'r'))
    G = d['G']
    TGT_BODY_ID = d['bodyID']

    edge_id_dict = d['edge_id_dict']


    dfmerged_mbon_post = dfmerged[dfmerged['body ID_post'] == TGT_BODY_ID].copy()
    # FIXME why do we have TBAR_ID DUPLICTES? 
    dfmerged_mbon_post = dfmerged_mbon_post.groupby('tbar_id', as_index=False).first()


    N = len(G.nodes())
    all_nodes = G.nodes()

    p = index.Property()
    p.dimension = 3
    p.dat_extension = 'data'
    p.idx_extension = 'index'
    infile_hash = md5.new(infile).hexdigest()
    index_file_name = '/tmp/3d_index.{}.{}'.format(infile_hash,
                                                     int(time.time()))
    print("creating rtree index at {:s}".format(index_file_name))
    idx3d = index.Index(index_file_name, properties=p)


    # populate the index with the locations of the nodes
    
    def psort(x, y):
        if x < y:
            return x, y
        else:
            return y, x

    pos = 0
    for n1, n2, id in G.edges(data='id'):
        n1a = G.node[n1]
        n2a = G.node[n2]
        x = psort(n1a['x'], n2a['x'])
        y = psort(n1a['y'], n2a['y'])
        z = psort(n1a['z'], n2a['z'])
        idx3d.insert(id, (x[0], y[0], z[0], x[1], y[1], z[1]))

        pos += 1


    # for each TBAR, find the NEAREST_N and compute the distances to
    # the midpoint of those

    NEAREST_N = 10
    res = []

    for ri, r in dfmerged_mbon_post.iterrows():

        tgt_point = np.array([r.loc_x_post, r.loc_y_post, r.loc_z_post])

        entry = {'tbar_id' : r.tbar_id, 
                 'loc_x_post' : r.loc_x_post, 
                 'loc_y_post' : r.loc_y_post, 
                 'loc_z_post' : r.loc_z_post}

        ei = 0
        for found_edge_id in list(idx3d.nearest((tgt_point[0], tgt_point[1], tgt_point[2]), NEAREST_N)):
            n1, n2 = edge_id_dict[found_edge_id]

            n1a = G.node[n1]
            n2a = G.node[n2]
            n1pos = np.array([n1a['x'], n1a['y'], n1a['z']])
            n2pos = np.array([n2a['x'], n2a['y'], n2a['z']])
            t, d = compute_distance_pos(n1pos, n2pos, tgt_point)

            #print found_edge_id, n1, n2, d
            per_edge_entry = entry.copy()
            per_edge_entry['n1'] = n1
            per_edge_entry['n2'] = n2
            per_edge_entry['edge_id'] = found_edge_id
            per_edge_entry['distance'] = d
            per_edge_entry['normalized_line_intercept'] = t
            per_edge_entry['found_edge_i'] = ei
            res.append(per_edge_entry)
            ei += 1
    tbar_distance_df = pd.DataFrame(res)        
    if len(tbar_distance_df) > 0:
        tbar_distance_df = tbar_distance_df.sort_values('distance', ascending=True)
        tbar_closest_distance_df = tbar_distance_df.groupby('tbar_id').first()
    else:
        print "WARNING empty dataframe", len(dfmerged_mbon_post)
        tbar_closest_distance_df = None

    pickle.dump({'infile' : infile, 
                 'tbar_closest_distance_df' : tbar_closest_distance_df}, 
                open(outfile, 'w'))

if __name__ == "__main__":
    pipeline_run([swc_to_graph, compute_node_resistances, 
                  find_closest_edge_for_tbar], multiprocess=4)
