"""
python.app mayavis.py
"""

import numpy as np
import networkx as nx
import cPickle as pickle



def plot_synapses(g):


    # Create data with x and y random in the [-2, 2] segment, and z a
    # Gaussian function of x and y.

    from mayavi import mlab
    mlab.figure(1, bgcolor=(0, 0, 0), fgcolor=(1, 1, 1))

    posdata = []
    for n, d in g.nodes_iter(data=True):
        posdata.append((d['x'], d['y'], d['z']))
    posdata = np.array(posdata)
    # Visualize the points

    x = posdata[:, 0]
    y = posdata[:, 1]
    z = posdata[:, 2]

    print posdata[:10]

    print "plotting"
    pts = mlab.points3d(x, y, z, # 
                        scale_mode='none', scale_factor=10.0)

    mlab.view(distance=10000) 
    
    mlab.show()

def plot_dendrites(g):


    from mayavi import mlab
    mlab.figure(1, size=(400, 400), bgcolor=(0, 0, 0))
    mlab.clf()


    node_to_pos = {}
    nodedata = []
    for ni, (n, d) in enumerate(g.nodes_iter(data=True)):
        nodedata.append((d['x'], d['y'], d['z']))
        node_to_pos[n] = ni

    nodedata = np.array(nodedata)
    # Visualize the points

    x = nodedata[:, 0]
    y = nodedata[:, 1]
    z = nodedata[:, 2]

    connections = []

    for n1, n2, a in g.edges(data=True):
        connections.append((node_to_pos[n1], node_to_pos[n2]))
    connections = np.array(connections)
    # Create the points
    src = mlab.pipeline.scalar_scatter(x, y, z) # , s)
    
    # Connect them
    src.mlab_source.dataset.lines = connections
    src.update()
    
    # The stripper filter cleans up connected lines
    lines = mlab.pipeline.stripper(src)
    
    # now add the extra points
    df = pickle.load(open('dfmerged_mbon_post.pickle', 'r'))
    
        
    pts = mlab.points3d(df.loc_x_post, 
                        df.loc_y_post, 
                        df.loc_z_post, # 
                        scale_mode='none', scale_factor=5.0)



    # Finally, display the set of lines
    mlab.pipeline.surface(lines, colormap='Accent', line_width=2, opacity=1.0)
    mlab.view(distance=10000) 

    # And choose a nice view
    #mlab.view(33.6, 106, 5.5, [0, 0, .05])
    #mlab.roll(125)
    mlab.show()
    

if __name__ == "__main__":


    g = pickle.load(open("test.graph.pickle", 'r'))
    #plot_synapses(g)
    plot_dendrites(g)
