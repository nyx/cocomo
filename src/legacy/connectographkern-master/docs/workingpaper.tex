\documentclass[8pt]{article}
%\usepackage[utf8]{inputenc}

\usepackage[english]{babel} % Language hyphenation and typographical rules

\usepackage[hmarginratio=1:1,top=0.5in,left=1.0in,columnsep=20pt]{geometry} % Document margins
\usepackage[small,labelfont=bf,up,textfont=it,up]{caption} % Custom captions under/above floats in tables or figures
\usepackage{subcaption}



\usepackage{hyperref} % For hyperlinks in the PDF
\usepackage{graphicx}
\usepackage[backend=biber,doi=false,isbn=false,url=false,eprint=false,sorting=none]{biblatex}



\usepackage{graphicx}
\usepackage[space]{grffile}


\usepackage[draft]{fixme}

\usepackage{grfext}
\bibliography{connectosbm}
\DeclareGraphicsRule{.ai}{pdf}{.ai}{}
\PrependGraphicsExtensions*{.ai,.AI}

\newcommand{\figpart}[1]{(\textbf{\uppercase{#1}})}

\title{Patterns of MBON presynaptic connectivity}
\author{Eric Jonas and Srini Turaga}


\renewcommand{\fixme}{\fxwarning}
\newcommand{\question}{\fxwarning}
\usepackage{sectsty}
\allsectionsfont{\normalfont\sffamily\bfseries}
\usepackage{palatino}

\RequirePackage{snapshot}

\usepackage[font={sf,footnotesize,up},textfont=up]{caption}


\begin{document}



\maketitle

\begin{abstract}
  Kenyon cells (KCs) provide the primary inputs to mushroom body
  output neurons (MBONs) in the drosophila mushroom body. Using
  recently-acquired connectome data we quantify the patterns present
  in KC to MBON connectivity through the lens of a class of latent
  variable models. This working paper outlines our initial exploration
  and preliminary results. We search for low-dimensional structure
  which explains the patterns of connectivity. Neuroscience often
  looks to parsimonious models to explain neural connectivity, both
  discrete (“cell types”) and continuous (“tuning curves”, “delay
  lines” ).
  
\end{abstract}


This is a working paper describing our initial implementation and analysis.
We describe figures directly in the body of the text. 

\section{Connectivity overview}
%% data frm
What is the bulk input to MBONs
\begin{figure}
  \includegraphics[width=3in]{figs/MBON rosette datascience.synapses_per_pre_type.pdf}
  \caption{Distribution of synapses onto MBONs for each presynaptic cell type}\label{fig:1}
\end{figure}


\begin{figure}
\centering\includegraphics[width=7in]{figs/MBON rosette datascience.pre_type_hist.0.pdf}
\caption{All KC synapse counts onto MBON organized by KC type.  }\label{fig:1}
\end{figure}



\section{Rosette analysis}
Rosettes are clusters of synapses \fixme{Cite something about rosettes
  existing as a morphological feature}. For a given MBON, we can plot
the distribution of inter-synapse distances, given a plot like below
for several example MBONs. As a comparison, we shuffle all synapse
coordinates \fixme{describe shuffle}. We see that relative to shuffle,
there's an increase in synpases within ~30 pixels of each other.

We can attempt to detect rosettes by clustering synapses. If two
synapses are closer than a euclidean threshold $T_d$, they are part of
the same cluster. Note that if three synapses (a, b, c) were arranged
in a line, and $d(a, b) < T_d$ and $d(b, c) < T_d$ then all three
would end up in the same cluster.

\fixme{Look at distribution of convex hull of synapses in a cluster, or maximum
  width, to determine how big of a problem this is}

For a given MBON at a given threshold $T_d$, this produces a
clustering of all synapses -- some singletons, some clusters of size
two, three, etc. We call all clusters of size two and greater a
``rosette''.

What is the bulk input to MBONs
\begin{figure}
  \centering
  \subcaptionbox{}
  {\includegraphics[width=3in]{figs/MBON rosette datascience.per-mbon distance histograms vs shuffle.pdf}}
  \subcaptionbox{}
  {\includegraphics[width=3in]{figs/MBON rosette datascience.size of rosettes per neuron type.40.pdf}}
  \subcaptionbox{}
  {\includegraphics[width=3in]{figs/MBON rosette datascience.rosettes_vs_thold.MBON-14-A.pdf}}
  \subcaptionbox{}
  {\includegraphics[width=3in]{figs/MBON rosette datascience.rosettes_vs_thold.MBON-07-B.pdf}}
  \caption{\figpart{A} Histogram of distances between pairs of
    synapses on a given MBON, vs shuffle. We can see that synapses
    cluster -- their proximity is closer than by chance. \figpart{B}
    Rosette sizes (number of synapses) when all synapses less than
    $40$ apart from one another. Note MBON-07-A and MBON-07-B
    primarially have singletons. \figpart{C,D} How the size of a
    rosette varies with our spatial threshold.}
\end{figure}


\question{Are we doing a good job of identifying rosettes?}

\subsection{How many rosettes does a pair of presynaptic cells
  participate in together?}

\begin{figure}
  \centering
  \begin{minipage}[b]{.45\linewidth}
    \centering
    \includegraphics[width=\linewidth]{figs/MBON rosette pair analysis.cell cell pairs through rosettes.pdf}
  \end{minipage}
  \begin{minipage}[b]{.5\linewidth}
    \centering
    \includegraphics[width=\linewidth]{figs/MBON rosette pair analysis.z-scores.pdf}
  \end{minipage}
  \begin{minipage}[b]{1.0\linewidth}
    \centering
  \includegraphics[width=\linewidth]{figs/MBON rosette pair analysis.shuffle_hists.MBON-14-A.20.zscore.pdf}
  \includegraphics[width=\linewidth]{figs/MBON rosette pair analysis.shuffle_hists.MBON-14-A.20.counts.pdf}
  \end{minipage}
  \caption{Is rosette participation random? \figpart{A} For every pair of presynaptic cells, we count how many rosettes they are involved in together. Compared to shuffle, KCs are much more likely to participate in more than one rosette together.}
\end{figure}

For each pair of presynaptic cells synapsing on a given MBON, we can
ask if they are disproportionately more likely to participate in
rosettes together. Fig~\ref{fig:rosette_participation} lists the
number of pairs that participate in 1, 2, or more rosettes. To assess
if this is by chance, we randomly shuffle the N presynaptic cells
between the M rosettes. This preseves both the number of synapses per
presynaptic cell as well as the distribution of rosette sizes. 500
shuffles are performed and the z-score-normalized results are shown.
We find that cell pairs participate in two or three rosettes together
more than shuffled data, but less than in one rosette together.

\fixme{Show how it's robust to rosette size. Is this actually a big
  effect? Can we express this in terms of conditional probabilities?
  ``Given a pair of cells is in a rosette together, they are N times
  more likely to participate in a second rosette and M times more likely to participate in a third rosette}

\fixme{Is this a physical location artifact}

\subsection{How often does a presynaptice cell participate multiple times in a rosette}

Is this more than chance?




\section{Block model results}
We first focus on the kenyon cells that participate in KC-MBON
synapses. The original data divided KCs into
\begin{itemize}
  \item KC-s:
  \item KC-c:
  \item KC-any:
  \item Others
\end{itemize}


First present results for a block model across all SBMs with
substantial connectivity

\subsection{Results for MBON-07-B}
\begin{figure}
  \centering
  \subcaptionbox{}
  {\includegraphics[width=0.3\linewidth]{figs/sbm kc-kc results analysis.bbnonconj_1000_th40_MBON-07-B.MBON-07-B.conmat.pdf}}
  \subcaptionbox{}
  {\includegraphics[width=0.3\linewidth]{figs/sbm kc-kc results analysis.bbnonconj_1000_th40_MBON-07-B.MBON-07-B.map_sorted.pdf}}
  \subcaptionbox{}
  {\includegraphics[width=0.3\linewidth]{figs/sbm kc-kc results analysis.bbnonconj_1000_th40_MBON-07-B.MBON-07-B.pr_auc_hist.pdf}}
   \subcaptionbox{}
  {\includegraphics[width=0.3\linewidth]{figs/sbm kc-kc results analysis.bbnonconj_1000_th40_MBON-07-B.MBON-07-B.roc_auc_hist.pdf}}
   \subcaptionbox{}
  {\includegraphics[width=0.3\linewidth]{figs/sbm kc-kc results analysis.bbnonconj_1000_th40_MBON-07-B.MBON-07-B.cluster_metric_anatomist_types.pdf}}
   \subcaptionbox{}
  {\includegraphics[width=0.3\linewidth]{figs/sbm kc-kc results analysis.bbnonconj_1000_th40_MBON-07-B.MBON-07-B.cluster_number.pdf}}
  \caption{KC connectivity through rosettes on MBON-07-B. \figpart{a} Raw input matrix \figpart{b} Discovered connectivity structure. \figpart{c} Cross-validated model fit. \figpart{d} Cross-validated model fit. \figpart{e} How well do the clusters we have identified represent a refinement of known cluster types?  \figpart{f} posterior distribution on the number of identified clusters. }
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.85\linewidth]{figs/sbm kc-kc results analysis.bbnonconj_1000_th40_MBON-07-B.MBON-07-B.z_matrix_with_truth.pdf}
  \caption{Sorted cell-cell coassignment matrix for MBON-07-B with known ground truth types}. 
\end{figure}
\subsubsection{KC-any cells are robustly grouped with KC-c and KC-s cells}
How do we show this?

\subsubsection{KC-s cells appear to have two distinct subtypes}
how do we show this


\subsection{Results for simultaneous fitting of all MBONs }

\begin{figure}
  \centering
  \subcaptionbox{}
  {\includegraphics[width=1.0\linewidth]{figs/sbm kc-kc tensor results analysis.main_mbon.conmat.png}}
  \subcaptionbox{}
  {\includegraphics[width=1.0\linewidth]{figs/sbm kc-kc tensor results analysis.main_mbon.map_sorted.png}}
\subcaptionbox{}
  {\includegraphics[width=0.35\linewidth]{figs/sbm kc-kc tensor results analysis.main_mbon.cluster_number.pdf}}
\subcaptionbox{}
  {\includegraphics[width=0.35\linewidth]{figs/sbm kc-kc tensor results analysis.main_mbon.cluster_metric_anatomist_types.pdf}}
\subcaptionbox{}
{\includegraphics[width=0.25\linewidth]{figs/sbm kc-kc tensor results analysis.main_mbon.cluster_size_confidence.pdf}}
\caption{Multi-relation stochastic block model using multiple MBONs simultaneously.\figpart{A} Input data is the through-rsette KC-KC connectivity matrices from eight MBONs. \figpart{B} The model is fit to all 8 MBONs simultaneously, identifying a KC-KC clustering that works well across MBONs. Here the MAP estimate of the sorted connectivty matrix, reveailing multi-scale block structure. \figpart{C} Posterior distribution of number of clusters. \figpart{D} Agreement of our clustering with neuronanatomist-defined types. \figpart{E} Cluster sizes as a function of posterior confidence (see text) }
\end{figure}

\begin{figure}
  \centering
  \subcaptionbox{}
  {\includegraphics[width=0.8\linewidth]{figs/sbm kc-kc tensor results analysis.main_mbon.z_matrix_with_truth.pdf}}
  \subcaptionbox{}
  {\includegraphics[width=0.4\linewidth]{figs/sbm kc-kc tensor results analysis.main_mbon.per_mbon_pr_auc.pdf}}
  \subcaptionbox{}
  {\includegraphics[width=0.4\linewidth]{figs/sbm kc-kc tensor results analysis.main_mbon.per_mbon_roc_auc.pdf}}
  \caption{}
\end{figure}



\section{Supplemental}

\subsection{A review of stochastic block models}

We use a nonparametric stochastic block model to capture link-dependent
cell type. Our exposition below closely follows EJ's previous work in
\autocite{JonasKording2015}.

We build a structured probabilistic model which begins with the
generic notion of a cell being a member of a single type, and the notion that
this cell type determines connectivity. Rather than taking in
examples of types annotated by human neuroanatomists, we instead start
with the weakest possible assumption in an attempt to algorithmically
discover this structure. We contrast this with the supervised
approaches taken in \autocite{Guerra2011}, where there is high
confidence in the (morphologically-defined) types and then a
supervised classifier is built, as our goal here is explicitly
discovery of types.


From these assumptions (priors) we develop a generative Bayesian model
that estimates the underlying cell types and how they connect. We take
as input (fig ~\ref{fig:overview}a) the connectivity matrix of cells
(fig ~\ref{fig:overview}b), we perform joint probabilistic inference
to automatically learn the number of cell types, which cells belong to
which type, their type-specific connectivity. 

We use a model for connectivity, the infinite stochastic block
model (iSBM)\autocite{Kemp2006a,Xu2006}, which has been shown to
meaningfully cluster connection graphs while learning the number of
hidden groups, or types. 

For the basic model, we take as input a connectivity
matrix $R$ defining the connections between cell $e_i$ and $e_j$.
We assume there exist an unknown number $K$ of latent
(unobserved) cell types, $k \in \{1, 2, 3, \dots, K\}$, and that each
cell $e_i$ belongs to a single cell type. We indicate a cell $e_i$ is
of type $k$ using the assignment vector $\vec(c)$, so $c_i = k$. The
observed connectivity between two cells $R(e_i, e_j)$ then depends
only on their latent type through a likelihood model
$R \sim p(\cdot)$. We assume $f$ is parameterized based on the
latent type, $c_i=m$ and $c_j=n$, via a parameter $\eta_{mn}$, as well
as a set of global hyper parameters $\theta$, such that the probability
of a connection between $e_i$ and $e_j$ is $p(R(e_i, e_j) | \eta_{mn}, \theta)$. 

We then jointly infer the posterior distribution of the
class assignment vector $\vec(c) = \{c_i\}$, the parameter matrix
$\eta_{mn}$, and the global model hyperparameters $\theta$ :

\begin{equation}
  p(\vec{c}, \eta, \theta | R ) \propto \prod_{i, j} p(R(e_i, e_j) | \eta_{c_ic_j}, \theta) \prod_{m, n} p(\eta_{mn} | \theta)  p(\theta) p(\vec{c} | \alpha) p(\alpha) p(\theta)
\end{equation}

In our subsequent analysis makes use of both the full posterior
distribution over these parameters as well as the most probable, or
maximum \textit{a posteriori} (MAP), estimate.


Inference is performed via Markov-chain Monte Carlo (MCMC) via three
composable transition kernels -- one for structural, one for per-type
parameters, and one kernel for global parameters and
hyperparameters. Details of data preprocessing, inference parameters,
and runtime can be found in the Supplementary Methods section.

We use a Dirichlet-process prior on class assignments, which allows
the number of classs to be determined automatically. In brief, for $N$
total cells, the probability of a cell belonging to a class is
proportional to the number of datapoints already in that class, $N_k$,
such that $p(c_i = k) \propto \frac{m_k}{N + \alpha}$ and the
probability of the cell belonging to a new class $k'$ is $p(c_i = k')
\propto \frac{\alpha}{N + \alpha}$. $\alpha$ is the global
concentration parameter -- larger values of $\alpha$ make the model
more likely to propose new classes. We grid the parameter $\alpha$ and
allow the best value to be learned from the data.

\subsection{Understanding clustering metrics}

\subsection{More rosette analysis}

Why use presynaptic vs postsynaptic location?

\subsection{Convergent vs non-convergent synapses}



\printbibliography

\end{document}
