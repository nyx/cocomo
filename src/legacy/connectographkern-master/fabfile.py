"""
notes:

conda run -n py35 --offline -- flintrock --config flintrock.config.yaml launch connecto2

# setup task depends on instance type
fab -P  -R all push_aws_creds install_anaconda add_packages  ssh_get_key change_file_limits deploy_irm  efs_mount 
fab -R master  master_setup 

"""


from fabric.api import local, env, run, put, cd, task, sudo, settings, warn_only
from fabric.contrib import project
import fabric as fab
import os
import boto.ec2

import subprocess
import yaml


user = 'ec2-user'
clustername = 'connecto2'

env.roledefs['m'] = ['jonas@c65']

AWS_REGION = 'us-west-2'

def get_cluster():
    config_path = os.path.abspath("flintrock.config.yaml")
    #cmdstr = "./flintrock.wrapper.sh --config %s describe %s" % (config_path, clustername)
    cmdstr = "conda run -n py35 --offline -- flintrock  --config %s describe %s" % (config_path, clustername)
    # HOLY CRAP THIS IS DUMB FIX THIS 
    try:
        res= subprocess.check_output(cmdstr, shell=True)
    except subprocess.CalledProcessError as e:
        print "WHY IS THIS A CODEPATH" 

    return res


def create_roles():
    # FIXME don't use fixed user
    try:
        res =  get_cluster()

        r2 = res.split("---")[-1].strip()

        state = yaml.load(r2)

        master = state[clustername]['master']
        slaves = state[clustername]['slaves']

        return {'master' : [user + '@' + m for m in [master]],
                'slaves' : [user + '@' + s for s in slaves], 
                'all' : [user + '@' + n for n in slaves + [master]]}
    except:
        return {}
        pass
env.roledefs.update(create_roles())

@task
def launch():
    local("./flintrock.wrapper.sh --config `pwd`/flintrock.config.yaml launch %s" % clustername)

@task        
def ssh():
    local("ssh -A " + env.host_string)

@task        
def ssh_get_key():
    local("ssh -o StrictHostKeyChecking=no " + env.host_string + " hostname")

@task
def put_anaconda():
    local("wget https://3230d63b5fc54e62148e-c95ac804525aac4b6dba79b00b39d1d3.ssl.cf1.rackcdn.com/Anaconda2-2.4.0-Linux-x86_64.sh")
    local("aws s3 cp Anaconda2-2.4.0-Linux-x86_64.sh s3://ericmjonas-public/anaconda.sh")

@task
def install_anaconda(setenv=True):
    sudo("yum install -y -q wget bzip2")
    sudo("pip install awscli")

    run("aws s3 cp s3://ericmjonas-public/anaconda.sh anaconda.sh")
    
#    run("rm -Rf Anaconda2-2.4.0-Linux-x86_64.sh")
#run("wget https://3230d63b5fc54e62148e-c95ac804525aac4b6dba79b00b39d1d3.ssl.cf1.rackcdn.com/Anaconda2-2.4.0-Linux-x86_64.sh")
    if setenv:
        run("chmod +x anaconda.sh")
        run("rm -rf $HOME/anaconda")
        run("./anaconda.sh -b -p $HOME/anaconda")
        run('echo "export PATH=$HOME/anaconda/bin:\$PATH" >> ~/.bash_profile')  # set up conda env

@task
def add_packages():
    sudo("yum update -y -q")
    sudo("yum install -y -q fftw-devel tmux htop postgresql94-devel postgresql-devel postgresql-libs  emacs htop libgfortran java-1.7.0-openjdk-devel dstat")


    run("conda update --all --yes")
    run("conda install scikit-learn scikit-image distributed paramiko theano --yes")
    run("pip -q install ruffus")
    run("pip -q install glob2")
    run("pip -q install suds")
    run("pip -q install nimfa")
    run("pip -q install --upgrade boto")
    run("pip -q install --upgrade seaborn")

    run("pip -q install colorbrewer")
    


@task
def setup():
    #sudo("ln -s /media/ephemeral0 /data")
    #run("mkdir -p /data/jonas/connecto")
    sudo("mount -o remount,exec /media/ephemeral0")
    ##sudo("mount -o remount,exec /media/ephemeral1") # Fixme test if extists

@task
def change_file_limits():
    sudo("echo '*                soft    nofile          1000000' >> /etc/security/limits.conf")
    sudo("echo '*                hard    nofile          1000000' >> /etc/security/limits.conf")


@task
def push_aws_creds():
    put("~/.boto", "~/.boto")
    run("mkdir -p ~/.aws")
    put("~/.aws/setupenv.sh", "~/.aws/setupenv.sh")
    put("~/.aws/credentials", "~/.aws/credentials")



@task
def get_data():
    run("aws s3 sync s3://jonas-solar-sdo/datasets /data/jonas/suncast/datasets/")
    run("~/hadoop/bin/hdfs dfs -copyFromLocal /data/jonas/suncast/datasets/ /datasets")
    

@task
def master_setup():
    run("echo 'export SPARK_HOME=~/spark' >> ~/.bashrc")
    run("echo 'source $SPARK_HOME/conf/spark-env.sh' >> ~/.bashrc")
    run("echo 'set-option -g history-limit 20000' >> ~/.tmux.conf")
    run("~/spark/sbin/stop-all.sh")
    run("~/spark/sbin/start-all.sh")

    #run("cp -R ~/suncast.base /data/jonas/suncast/suncast 2>/dev/null")

@task
def deploy():
        local('git ls-tree --full-tree --name-only -r HEAD > .git-files-list')
    
        project.rsync_project("/data/jonas/connecto/connectographkern", local_dir="./",
                              exclude=['*.npy', "*.ipynb", 'data', "*.mp4"],
                              extra_opts='--files-from=.git-files-list')

        # with lcd("/Users/jonas/projects/pycircos"):
        #     local('git ls-tree --full-tree --name-only -r HEAD > .git-files-list')

        #     project.rsync_project("/data/jonas/connectographkern/src/pycircos", local_dir="/Users/jonas/projects/pycircos" ,
        #                           exclude=['*.npy', "*.ipynb", 'data'],
        #                           extra_opts='--files-from=.git-files-list')

        project.rsync_project("/data/jonas/connecto/connectographkern/notebooks",
                              local_dir=".",
                              extra_opts="--include '*.png' --include '*.pdf' --include '*.ipynb'  --include='*/' --exclude='*' " ,
                              
                              upload=False)
    
        # download movies
        project.rsync_project("/data/jonas/connecto/connectographkern/data",
                              local_dir=".",
                              extra_opts="--include '*.mp4' --include '*.png'  --include='*/' --exclude='*' " ,
                              
                              upload=False)
    


@task
def deploy_irm():
    sudo("sudo yum install git boost-devel cmake gcc-c++ -y -q")
    with cd("/tmp"):
        run("mkdir -p src")
        with cd("src"):
            run("rm -Rf netmotifs")
            run("git clone https://github.com/ericmjonas/netmotifs")
            with cd("netmotifs"):
                run("mkdir -p irm/build")
                with cd("irm/build"):
                    run("cmake -DCMAKE_INSTALL_PREFIX=/home/ec2-user/anaconda ../")
                    run("make -j 16")
                    run("make install")
            
            
# @task
# def deploy():
#     """
#     Uses rsync. Note directory trailing-slash behavior is anti-intuitive
#     """
    
    

#     local('git ls-tree --full-tree --name-only -r HEAD | grep -v ipynb >  .git-files-list')
    
#     project.rsync_project("%s/suncast/" % tgt_dir, local_dir="./",
#                           exclude=['*.pickle', "*.ipynb"], # NOTE WILL NOT PUSH IPYTHON NOTEBOOKS

#                           extra_opts='--files-from=.git-files-list')
    
    
#     project.rsync_project("%s/suncast/" % tgt_dir, local_dir="./",
                          
#                           extra_opts="--include='*/' --include='*.jar' --exclude='*' --exclude='*$*'")
# @task 
# def deploy_and_remote_build(withdeps=False, gengitfiles=True):
#     """
#     Rsync git files and remote build

#     run copy-jars after
#     """

#     if gengitfiles != 'False':
#         local('git ls-tree --full-tree --name-only -r HEAD > .git-files-list')
    
#     project.rsync_project("%s/suncast/" % tgt_dir, local_dir="./",
#                           exclude=['*.pickle', "*.ipynb"], # NOTE WILL NOT PUSH IPYTHON NOTEBOOKS

#                           extra_opts='--files-from=.git-files-list --prune-empty-dirs')


#     with cd("/data/jonas/suncast/suncast/pipeline"):
#         run("rm -f src/main/scala/helloworld.scala")
#         if withdeps:
#             run("sbt/sbt clean")
#             run("sbt/sbt assemblyPackageDependency")
#         run("sbt/sbt assembly")

# @task 
# def copy_suncast_base():
#     run("cp -R ~/suncast.base /data/jonas/suncast/suncast 2>/dev/null")


@task 
def efs_mount():
    TGT_DIR = "/data"
    EFS_SECURITY_GROUP="sg-a927bdcc"
    FILESYSTEM_ID = "fs-bbd72012"
    
    # add instance to the security group 
    instance_id = run("curl -s http://169.254.169.254/latest/meta-data/instance-id").strip()


    ec2 = boto.ec2.connect_to_region(AWS_REGION)

    instances = ec2.get_only_instances(instance_ids=[instance_id])
    instance = instances[0]
    EFS_SECURITY_GROUP = "sg-a927bdcc"
    existing_groups = [g.id for g in instance.groups]
    instance.modify_attribute("groupSet",existing_groups + [EFS_SECURITY_GROUP])


    sudo("yum install -y -q nfs-utils")
    sudo("mkdir -p %s" % TGT_DIR)
    with warn_only():
        sudo("umount -f %s" % TGT_DIR)

    sudo("mount -t nfs4 -o nfsvers=4.1 $(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone).%s.efs.us-west-2.amazonaws.com:/ %s" % (FILESYSTEM_ID,  TGT_DIR))
    sudo("chown ec2-user /data")
    


@task
def run_server():
    """

    dask-scheduler --host $(curl -s http://169.254.169.254/latest/meta-data/hostname)

    dask-ssh --scheduler $(curl -s http://169.254.169.254/latest/meta-data/hostname) --hostfile ~/spark/conf/slaves --nprocs=16


    """

@task
def deploy_cluster():
    local('git ls-tree --full-tree --name-only -r HEAD > .git-files-list')

    project.rsync_project("/data/jonas/connecto/connectographkern", local_dir="./",
                          exclude=['*.npy', "*.ipynb", 'data', "*.mp4"],
                          extra_opts='--files-from=.git-files-list')

@task 
def download_dist_out():
    project.rsync_project("/data/jonas/connecto/connectographkern/dist.out", local_dir="./",
                          upload=False)



                          
    
@task
def deploy_data_to_cluster():
    project.rsync_project("/data/jonas/connecto/connectographkern/data", local_dir="./data/",
                          #exclude=['*.npy', "*.ipynb", 'data', "*.mp4"],
                          extra_opts='--progress')
