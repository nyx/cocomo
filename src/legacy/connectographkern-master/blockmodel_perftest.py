import numpy as np

import irm
import irm.cvpipelineutil
import time
from ruffus import * 
import cPickle as pickle

@files(None, 'benchmark.data.pickle')
def create_data(infile, outfile):

    # turn into numeric types
    T1_C = 20
    
    c = {}
    for i in range(T1_C):
        for j in range(T1_C):
            c[(i, j)] = (100, np.random.beta(0.5, 0.5))

    np.random.seed(0)
    nodes, conmat = irm.data.generate.c_class_neighbors(8, c, 
                                                        JITTER=0.1)
    print conmat.shape

    conmat = conmat.astype(np.uint8)

    pickle.dump({'nodes' : nodes, 
                 'conmat' : conmat}, 
                open(outfile, 'w'))
@files(create_data, "benchmark.nonconj.pickle")
def non_conj(infile, outfile):
    d = pickle.load(open(infile, 'r'))
    conmat = d['conmat'] 
    nodes = d['nodes']


    model_name = "BetaBernoulliNonConj"
    init, data = irm.irmio.default_graph_init(conmat, model_name)
    #init['domains']['d1']['assignment'] = np.random.permutation(len(conmat)) % 100

    HPS = {'alpha' : 1.0, 'beta' : 1.0}

    init['relations']['R1']['hps'] = HPS
    slow_anneal = irm.runner.default_kernel_anneal(start_temp=1.0, 
                                                   iterations=1)

    kc ={'ITERS' : 20, 
        'kernels' : slow_anneal}
    t1 = time.time()
    score, state, times, _ = irm.cvpipelineutil.run_exp_pure(data, init, 'anneal_slow_10', 0, kc)['res']
    t2 = time.time()
    print "inference time was", t2-t1, "sec"

    pickle.dump({'time':  t2-t1,
                 'score' : score, 
                 'state' : state, 
                 'times' : times}, open(outfile, 'w'))

@files(create_data, "benchmark.conj_nonconj.pickle")
def conj_nonconj(infile, outfile):
    d = pickle.load(open(infile, 'r'))
    conmat = d['conmat'] 
    nodes = d['nodes']


    model_name = "BetaBernoulli"
    init, data = irm.irmio.default_graph_init(conmat, model_name)
    #init['domains']['d1']['assignment'] = np.random.permutation(len(conmat)) % 100

    HPS = {'alpha' : 1.0, 'beta' : 1.0}

    init['relations']['R1']['hps'] = HPS
    slow_anneal = irm.runner.default_kernel_anneal(start_temp=1.0, 
                                                   iterations=1)

    kc ={'ITERS' : 20, 
        'kernels' : slow_anneal}
    t1 = time.time()
    score, state, times, _ = irm.cvpipelineutil.run_exp_pure(data, init, 'anneal_slow_10', 0, kc)['res']
    t2 = time.time()
    print "inference time was", t2-t1, "sec"

    pickle.dump({'time':  t2-t1,
                 'score' : score, 
                 'state' : state, 
                 'times' : times}, open(outfile, 'w'))

@files(create_data, "benchmark.conj_conj.pickle")
def conj_conj(infile, outfile):
    d = pickle.load(open(infile, 'r'))
    conmat = d['conmat'] 
    nodes = d['nodes']


    model_name = "BetaBernoulli"
    init, data = irm.irmio.default_graph_init(conmat, model_name)
    #init['domains']['d1']['assignment'] = np.random.permutation(len(conmat)) % 100

    HPS = {'alpha' : 1.0, 'beta' : 1.0}

    init['relations']['R1']['hps'] = HPS
    slow_anneal = irm.runner.default_kernel_anneal(start_temp=1.0, 
                                                   iterations=1)

    # replace 

    slow_anneal[0][1]['subkernels'][0] = ('conj_gibbs', {})

    kc ={'ITERS' : 20, 
        'kernels' : slow_anneal}
    t1 = time.time()
    score, state, times, _ = irm.cvpipelineutil.run_exp_pure(data, init, 'anneal_slow_10', 0, kc)['res']
    t2 = time.time()
    print "inference time was", t2-t1, "sec"

    pickle.dump({'time':  t2-t1,
                 'score' : score, 
                 'state' : state, 
                 'times' : times}, open(outfile, 'w'))

@files([non_conj, conj_nonconj, conj_conj], "metrics.txt")
def compute_metrics((non_conj_filename, conj_nonconj_filename, 
                     conj_conj_filename), outfilename):
    non_conj = pickle.load(open(non_conj_filename, 'r'))
    conj_nonconj = pickle.load(open(conj_nonconj_filename, 'r'))
    conj_conj = pickle.load(open(conj_conj_filename, 'r'))

    for data, name in [(non_conj, 'non_conj'), 
                       (conj_nonconj, 'conj_nonconj'), 
                       (conj_conj, 'conj_conj')]:
        print name, data['time']
        print name, irm.util.assign_to_counts(data['state']['domains']['d1']['assignment'])
        
if __name__ == "__main__":
    pipeline_run([create_data, non_conj, conj_nonconj, conj_conj, compute_metrics])

