import numpy as np
import theano.tensor as T

class Poisson(object):
    def __init__(self, scale):
        self.scale = scale


    def cost(self):

        def log_likelihood_func(obs, theta):
            return obs * T.log(self.scale * theta) - \
                self.scale * theta - T.gammaln(obs+1)
        return log_likelihood_func


    def obs_forward(self, x):
        """
        go from kernel val to observations
        """
        return np.random.poisson(self.scale * x)

class Normal(object):
    def __init__(self, scale):
        self.scale = scale


    def cost(self):
        def log_likelihood_func(obs, theta):
            return -(obs - theta)**2/self.scale
        return log_likelihood_func

    def obs_forward(self, x):
        """
        go from kernel val to observations
        """
        return x

class Logistic(object):
    def __init__(self, mean, scale=10.0):
        self.mean = mean
        self.scale = scale
    def cost(self):
        def log_likelihood_func(obs, theta):

            p = 1.0 / (1.0 + T.exp(-(theta - self.mean)*self.scale))
            return obs*T.log(p) + (1-obs)*T.log(1-p)

        return log_likelihood_func

    def obs_forward(self, x):
        """
        go from kernel val to observations
        """
        r = 1.0 / (1.0 + np.exp(-(x - self.mean)*self.scale))
        self.r = r
        return (np.random.rand(*x.shape) < r).astype(np.int)

    
