"""
Generate synthetic datasets
"""
import simplejson
from ruffus import * 
import os
import pandas as pd
import copy
import cPickle as pickle
import numpy as np
import synthetic




DATA_DIR = "data"

def td(x):
    return os.path.join(DATA_DIR, x)


@files(None, td("block_sym.cleandata.pickle"))
def generate_block_sym(_, outfile):
    np.random.seed(4)
    blocks = 4
    total_nodes = 100
    G = synthetic.stochastic_block_model(blocks, total_nodes, 
                                     symmetric=True)

    obs = synthetic.get_adj_mat(G)
    neurondf = synthetic.get_nodes_to_df(G)
    
    pickle.dump({'synapse_con_mat' : obs, 
                 'neurondf'  : neurondf}, 
                open(outfile, 'w'), -1)

@files(None, td("block_asym.cleandata.pickle"))
def generate_block_asym(_, outfile):
    np.random.seed(7)
    blocks = 4
    total_nodes = 100
    G = synthetic.stochastic_block_model(blocks, total_nodes, 
                                     symmetric=False)

    obs = synthetic.get_adj_mat(G)
    neurondf = synthetic.get_nodes_to_df(G)
    
    pickle.dump({'synapse_con_mat' : obs, 
                 'neurondf'  : neurondf}, 
                open(outfile, 'w'), -1)


@files(None, td("latentspace_ring_2d.cleandata.pickle"))
def generate_latentspace_ring_2d(_, outfile):
    np.random.seed(7)
    total_nodes = 100
    G = synthetic.latent_space_ring(total_nodes, 2, gauss_size=0.2)

    obs = synthetic.get_adj_mat(G)
    neurondf = synthetic.get_nodes_to_df(G)
    
    pickle.dump({'synapse_con_mat' : obs, 
                 'neurondf'  : neurondf}, 
                open(outfile, 'w'), -1)

@files(None, td("synfire_1d.cleandata.pickle"))
def generate_synfire_1d(_, outfile):
    np.random.seed(7)
    total_nodes = 100
    
    blocks = 8
    manual_weights = {}
    for i in range(blocks -1):
        manual_weights[(i, i+1)] = 0.85
        
    G = synthetic.stochastic_block_model(blocks, total_nodes, 
                                         manual_weights = manual_weights, 
                                         symmetric=False)

    obs = synthetic.get_adj_mat(G)
    neurondf = synthetic.get_nodes_to_df(G)
    
    pickle.dump({'synapse_con_mat' : obs, 
                 'neurondf'  : neurondf, 
                 'latent_params' : ['block']}, 
                open(outfile, 'w'), -1)

@files(None, td("block_sym.count.cleandata.pickle"))
def generate_block_sym_count(_, outfile):
    np.random.seed(4)
    blocks = 6
    total_nodes = 400
    alpha_beta = 5.0, 0.2
    G = synthetic.stochastic_block_model(blocks, total_nodes, 
                                         conn_alpha_beta = alpha_beta, 
                                         symmetric=True, 
                                         counts=True)
    
    obs = synthetic.get_adj_mat(G)
    neurondf = synthetic.get_nodes_to_df(G)
    
    pickle.dump({'synapse_con_mat' : obs, 
                 'neurondf'  : neurondf, 
                 'latent_params' : ['block']}, 
                open(outfile, 'w'), -1)

@files(None, td("block_asym.count.cleandata.pickle"))
def generate_block_asym_count(_, outfile):
    np.random.seed(7)
    blocks = 6
    total_nodes = 400
    alpha_beta = 3.0, 1.0
    G = synthetic.stochastic_block_model(blocks, total_nodes, 
                                         conn_alpha_beta = alpha_beta, 
                                         symmetric=False, 
                                         pmin = 0.1, 
                                         pmax = 100.0,
                                         counts=True)

    obs = synthetic.get_adj_mat(G)
    neurondf = synthetic.get_nodes_to_df(G)
    
    pickle.dump({'synapse_con_mat' : obs, 
                 'neurondf'  : neurondf, 
                 'latent_params' : ['block']}, 
                open(outfile, 'w'), -1)


@files(None, td("latentspace_ring_2d.count.cleandata.pickle"))
def generate_latentspace_ring_2d_count(_, outfile):
    np.random.seed(7)
    total_nodes = 100
    G = synthetic.latent_space_ring(total_nodes, 2, gauss_size=0.2)

    obs = synthetic.get_adj_mat(G)
    neurondf = synthetic.get_nodes_to_df(G)
    
    pickle.dump({'synapse_con_mat' : obs, 
                 'neurondf'  : neurondf}, 
                open(outfile, 'w'), -1)

@files(None, td("synfire_1d.count.cleandata.pickle"))
def generate_synfire_1d_count(_, outfile):
    np.random.seed(7)
    total_nodes = 100
    
    blocks = 8
    manual_weights = {}
    for i in range(blocks -1):
        manual_weights[(i, i+1)] = 0.85
        
    G = synthetic.stochastic_block_model(blocks, total_nodes, 
                                         manual_weights = manual_weights, 
                                         symmetric=False)

    obs = synthetic.get_adj_mat(G)
    neurondf = synthetic.get_nodes_to_df(G)
    
    pickle.dump({'synapse_con_mat' : obs, 
                 'neurondf'  : neurondf, 
                 'latent_params' : ['block']}, 
                open(outfile, 'w'), -1)


def ring_poisson_params():
    for c in [(-1,),  (0,),  (1,), 
              (-1, 1), ]:
        infile = None
        outfile = td("ring_poisson_%s.cleandata.pickle" % str(c))
        yield infile, outfile, c

@files(ring_poisson_params)
def ring_poisson(infile, outfile, conn_points):
    np.random.seed(0)

    N = 400
    G = synthetic.multiring_poisson(N, conn_points, 1.0, 0.2)

    obs = synthetic.get_adj_mat(G)
    neurondf = synthetic.get_nodes_to_df(G)
    
    pickle.dump({'synapse_con_mat' : obs, 
                 'neurondf'  : neurondf, 
                 'G' : G,
                 'latent_params' : ['theta']}, 
                open(outfile, 'w'), -1)


if __name__ == "__main__":
    pipeline_run([generate_block_sym, generate_block_asym, 
                  generate_latentspace_ring_2d, 
                  generate_synfire_1d, 
                  
                  generate_block_sym_count, 
                  generate_block_asym_count, 
                  ring_poisson], multiprocess=12)
