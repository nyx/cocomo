"""Methods for computing the resistance / path length
in an undirected graph. 

Our goal is to scale to ~30k nodes with each node having ~4 edges
in a tree structure. 


There are several methods for doing this:

- Traditional shortest-path algorithms (Dijkstra, etc.) just return a
  shortest path, which isn't exactly the same as resistance in
  non-tree graphs
- "Resistance" is the effective electrical resistance and is closer
  to what we're looking for. This can easily be computed from the
  pseudoinverse of the graph laplacian, but efficiently computing
  the pseudoinverse can be slow. Default implementations
  can have numerical stability issues. 
- We can do various cheats assuming the graph is a tree -- basically
  just walking it. 

"""

import networkx as nx
import numpy as np
import time as time
import scipy.sparse

from numba import jit

def ginv(X):
    """
    Compute the M-P pseudoinverse of a non-singular X, 
    used as part of qrginv

    """
    n, m = X.shape
    if n > m:
        C = np.dot(X.T, X)
        ginv = np.linalg.solve(C, X.T)
    else:
        C = np.dot(X, X.T)
        G = np.linalg.solve(C, X)
        ginv = G.T
        
    return ginv

def create_perm(idx):
    """
    Turn a list of indices into a permutation matrix. 

    Scipy's QR returns a permutation vector, we sometimes
    need a permutation matrix.
    """
    N = len(idx)
    P = np.zeros((N, N))
    for i in range(N):
        P[idx[i], i]= 1
    return P

def qrginv(B):
    """Compute the pseudoinverse of possibly-rectangular possibly-singular
    B. Can be directly applied to a graph laplacian. 

    Note that while this doesn't seem to be all that much faster 
    than np.linalg.pinv, it is MUCH more numerically stable. 

    This would also be considerably faster if python exposed a sparse
    QR decomposition but I looked everywhere and con't find it.
    


    Katsikis, V. N., Pappas, D., & Petralias, A. (2011). An improved
    method for the computation of the Moore-Penrose inverse
    matrix. Applied Mathematics and Computation, 217(23),
    9828-9834. doi:10.1016/j.amc.2011.04.080
    """
    N, M =  B.shape
    t1 = time.time()
    Q, R, Pidx = scipy.linalg.qr(B, pivoting=True)
    t2 = time.time()
    print "    qr took", t2-t1
    P = create_perm(Pidx)
    r = np.sum(np.any(np.abs(R) > 1e-13, 1))
    R1 = R[:r, :]
    R2 = ginv(R1)
    R3 = np.hstack([R2, np.zeros((M, N-r))])
    A = np.dot(P, np.dot(R3, Q.T))
    return A



def add_random_weights(G, inplace=False):
    """
    Add edge weights uniformly distributed on [1, 2]
    returns a copy of the original
    """
    if inplace:
        Gout = G
    else:
        Gout = G.copy()
    for e1, e2 in G.edges_iter():
        Gout[e1][e2]['weight'] = np.random.rand()+ 1.0

    return Gout

def random_binary_tree(levels):
    G = nx.balanced_tree(2, levels)
    return G


def path_to_root(n, Gdir, root):
    if n == root:
        return [root]
    else:
        predecessors = Gdir.predecessors(n)[0] # only one -- tree
        
        return path_to_root(predecessors, Gdir, root) + [n]


@jit(nopython=True)
def first_diff(p1, p2, min_len):
    for i in range(min_len):
        if p1[i] != p2[i]:
            return i
    return min_len

def graph_path_distance(G, root=None, safe=False):
    """
    given a weighted tree, return a dictionary of the distance
    between all nodes

    """
    if not nx.is_tree(G):
        raise ValueError("Graph is not a tree")

    if root is None:
        # pick a node, call it the root 
        root = G.nodes()[0]

    # copy the undirected graph and make it directed 
    # from the root 

    Gdir = nx.DiGraph()
    Gdir.add_nodes_from(G.nodes())

    for n1, n2 in nx.dfs_edges(G, root):
        Gdir.add_edge(n1, n2, weight=G[n1][n2]['weight'])

    
    assert nx.is_tree(Gdir)
    assert [n for n,d in Gdir.in_degree().items() if d==0]  == [root]

    N = len(G.nodes())

    # For each node, how far is it from root
    dvar = 'distance_from_root'
    Gdir.node[root][dvar] = 0
    for n1, n2 in nx.dfs_edges(Gdir, root):
        parent_dist =  Gdir.node[n1][dvar]
        #print n1, "->", n2, "dist to root=", parent_dist
        edge_weight = Gdir[n1][n2]['weight']
        Gdir.node[n2][dvar] = parent_dist + edge_weight

    # for eacn node, save its path to root
    all_paths = {}
    for n in Gdir.nodes_iter():
        all_paths[n] = np.array(path_to_root(n, Gdir, root))


    # walk all pairs of nodes and find their distance
    distances = {}
    for n1_i, n1 in enumerate(Gdir.nodes_iter()):
        if n1_i % 100 == 0:
            print n1_i, "of", N, "nodes"
        for n2 in Gdir.nodes_iter():
            p1 = all_paths[n1]
            p2 = all_paths[n2]
            min_len = min(len(p1), len(p2))
            max_len = max(len(p1), len(p2))

            differ_at = first_diff(p1, p2, min_len)
            #differ_at = min_len
            
            # for i in range(min_len):
            #     if p1[i] != p2[i]:
            #         differ_at = i
            #         break
            if differ_at < max_len:
                common_parent_node = p1[differ_at-1]
                # sanity check
                if safe:
                    assert (p1[:differ_at] == p2[:differ_at]).all()
                    assert (n1, n2) not in distances
                dist = Gdir.node[n1][dvar] + Gdir.node[n2][dvar] - 2*Gdir.node[common_parent_node][dvar]

                distances[(n1, n2)] = dist
                if safe:
                    if (n2, n1) in distances:

                        if distances[(n1, n2)] != distances[(n2, n1)]:
                            print "n1=", n1, "n2=", n2, "common_parent=", common_parent_node, "differ_at=", differ_at, 'min_len=', min_len
                            print Gdir.node[n1][dvar], Gdir.node[n2][dvar]
                            print Gdir.node[common_parent_node][dvar]
            else:
                distances[(n1, n2)] = 0


    # sanity check : is the distance between any node and root equal to its dist_from_root? 
    if safe:
        for n1 in Gdir.nodes_iter():
            assert distances[(root, n1)]== Gdir.node[n1][dvar]

    return distances

def compute_resistance(pinv):
    """
    Note pinv must be the pseudoinverse of the laplacian 
    of a graph of CONDUCTANCES, not resistances. 

    Gutman, Ivan, and W. Xiao. "Generalized inverse of the Laplacian
    matrix and some applications." Bulletin de l'Academie Serbe des
    Sciences at des Arts (Cl. Math. Natur.) 129 (2004): 15-23.

    equation 7
    
    """
    R = np.zeros(pinv.shape)
    for i in range(pinv.shape[0]):
        for j in range(pinv.shape[1]):
            R[i, j] = (pinv[i,i] + pinv[j, j] -  pinv[i, j] - pinv[j, i])
    
    return R


def resistance_to_conductance(G):
    """
    compute 1/weight for each ndoe
    """
    Gout = G.copy()
    for ei, ej in G.edges_iter():
        Gout[ei][ej]['weight'] = 1.0  / G[ei][ej]['weight']
    return Gout


@jit(nopython=True)
def compute_distances(node_to_pos, paths, path_lengths, values, safe=False):
    NODEN = len(paths)

    # walk all pairs of nodes and find their distance
    distances = np.zeros((NODEN, NODEN))
    
    for n1_i in xrange(NODEN):
        if n1_i % 100 == 0:
            print(n1_i)
            
        for n2_i in xrange(NODEN):
            p1 = paths[n1_i]
            p2 = paths[n2_i]
            min_len = min(path_lengths[n1_i], path_lengths[n2_i])
            max_len = max(path_lengths[n1_i], path_lengths[n2_i])

            differ_at = first_diff(p1, p2, min_len)

            if differ_at < max_len:
                common_parent_node = p1[differ_at-1]
                dist = values[n1_i] + values[n2_i] - 2*values[node_to_pos[common_parent_node]]
                distances[n1_i, n2_i] = dist
            else:
                distances[n1_i, n2_i] = 0
    return distances

def graph_path_distance_fast(G, root=None, safe=False, weight_var='weight'):
    """
    just like graph_path_distance but has a numba inner loop

    """
    if not nx.is_tree(G):
        raise ValueError("Graph is not a tree")

    if root is None:
        # pick a node, call it the root 
        root = G.nodes()[0]

    # copy the undirected graph and make it directed 
    # from the root 

    Gdir = nx.DiGraph()
    Gdir.add_nodes_from(G.nodes())

    for n1, n2 in nx.dfs_edges(G, root):
        Gdir.add_edge(n1, n2, weight=G[n1][n2][weight_var])

    
    assert nx.is_tree(Gdir)
    assert [n for n,d in Gdir.in_degree().items() if d==0]  == [root]

    N = len(G.nodes())

    # For each node, how far is it from root
    dvar = 'distance_from_root'
    Gdir.node[root][dvar] = 0
    for n1, n2 in nx.dfs_edges(Gdir, root):
        parent_dist =  Gdir.node[n1][dvar]
        #print n1, "->", n2, "dist to root=", parent_dist
        edge_weight = Gdir[n1][n2]['weight']
        Gdir.node[n2][dvar] = parent_dist + edge_weight

    # for eacn node, save its path to root
    longest_path = 0
    all_paths = {}
    for n in Gdir.nodes_iter():
        all_paths[n] = np.array(path_to_root(n, Gdir, root))
        longest_path = max(longest_path, len(all_paths[n]))
    print "THE LONGEST PATH IS", longest_path
    
    ### Abandon any graphx stuff
    all_nodes = np.array(Gdir.nodes())
    NODEN = len(all_nodes)
    node_to_pos = {k : v for v, k in enumerate(all_nodes)}
    node_to_pos_dict = node_to_pos.copy()
    # can we turn nodetopos into an array
    if np.min(node_to_pos.keys()) >= 0 and np.max(node_to_pos.keys()) < 1000000:
        node_to_pos_key_vect = np.zeros(np.max(node_to_pos.keys()) + 1, dtype=int)
        for k, v in node_to_pos.iteritems():
            node_to_pos_key_vect[k] = v
        node_to_pos = node_to_pos_key_vect
    
    # path matrix:
    paths = np.zeros((NODEN, longest_path + 1), dtype=int)
    path_lengths = np.zeros(NODEN, dtype=int)
    values = np.zeros(NODEN, dtype=float)
    for ni, n in enumerate(all_nodes):
        p = all_paths[n]
        path_lengths[ni] = len(p)
        paths[ni, :len(p)] = p
        values[ni] = Gdir.node[n][dvar]
    
    # walk all pairs of nodes and find their distance
    print "Running compute_distances", type(node_to_pos), type(paths), type(path_lengths), type(values)
    print node_to_pos.dtype, paths.dtype, path_lengths.dtype, values.dtype
    distances_array = compute_distances(node_to_pos, paths, path_lengths, values)

    
    # sanity check : is the distance between any node and root equal to its dist_from_root? 
    if safe:
        for n1 in Gdir.nodes_iter():
            assert distances[(root, n1)]== Gdir.node[n1][dvar]

    return distances_array, node_to_pos_dict


def dist_mat_to_dict(distmat, node_to_pos):
    distances = {}

    for n1, n1_i in node_to_pos.iteritems():
        for n2, n2_i in node_to_pos.iteritems():
            
            distances[(n1, n2)] = distances_array[n1_i, n2_i]
    return distances
