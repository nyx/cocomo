import numpy as np
import cPickle as pickle
import matplotlib
matplotlib.use('Agg')
from matplotlib import pylab
from ruffus import * 
import pandas
import sqlite3
import os
import pandas as pd


CELEGANS_DB='neurodata.preprocess/celegans_herm/celegans_herm.db'


DATA_DIR = "data"

def td(x):
    return os.path.join(DATA_DIR, x)


def create_adj_mat(con, synapse_type, cell_data):
    """
    returns (upper-triangular) contact area matrix, cell_ids in order

    """
    

    n = pandas.io.sql.read_frame("select * from Cells order by soma_pos", 
                                 con, index_col="cell_id" )
    n['index'] = np.arange(len(n), dtype=np.int)
    
    s = pandas.io.sql.read_frame("select * from Synapses", 
                                 con)
    idx = n['index']
    print idx
    s['from_idx'] = np.array(idx[s['from_id']])
    s['to_idx'] = np.array(idx[s['to_id']])
    

    sub = s[s['synapse_type'] == synapse_type]

    conn_mat = np.zeros((len(n), len(n)), dtype=np.float32)
    for r_i, r in sub.iterrows():
        
        conn_mat[r['from_idx'], r['to_idx']] =  r['count']

    return conn_mat, cell_data.index.values


# @files(MOUSE_RETINA_DB, "adjmat.byid.png")
# def plot_adj(infile, outfile):
#     """
    
#     """

#     con = sqlite3.connect(infile)
#     cell_data = pandas.io.sql.read_frame("select * from cells order by cell_id", 
#                                          con, index_col="cell_id")


#     area_mat, cell_ids = create_adj_mat(con, PAPER_MAX_CONTACT_AREA, cell_data)

#     CELL_N = len(cell_ids)

#     lower_triangular_idx = np.tril_indices(CELL_N)

#     assert area_mat[lower_triangular_idx].sum() == 0

#     area_mat += area_mat.T


#     p = np.random.permutation(CELL_N)
#     area_mat_p = area_mat[p, :]
#     area_mat_p = area_mat_p[:, p]

#     f = pylab.figure(figsize=(8, 8))
#     ax = f.add_subplot(1, 1, 1)

#     ax.imshow(area_mat_p  > 0.2, interpolation='nearest', 
#               cmap=pylab.cm.Greys)
    
#     f.savefig(outfile, dpi=600)


@files(CELEGANS_DB, td("celegans.elec.cleandata.pickle"))
def filter_and_mat_elec(infile, outfile):

    con = sqlite3.connect(infile)
    cell_data = pd.io.sql.read_sql("select * from Cells",
                                   con, index_col='cell_id')


    conn_mat, cell_ids = create_adj_mat(con, 'E', cell_data)
    print cell_data.dtypes

    pickle.dump({"synapse_con_mat" : conn_mat,
                 'neurondf' : cell_data, 
                'latent_params' : ['cell_class', 'soma_pos', 
                                   'role', 'neurotransmitters']}, 
                 open(outfile, 'w'), -1)

@files(CELEGANS_DB, td("celegans.chem.cleandata.pickle"))
def filter_and_mat_chem(infile, outfile):

    con = sqlite3.connect(infile)
    cell_data = pd.io.sql.read_sql("select * from Cells",
                                   con, index_col='cell_id')


    conn_mat, cell_ids = create_adj_mat(con, 'C', cell_data)
    print cell_data.dtypes

    pickle.dump({"synapse_con_mat" : conn_mat,
                 'neurondf' : cell_data, 
                'latent_params' : ['cell_class', 'soma_pos', 'role', 
                                   'neurotransmitters']}, 
                 open(outfile, 'w'), -1)

@files(CELEGANS_DB, td("celegans.elec_bin.cleandata.pickle"))
def filter_and_mat_elec_bin(infile, outfile):

    con = sqlite3.connect(infile)
    cell_data = pd.io.sql.read_sql("select * from Cells",
                                   con, index_col='cell_id')


    conn_mat, cell_ids = create_adj_mat(con, 'E', cell_data)
    print cell_data.dtypes

    pickle.dump({"synapse_con_mat" : (conn_mat > 0).astype(float),
                 'neurondf' : cell_data, 
                'latent_params' : ['cell_class', 'soma_pos', 
                                   'role', 'neurotransmitters']}, 
                 open(outfile, 'w'), -1)

@files(CELEGANS_DB, td("celegans.chem_bin.cleandata.pickle"))
def filter_and_mat_chem_bin(infile, outfile):

    con = sqlite3.connect(infile)
    cell_data = pd.io.sql.read_sql("select * from Cells",
                                   con, index_col='cell_id')


    conn_mat, cell_ids = create_adj_mat(con, 'C', cell_data)
    print cell_data.dtypes

    pickle.dump({"synapse_con_mat" : (conn_mat> 0).astype(float),
                 'neurondf' : cell_data, 
                'latent_params' : ['cell_class', 'soma_pos', 'role', 
                                   'neurotransmitters']}, 
                 open(outfile, 'w'), -1)

if __name__ == "__main__":
    pipeline_run([filter_and_mat_elec, filter_and_mat_chem,
                  filter_and_mat_elec_bin, filter_and_mat_chem_bin, 
    ])
