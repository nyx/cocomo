import simplejson
from ruffus import * 
import os
import pandas as pd
import copy
import cPickle as pickle
import numpy as np
import sklearn.metrics, sklearn.neighbors
import scipy.sparse
import exputil
import json

DATA_DIR = "data"

def td(x):
    return os.path.join(DATA_DIR, x)

@mkdir(DATA_DIR)
@files("../data.original/mushroombody/annotations-body.json", 
       td("mushroombody.neurondf.pickle"))
def load_neurons(infile, outfile):

    neuron_json  = simplejson.load(open(infile))
    neurondf = pd.DataFrame(neuron_json['data'])

    neurondf['idx'] = np.arange(len(neurondf))

    pickle.dump({'neurondf' : neurondf}, 
                open(outfile, 'w'), -1)

                 

@files("../data.original/mushroombody/synapse.json", 
       td("mushroombody.synapsesdf.pickle"))
def load_synapses(infile, outfile):
    
    synapse_json  = simplejson.load(open(infile, 'r'))
    synapse_data = synapse_json['data']

    
    # turn this into two dataframes that can be joined
    tbars = []
    connections = []

    for tbar_i, tbar in enumerate(synapse_data):
        tbar_info = copy.deepcopy(tbar['T-bar'])

        tbar_info['id'] = int(tbar_i)
        tbar_info['loc_x'] = int(tbar_info['location'][0])
        tbar_info['loc_y'] = int(tbar_info['location'][1])
        tbar_info['loc_z'] = int(tbar_info['location'][2])
        del tbar_info['location']
        tbars.append(tbar_info)

        for p in tbar['partners']:
            p['loc_x'] = p['location'][0]
            p['loc_y'] = p['location'][1]
            p['loc_z'] = p['location'][2]
            p['tbar_id']= int(tbar_i)
            del p['location']
            connections.append(p)


    tbar_df = pd.DataFrame(tbars)
    connections_df = pd.DataFrame(connections)


    pickle.dump({'tbar_df' : tbar_df, 
                 'connections_df' : connections_df}, 
                open(outfile, 'w'), -1)


@files([load_neurons, load_synapses], td("mushroombody.cleandata.pickle"))
def filter_and_mat((infile_neurons, infile_synapses), outfile):
    n = pickle.load(open(infile_neurons, 'r'))
    s = pickle.load(open(infile_synapses, 'r'))
    
    neurondf = n['neurondf']

    neurondf_subset = neurondf.dropna(subset=['name']) # drop all unnamed cells
    neurondf_subset['idx'] = np.arange(len(neurondf_subset))
    neurondf_subset.index = neurondf_subset['body ID']
    print "len(neurondf_subset)", len(neurondf_subset)

    tbar_df = s['tbar_df']
    connections_df = s['connections_df']
    print len(connections_df)
    
    dfmerged = pd.merge( connections_df, tbar_df, left_on='tbar_id', right_on='id')
    

    conns = dfmerged.groupby(['body ID_x', 'body ID_y']).size()
    conns = conns.reset_index()
    print len(conns)

    neuronlist = neurondf_subset.index.values.tolist()


    con_sub = conns[conns['body ID_y'].isin(neuronlist)]
    con_sub = con_sub[con_sub['body ID_x'].isin(neuronlist)]

    print "Creating a ", len(neuronlist), "by", len(neuronlist), "matrix"
    mat = np.zeros((len(neuronlist), len(neuronlist)), dtype=np.int16)

    for row_i, row in con_sub.iterrows():
        pre = row['body ID_x']
        post = row['body ID_y']
        pre_idx = neurondf_subset.get_value(pre, 'idx')
        post_idx = neurondf_subset.get_value(post, 'idx')
        #print pre_idx, post_idx
        if mat[pre_idx, post_idx] != 0:
            print "whoops"
        print pre_idx, post_idx, row[0]
        mat[pre_idx, post_idx] = row[0]
    pickle.dump({'synapse_con_mat' : mat, 
                 'neurondf'  : neurondf_subset}, 
                open(outfile, 'w'), -1)
                 

@files([load_neurons, load_synapses], td("mushroombody.KC-MBON.rosette.df.pickle"))
def create_rosette_kc_data((infile_neurons, infile_synapses), outfile):
    neurons_data = pickle.load(open(infile_neurons, 'r'))
    synapse_data = pickle.load(open(infile_synapses, 'r'))
    
    
    tbar_df = synapse_data['tbar_df']
    connections_df = synapse_data['connections_df']
    neurons_df = neurons_data['neurondf']

    dfmerged = pd.merge( connections_df, tbar_df, left_on='tbar_id', right_on='id', suffixes=('_post', '_pre'))
    dfmerged.head()
    
    neuronsdf = neurons_df.set_index('body ID')

    vc = dfmerged['body ID_post'].value_counts()
    neuronsdf['post_syn_count'] = vc

    neuronsdf['name_clean'] = neuronsdf.name.apply(lambda x: str(x))

    kcs_df = neuronsdf[neuronsdf.name_clean.str.contains('KC')]
    kcs_bodyids = kcs_df.index.values
    # srini says only convergent-annotated synapses should be used


    #res = []
    rosette_res = []

    for convergent in [False, True]:
        if convergent:
            kcs_pre_df = dfmerged[(dfmerged['body ID_pre'].isin(kcs_bodyids)) & (dfmerged['convergent'] == 'convergent')]
        else:
            kcs_pre_df = dfmerged[(dfmerged['body ID_pre'].isin(kcs_bodyids))]

        for ni, n in neuronsdf[neuronsdf.name_clean.str[:4] == 'MBON'].iterrows():
            for thold in [10, 20, 30, 40]:
                mbons_post = kcs_pre_df[(kcs_pre_df['body ID_post'] == ni) ].copy()

                postsyn_vector = np.zeros((len(mbons_post), 3))
                postsyn_vector[:, 0] = mbons_post.loc_x_post
                postsyn_vector[:, 1] = mbons_post.loc_y_post
                postsyn_vector[:, 2] = mbons_post.loc_z_post
                ng = sklearn.neighbors.radius_neighbors_graph(postsyn_vector, thold, include_self=True)
                cc = scipy.sparse.csgraph.connected_components(ng)[1]
                #unique = np.array(pd.value_counts(cc))

                mbons_post['rosette'] = cc
                mbons_post['rosette_thold'] = thold
                mbons_post['post_name'] = n.name_clean
                mbons_post['body ID_post'] == ni
                mbons_post['convergent_filtered'] = convergent

                rosette_res.append(mbons_post)
                a = pd.value_counts(pd.value_counts(cc))
                a.index.name = "cluster_size"
                d = pd.DataFrame(a).reset_index()
                d.columns = ['cluster_size', 'count']
                d['thold'] = thold
                d['post_name'] = n.name_clean
                d['body ID_post'] = ni
                
                #res.append(d)
    #cluster_sizes = pd.concat(res)

    rosette_df = pd.concat(rosette_res).reset_index()
    rosette_df['rosette_id'] = rosette_df.apply(lambda x : "%s:%d" % (x.post_name, 
                                                                      x.rosette), 
                                                axis=1)

    pickle.dump({'rosette_df' : rosette_df},
                open(outfile, 'w'), -1)
    
                 
@files(create_rosette_kc_data, 
       td('mushroombody.KC-MBON.rosette.mats.pickle'))
def create_rosette_kc_mats(infile, outfile):
    
    rosette_df = pickle.load(open(infile, 'r'))['rosette_df']

    res = {}

    for thold, rosette_th in rosette_df.groupby('rosette_thold'):

        rosette_ids = np.unique(rosette_th.rosette_id)

        body_ids_pre = np.unique(rosette_th['body ID_pre'])

        # construct the matrix
        mat = np.zeros((len(rosette_ids), len(body_ids_pre)), dtype=int)

        # mapping
        body_id_dict = {k: v for v, k in enumerate(body_ids_pre)}
        rosette_id_dict = {k: v for v, k in enumerate(rosette_ids)}

        for ri, r in rosette_th.iterrows():
            rosette_pos = rosette_id_dict[r.rosette_id]
            body_id_pos = body_id_dict[r['body ID_pre']]
            mat[rosette_pos, body_id_pos] += 1

        res[thold] = {'rosette_th' : rosette_th, 
                      'body_ids_pre' : body_ids_pre, 
                      'rosette_ids' : rosette_ids, 
                      'mat' : mat, 
                      'body_id_dict' : body_id_dict, 
                      'rosette_id_dict' : rosette_id_dict}
    pickle.dump({'mats' : res}, 
                open(outfile, 'w'), -1)


@files(create_rosette_kc_data, 
       td('mushroombody.KC-MBON.rosette.tensors.pickle'))
def create_rosette_kc_tensors(infile, outfile):
    
    rosette_df = pickle.load(open(infile, 'r'))['rosette_df']

    res = {}

    for thold, rosette_th in rosette_df.groupby('rosette_thold'):

        rosette_ids = np.unique(rosette_th.rosette_id)

        body_ids_pre = np.unique(rosette_th['body ID_pre'])
        body_ids_post = np.unique(rosette_th['body ID_post'])

        # construct the matrix

        tensor = {}
        # mapping
        body_id_pre_dict = {k: v for v, k in enumerate(body_ids_pre)}
        body_id_post_dict = {k: v for v, k in enumerate(body_ids_post)}
        rosette_id_dict = {k: v for v, k in enumerate(rosette_ids)}

        for ri, r in rosette_th.iterrows():
            rosette_pos = rosette_id_dict[r.rosette_id]
            body_id_pre = body_id_pre_dict[r['body ID_pre']]
            body_id_post = body_id_post_dict[r['body ID_post']]
            key = (rosette_pos, body_id_pre, body_id_post) 
            if key not in tensor:
                tensor[key] = 0
            tensor[key] +=1

        res[thold] = {'rosette_th' : rosette_th, 
                      'body_ids_pre' : body_ids_pre, 
                      'body_ids_post' : body_ids_post, 
                      'rosette_ids' : rosette_ids, 
                      'tensor' : tensor, 
                      'body_id_pre_dict' : body_id_pre_dict, 
                      'body_id_post_dict' : body_id_post_dict, 
                      'rosette_id_dict' : rosette_id_dict}
    pickle.dump({'tensors' : res}, 
                open(outfile, 'w'), -1)



@files(create_rosette_kc_data, 
       td('mushroombody.KC-rosette-KC.mats.pickle'))
def create_kc_kc_mats(infile, outfile):

    neurons_data = pickle.load(open(td("mushroombody.neurondf.pickle"), 'r'))

    kc_mbon_rosettes = pickle.load(open(infile, 'r'))
    rosette_df = kc_mbon_rosettes['rosette_df']

    neurons_df = neurons_data['neurondf']

    pre_ids_unique = np.unique(rosette_df['body ID_pre'])
    pre_ids_to_pos = {k:v for v, k in enumerate(pre_ids_unique)}
    KCN = len(pre_ids_unique)


    # get the unique ID names in order

    pre_ids_names = []

    for pi in pre_ids_unique:
        a = neurons_df[neurons_df['body ID'] == pi].iloc[0]
        pre_ids_names.append(a['name'])
    pre_ids_names = np.array(pre_ids_names)
    #rand_idx = np.argsort(pre_ids_names)# np.random.permutation(KCN)
    #pre_ids_names_sorted = pre_ids_names[rand_idx]

    all_results = {}

    for (rosette_thold, convergent_filtered),  rosettes_th in rosette_df.groupby(['rosette_thold', 'convergent_filtered']):



        kc_rosette_kc_mats = {}
        kc_kc_mats = {}
        MAX_ROSETTE_SIZE = 20
        for post_name, a in  rosettes_th.groupby('post_name'):


            conn_mat = np.zeros((KCN, KCN, MAX_ROSETTE_SIZE), dtype=np.uint16)

            for rosette_id, rosette in a.groupby('rosette_id'):
                pre_ids = np.unique(rosette['body ID_pre'])
                pos = [pre_ids_to_pos[pre_id] for pre_id in pre_ids]
                #if len(rosette) > 1
                rosette_size = len(rosette)
                for i in pos:
                    for j in pos:
                        if i != j: # ignore same-same
                            conn_mat[i, j, rosette_size] += 1 
            kc_rosette_kc_mats[post_name] = conn_mat

            # now kc-kc ignoring rosette
            kc_kc_conn_mat = np.zeros((KCN, KCN))
            allpos = [pre_ids_to_pos[i] for i in a['body ID_pre'].value_counts().index.values]
            for i in allpos:
                for j in allpos:
                    kc_kc_conn_mat[i, j] = 1
            kc_kc_mats[post_name] = kc_kc_conn_mat
        
        d = {'kc_kc_mats': kc_kc_mats, 
             'kc_rosette_kc_mats' : kc_rosette_kc_mats}

        all_results[(rosette_thold, convergent_filtered)] = d

            
    pickle.dump({'pre_ids_order' : pre_ids_unique, 
                 'pre_ids_to_pos' : pre_ids_to_pos, 
                 'all_results' : all_results}, 
                open(outfile, 'w'), -1)


def kc_kc_binary_cv_params():

    d = pickle.load(open(td("mushroombody.KC-rosette-KC.mats.pickle"), 'r'))

    a = d['all_results'][(20, True)]
    kc_kc_mats = a['kc_kc_mats']
    kc_rosette_kc_mats = a['kc_rosette_kc_mats']
    MBON_names = kc_rosette_kc_mats.keys()
    nfold = 10
    seed = 0

    for thold in [20, 40]:
        for convergent_only in [True, False]:
            for m in MBON_names:
                mat_infile = td("mushroombody.KC-rosette-KC.mats.pickle")
                infile = td("mushroombody.KC-MBON.rosette.df.pickle")
                if convergent_only:
                    convergent_int = 1
                else:
                    convergent_int = 0
                outfile = td("KC-rosette-KC.{:s}.{:d}.{:d}.cv_{:d}.pickle".format(m, thold, convergent_int, nfold))      
                yield (mat_infile, infile), outfile, m, nfold, thold, convergent_only, seed


@follows(create_rosette_kc_mats)
@follows(create_kc_kc_mats)
@files(kc_kc_binary_cv_params)
def create_kc_kc_binary_cv_params((mat_infile, infile), 
                                  outfile, mbon_name, nfold, 
                                  thold, convergent_only, seed):
    """
    For each MBON, create a kc-kc binary connectivity matrix 
    """
    THRESHOLD = thold
    CONVERGENT = convergent_only

    kc_mbon_rosettes = pickle.load(open(infile, 'r'))
    rosette_df = kc_mbon_rosettes['rosette_df']


    d = pickle.load(open(mat_infile, 'r'))

    a = d['all_results'][(THRESHOLD, CONVERGENT)]

    kc_kc_mats = a['kc_kc_mats']
    kc_rosette_kc_mats = a['kc_rosette_kc_mats']
    pre_ids_to_pos = d['pre_ids_to_pos']
    pre_ids_order = d['pre_ids_order']

    KCN = len(pre_ids_order)

    np.random.seed(seed)


    rosette_mat = kc_rosette_kc_mats[mbon_name]
    conn_mat = rosette_mat[:, :, 2:].sum(axis=2)
    conmat = (conn_mat > 0).astype(np.uint8)
    print "there are", np.sum(conn_mat.astype(float)), "ones" 
    
    cv_mask_set = exputil.create_cv_mask_set(conmat.shape, nfold)

    pickle.dump({'conmat' : conmat, 
                 'infile' : infile, 
                 'mat_infile' : mat_infile, 
                 'rosette_thold' : THRESHOLD, 
                 'convergent_only' : CONVERGENT, 
                 'pre_ids_to_pos' : pre_ids_to_pos, 
                 'pre_ids_order' : pre_ids_order, 
                 'cvs' : cv_mask_set}, 
                open(outfile, 'w'), -1)


def kc_kc_tensor_binary_cv_params():

    d = pickle.load(open(td("mushroombody.KC-rosette-KC.mats.pickle"), 'r'))

    a = d['all_results'][(20, True)]
    kc_kc_mats = a['kc_kc_mats']
    kc_rosette_kc_mats = a['kc_rosette_kc_mats']
    MBON_name_sets = {'all' : kc_rosette_kc_mats.keys(), 
                      'main_mbon' : ['MBON-14-A', 'MBON-14-B', 
                                     'MBON-18', 'MBON-07-A', 'MBON-07-B', 
                                     'MBON-X', 
                                     'MBON-19-A', 'MBON-19-B']
    }

    nfold = 10
    seed = 0

    for thold in [20]:
        for convergent_only in [True, False]:
            for m_set_name, m_set in MBON_name_sets.iteritems():
                mat_infile = td("mushroombody.KC-rosette-KC.mats.pickle")
                infile = td("mushroombody.KC-MBON.rosette.df.pickle")
                if convergent_only:
                    convergent_int = 1
                else:
                    convergent_int = 0
                outfile = td("KC-rosette-KC.{:s}.{:d}.{:d}.cv_{:d}.pickle".format(m_set_name, thold, convergent_int, nfold))      
                yield (mat_infile, infile), outfile, m_set_name, m_set, nfold, thold, convergent_only, seed


@follows(create_rosette_kc_mats)
@follows(create_kc_kc_mats)
@files(kc_kc_tensor_binary_cv_params)
def create_kc_kc_tensor_binary_cv((mat_infile, infile), 
                                  outfile, m_set_name, m_set, nfold, 
                                  thold, convergent_only, seed):
    """
    For each MBON, create a kc-kc binary connectivity matrix 
    """
    print "Creating kc-kc tensor binary for mbon set={} convergent_only={}".format(m_set_name, convergent_only)
    THRESHOLD = thold
    CONVERGENT = convergent_only

    kc_mbon_rosettes = pickle.load(open(infile, 'r'))
    rosette_df = kc_mbon_rosettes['rosette_df']


    d = pickle.load(open(mat_infile, 'r'))

    a = d['all_results'][(THRESHOLD, CONVERGENT)]

    kc_kc_mats = a['kc_kc_mats']
    kc_rosette_kc_mats = a['kc_rosette_kc_mats']
    pre_ids_to_pos = d['pre_ids_to_pos']
    pre_ids_order = d['pre_ids_order']

    KCN = len(pre_ids_order)

    np.random.seed(seed)

    MBON_N = len(m_set)
    ROSETTE_AT_LEAST = 2
    out_mat_shape = kc_rosette_kc_mats[kc_rosette_kc_mats.keys()[0]].shape
    conmat = np.zeros((MBON_N, out_mat_shape[0], out_mat_shape[1]), 
                      dtype=np.uint8)
    cv_mask_set = np.zeros((nfold, MBON_N, out_mat_shape[0], out_mat_shape[1]), 
                           dtype = np.uint8)
    for i in range(MBON_N):

        mbon_name = m_set[i]
        # now put all the rosette masks together
        rosette_mat = kc_rosette_kc_mats[mbon_name]
        
        conn_mat = rosette_mat[:, :, ROSETTE_AT_LEAST:].sum(axis=2)
        conmat[i] = conn_mat > 0

        print "there are", np.sum(conn_mat.astype(float)), "ones" 
    

        cv_mask_set[:, i] = exputil.create_cv_mask_set(out_mat_shape, nfold)

    pickle.dump({'conmat' : conmat, 
                 'infile' : infile, 
                 'mat_infile' : mat_infile, 
                 'm_set_name' : m_set_name, 
                 'm_set' : m_set, 
                 'rosette_thold' : THRESHOLD, 
                 'convergent_only' : CONVERGENT, 
                 'pre_ids_to_pos' : pre_ids_to_pos, 
                 'pre_ids_order' : pre_ids_order, 
                 'cvs' : cv_mask_set}, 
                open(outfile, 'w'), -1)


def kc_kc_tensor_kcsubtype_binary_cv_params():
    """
    Filter through the others for cell subtypes
    """
    
    CELL_SUBTYPE_QUERIES = {'KC-s' : "name_clean == 'KC-s'",
                            'KC-c' : "name_clean == 'KC-p'",
                            'KC-p' : "name_clean == 'KC-c'",
                            'KC-any' : "name_clean == 'KC-any'",
    }


    for params in kc_kc_tensor_binary_cv_params():
        
        last_outfile = params[1]

        infile = last_outfile
        for subtype_name, subtype_query in CELL_SUBTYPE_QUERIES.iteritems():
            outfile =last_outfile.replace(".pickle", ".{}.pickle".format(subtype_name))

            yield infile, outfile, subtype_name, subtype_query


@follows(create_kc_kc_tensor_binary_cv)
@files(kc_kc_tensor_kcsubtype_binary_cv_params)
def create_kc_kc_tensor_kcsubtype_binary_cv(infile, outfile, subtype_name, subtype_query):
    print "creating", outfile

    neurons_data = pickle.load(open(td("mushroombody.neurondf.pickle"), 'r'))
    neurons_df = neurons_data['neurondf']
    neuronsdf = neurons_df.set_index('body ID')
    neuronsdf['name_clean'] = neuronsdf.name.apply(lambda x: str(x))

    d = pickle.load(open(infile, 'r'))

    conmat = d['conmat']
    cvs = d['cvs']
    pre_ids_to_pos = d['pre_ids_to_pos']
    pre_ids_order = d['pre_ids_order']

    neurons_subset = neuronsdf.ix[pre_ids_order].copy()
    neurons_subset['pos'] = np.arange(len(neurons_subset))

    our_subset = neurons_subset.query(subtype_query)

    new_pre_ids_order = pre_ids_order[our_subset.pos]
    new_conmat = conmat[:, our_subset.pos, :]
    new_conmat = new_conmat[:, :, our_subset.pos].copy()
    
    new_cvs = cvs[:,:, our_subset.pos, :]
    new_cvs = new_cvs[:, :, :, our_subset.pos].copy()

    new_pre_ids_to_pos = {k : v for v, k in enumerate(new_pre_ids_order)}

    d['conmat'] = new_conmat
    d['cvs'] = new_cvs
    d['pre_ids_to_pos'] = new_pre_ids_to_pos
    d['pre_ids_order'] = new_pre_ids_order
    d['subtype_name'] = subtype_name
    d['subtype_query'] = subtype_query

    pickle.dump(d, open(outfile, 'w'), -1)


@files([load_neurons, load_synapses], td("mushroombody.all-MBON.rosette.df.pickle"))
def create_all_rosette_data((infile_neurons, infile_synapses), outfile):
    """
    Create rosette data for all MBON neurons regardless of the presynaptic
    cell type (filter for that later!) 
    
    Previous attempts were kc-only
    
    
    """
    neurons_data = pickle.load(open(infile_neurons, 'r'))
    synapse_data = pickle.load(open(infile_synapses, 'r'))
    
    
    tbar_df = synapse_data['tbar_df']
    connections_df = synapse_data['connections_df']
    neurons_df = neurons_data['neurondf']

    dfmerged = pd.merge( connections_df, tbar_df, left_on='tbar_id', right_on='id', suffixes=('_post', '_pre'))
    dfmerged.head()
    
    neuronsdf = neurons_df.set_index('body ID')

    vc = dfmerged['body ID_post'].value_counts()
    neuronsdf['post_syn_count'] = vc

    neuronsdf['name_clean'] = neuronsdf.name.apply(lambda x: str(x))

    #res = []
    rosette_res = []

    rosette_group = 0
    for convergent in [False, True]:
        if convergent:
            pre_df = dfmerged[(dfmerged['convergent'] == 'convergent')]
        else:
            pre_df = dfmerged
        for ni, n in neuronsdf[neuronsdf.name_clean.str[:4] == 'MBON'].iterrows():
            for thold in [10, 20, 30, 40]:
                mbons_post = pre_df[(pre_df['body ID_post'] == ni) ].copy()

                postsyn_vector = np.zeros((len(mbons_post), 3))
                postsyn_vector[:, 0] = mbons_post.loc_x_post
                postsyn_vector[:, 1] = mbons_post.loc_y_post
                postsyn_vector[:, 2] = mbons_post.loc_z_post

                ng = sklearn.neighbors.radius_neighbors_graph(postsyn_vector, thold, include_self=True)
                cc = scipy.sparse.csgraph.connected_components(ng)[1]
                #unique = np.array(pd.value_counts(cc))

                mbons_post['rosette'] = cc
                mbons_post['rosette_thold'] = thold
                mbons_post['post_name'] = n.name_clean
                mbons_post['body ID_post'] == ni
                mbons_post['convergent_filtered'] = convergent
                mbons_post['rosette_id'] = mbons_post.apply(lambda x: "%s:%d:%s" % (n.name_clean, rosette_group, x.rosette), axis=1)

                rosette_res.append(mbons_post)
                
                rosette_group += 1    # make sure we can always group by rosette
                #res.append(d)
    #cluster_sizes = pd.concat(res)

    rosette_df = pd.concat(rosette_res).reset_index()


    pickle.dump({'rosette_df' : rosette_df},
                open(outfile, 'w'), -1)


@files([load_neurons, create_all_rosette_data], td("mushroombody.all-MBON.rosette.json.sentinel"))
def rosette_all_to_json((infile_neurons, infile_rosette), outfile):

    neurons_data = pickle.load(open(infile_neurons, 'r'))
    neurons_df = neurons_data['neurondf']
    neuronsdf = neurons_df.set_index('body ID')
    neuronsdf['name_clean'] = neuronsdf.name.apply(lambda x: str(x))


    rosette_df = pickle.load(open(infile_rosette, 'r'))['rosette_df']
    
    for (thold, convergent), subdf in rosette_df.groupby(['rosette_thold', 'convergent_filtered']):
        for mbon_id, mbon_df in subdf.groupby('body ID_post'):
            rosette_res = {}
            for rosette_id, rosette in mbon_df.groupby('rosette_id'):
                rosette_res[rosette_id] = [{'tbar_id': r.tbar_id, 
                                            'location' : [r.loc_x_pre, 
                                                          r.loc_y_pre, 
                                                          r.loc_z_pre], 
                                             'body ID' : r['body ID_pre']} for ri, r in rosette.iterrows()]
            out_filename = "rosette.{}.{}.{}.{}.json".format(mbon_id, 
                                                       neuronsdf.ix[mbon_id].name_clean, 
                                                             thold, convergent)
            json.dump(rosette_res, 
                      open(td(out_filename), 'w'))
    open(outfile, 'w')


@transform(td("rosette.*.*.*.*.json"), 
           suffix(".json"), ".validated.txt")
def validate_rosette_json(infile, outfile):
    """

    FIXME takes impossibly long
    """
    synapse_file = "../data.original/mushroombody/synapse.json"

    synapse_json  = json.load(open(synapse_file, 'r'))
    synapse_data = synapse_json['data']
    print infile.split(".")
    base, mbon_id, mbon_name, thold, convergent, extension = infile.split(".")
    thold = int(thold)
    only_convergent = convergent == "True"
    print "only convergent? : ", convergent
    rosette_data = json.load(open(infile, 'r'))
    outfid = open(outfile, 'w')

    bad_count = 0
    for rosette_i, rosette in rosette_data.iteritems():
        # yes this is slow, but the hope is it is also fool-proof
        tgt_tbars = {}
        tgt_tbar_partners = {}
        for ri, rosette_member in enumerate(rosette):
            #print "ri=", ri
            tgt_tbars[ri] = []
            tgt_tbar_partners[ri] = []

            # now we iterate through all of the synapse data
            # and look for tbars from the same cell that are
            # at the listed location
            for tbar_i, tbar in enumerate(synapse_data):
                if tbar['T-bar']['body ID'] == rosette_member['body ID']:
                    is_convergent = ('convergent' in tbar['T-bar'])
                    if only_convergent and is_convergent == False :                
                        #print "tbar keys=", tbar['T-bar'].keys()
                        #print "tbar['convergent'] =", tbar['T-bar']['convergent']
                        continue

                    tbar_loc = tbar['T-bar']['location']
                    if tbar_loc == rosette_member['location']:
                        tgt_tbars[ri].append(tbar)
                        tgt_tbar_partners[ri].append(tbar['partners'])  
            if len(tgt_tbars[ri]) == 0:
                raise Exception("Could not find this tbar")

        #print "tgt_tbar_partners=",tgt_tbar_partners
        #print rosette_i, "rosette:", rosette
        partner_body_ids = [set([a['body ID'] for a in t[0]]) for t in tgt_tbar_partners.values()]
        common_partners = set.intersection(*partner_body_ids)
        # the tbar locations
        tgt_tbar_locations = [t[0]['T-bar']['location'] for t in tgt_tbars.values()]

        if len(tgt_tbar_locations):
            # singleton, continue
            continue

        found_good = False
        for c in common_partners:
            locs = []
            # for each tbar, get the location of this partner
            for t in tgt_tbar_partners.values():
                for a in t[0]:
                    if a['body ID'] == c:
                        locs.append(a['location'])

            dists = sklearn.metrics.pairwise.euclidean_distances(np.array(locs))
            dists[np.diag_indices(len(dists))] = 1000

            # check if each one is within thold of a different one
            if (np.min(dists, axis=1) < thold).all():
                found_good = True
                break
            else:
                print "max dist was", np.max(dists)

        
        if found_good:
            pass # print "rosette_i=", rosette_i, "good"
        else:
            outfid.write("rosette_i={} bad\n".format(rosette_i))
            bad_count += 1
    if bad_count > 0:
        raise Exception("{} bad exceptions".foramt(bad_count))


if __name__ == "__main__":
    pipeline_run([load_neurons, load_synapses, filter_and_mat, 
                  create_rosette_kc_data, create_rosette_kc_mats,
                  create_rosette_kc_tensors, 
                  create_kc_kc_mats, 
                  create_kc_kc_binary_cv_params, 
                  create_all_rosette_data, 
                  create_kc_kc_tensor_binary_cv, 
                  create_kc_kc_tensor_kcsubtype_binary_cv, 
                  rosette_all_to_json], multiprocess=4)

