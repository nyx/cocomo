import numpy as np
import theano
import theano.tensor as T
from theano import pp
from scipy.spatial.distance import pdist, cdist
import sys
import cPickle as pickle
import time
import copy
import scipy.optimize
def run_sgd_full(kernel_func, log_likelihood_func,
                 xinit, theta_init, obs, obs_valid, alpha = 0.5, 
                 iters = 10000, seed = None, use_adagrad=False, 
                 logfunc = None, batch_size=None, partition_batch=False, 
                 theta_inference = True, theta_alpha= None, 
                 verbose=True, verbose_iter = 100):
    """
    SGD on full model, computing the gradient on all params. 

    code maximizes the log likelihood  

    kern_func : a function which returns a theano expression in X

    log_likelihood_func : function of observations x kernel, 
    such as the log likelihood of the poisson

    
    """
    
    N, DIM = xinit.shape

    # now the actual distance function

    X = T.dmatrix('X')
    theta = T.dvector('theta')
    observations = T.matrix('observations', dtype=obs.dtype)
    obsvalid = T.dmatrix('obsvalid')

    kern = kernel_func(X, theta)

    observation_expression = log_likelihood_func(observations, 
                                                 kern)
    obsmodel_elem = observation_expression * obsvalid
    obsmodel = T.sum(obsmodel_elem)
    obsmodel_f = theano.function([X, theta, observations, obsvalid], obsmodel, 
                                 on_unused_input='ignore')

    obsmodel_grad = T.grad(obsmodel, X)
    obsmodel_grad_f = theano.function([X, theta, observations, obsvalid], 
                                      obsmodel_grad, 
                                      on_unused_input='ignore')
    if theta_inference:
        obsmodel_grad_theta = T.grad(obsmodel, theta)
        obsmodel_grad_theta_f = theano.function([X, theta, observations, obsvalid], obsmodel_grad_theta)
    
    if seed is not None:
        np.random.seed(seed)

    if theta_alpha is None:
        theta_alpha = alpha

    grad_hists = np.zeros((N, DIM)) # for each param

    
    theta_k = copy.deepcopy(theta_init)
    
    xk = xinit.copy()
    t1 = time.time()
    runlog = []
    for k in range(iters):
             
        if batch_size is None:
            indices = [np.arange(N)]
        elif partition_batch :
            indices =  np.array_split(np.random.permutation(N), N // batch_size)
        else:
            indices =  [np.random.permutation(N)[:batch_size]]


        for idx in indices:
            obs_sub = obs[idx]
            #obs_sub = obs_sub[:, idx]
            obs_valid_sub = obs_valid[idx]
            #obs_valid_sub = obs_valid_sub[:, idx]

            grad = obsmodel_grad_f(xk[idx], theta_k, obs_sub, obs_valid_sub)
            #grad[~np.isfinite(grad)]= 0.0
            #print grad
            if not np.isfinite(grad).all():
                raise ValueError("Gradient is NaN, try reducing step size")
            if use_adagrad:
                grad_norm  = grad**2
                grad_hists[idx] += grad_norm
                alpha_k = alpha / np.sqrt(grad_hists[idx])
            else:
                alpha_k = alpha

            xk[idx] = xk[idx] + alpha_k * grad
            
        if theta_inference:
            theta_grad = obsmodel_grad_theta_f(xk, theta_k, obs, obs_valid)
            theta_k = theta_k  + theta_alpha * theta_grad

        if k % verbose_iter == 0:
            obj = obsmodel_f(xk, theta_k, obs, obs_valid)
            if verbose:
                print k, "obj=", obj, "||grad||=", np.linalg.norm(grad), "||x||=",np.linalg.norm(xk), "%3.2f" % ((time.time() - t1)/60)
            runlog.append((k, obj, time.time()-t1, xk.copy()))

    ret = {'x_est' : xk, 
           'theta_est' : theta_k, 
           'time' : time.time() - t1, 
           'runlog' : runlog}
                          
    return ret




def run_bfgs(kernel_func, log_likelihood_func,
             xinit, theta_init, obs, obs_valid, 
             iters = 1000, seed = None,
             logfunc = None, theta_inference=False, 
             theta_alpha=None, gtol=None, ftol=None, 
             maxcor = None, solve_method = 'L-BFGS-B',
             verbose=True, verbose_iter = 100):
    """
    
    """
    
    N, DIM = xinit.shape
    if theta_inference:
        THETA_DIM = len(theta_init)
    else:
        THETA_DIM = 0

    # now the actual distance function

    X = T.dmatrix('X')
    theta = T.dvector('theta')
    observations = T.dmatrix('observations')
    obsvalid = T.dmatrix('obsvalid')

    kern = kernel_func(X, theta)

    observation_expression = log_likelihood_func(observations, 
                                                 kern)
    obsmodel_elem = observation_expression * obsvalid
    obsmodel = T.sum(obsmodel_elem)
    obsmodel_f = theano.function([X, theta, observations, obsvalid], obsmodel, 
                                 on_unused_input='ignore')

    obsmodel_grad = T.grad(obsmodel, X)
    obsmodel_grad_f = theano.function([X, theta, observations, obsvalid], 
                                      obsmodel_grad, 
                                      on_unused_input='ignore')


    if theta_inference:
        obsmodel_grad_theta = T.grad(obsmodel, theta)
        obsmodel_grad_theta_f = theano.function([X, theta, observations, obsvalid], obsmodel_grad_theta)
    
    if seed is not None:
        np.random.seed(seed)

    def f(x):
        if theta_inference:
            theta_k = x[-THETA_DIM:]
            x_k = x[:-THETA_DIM].reshape(N, DIM)
        else:
            x_k = x.reshape(N, DIM)
            theta_k = theta_init

        fval = -obsmodel_f(x_k, theta_k, obs, obs_valid)
        return fval

    def gradf(x):

        if theta_inference:
            theta_k = x[-THETA_DIM:]
            x_k = x[:-THETA_DIM].reshape(N, DIM)

            g = np.zeros_like(x)
            gval = obsmodel_grad_f(x_k, theta_k, obs, obs_valid)
            gtval = obsmodel_grad_theta_f(x_k, theta_k, obs, obs_valid)
            g[:N*DIM] = gval.flatten()
            g[-THETA_DIM:] = gtval
            return -g

        else:
            x_k = x.reshape(N, DIM)
            theta_k = theta_init
            gval = obsmodel_grad_f(x_k, theta_k, obs, obs_valid)
            return -gval.flatten()            

    def logger(x):
        pass




    t1 = time.time()
    if theta_inference:
        x0 = np.zeros(N * DIM + THETA_DIM)
        x0[:N*DIM] = xinit.flatten()
        x0[-THETA_DIM:] = theta_init
    else:
        x0 = xinit.flatten()
        
    # res = scipy.optimize.fmin_l_bfgs_b(f, x0, 
    #                                    fprime=gradf,  callback=logger, 
    #                                    disp=1, maxiter=iters)

    options = {'maxiter' : iters, 
               'disp' : 10}
    if gtol is not None:
        options['gtol'] = gtol
    if ftol is not None:
        options['ftol'] = ftol
    if maxcor is not None:
        options['maxcor'] = maxcor

    optresult = scipy.optimize.minimize(f, x0, 
                                        jac = gradf, 
                                        #method='L-BFGS-B', 
                                        method=solve_method, 
                                        options = options, 
                                        callback=logger)
    
    # def basin_callback(x, f, accept):
    #     print f, accept

    # optresult = scipy.optimize.basinhopping(f, x0, 
    #                                         minimizer_kwargs = {'method' : "L-BFGS-B", 
    #                                                             'jac' : gradf}, 
    #                                         callback = basin_callback, niter=iters)
    
    x_est =  optresult.x[:N*DIM].reshape(N, DIM)
    if theta_inference:
        theta_est = optresult.x[-THETA_DIM:] 
    else:
        theta_est = theta_init
    ret = {'x_est' : x_est,
           'theta_est' : theta_est,
           'time' : time.time() - t1, 
           'runlog' : None}
                          
    return ret




if __name__ == "__main__":
    run_row()
