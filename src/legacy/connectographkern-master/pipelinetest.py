import numpy as np
import kernels
import exputil
import losses
import topogen
import sgd
import sklearn.metrics
# for latent_manifold in foo:
#     for kernel in bar:
#         for loss in baz:
                        




kern_func = kernels.gaussian(1.0) # , 3.0)


kern_f = exputil.make_kern_f(kern_func)

N = 1000
D = 2

lossobj = losses.Logistic(0.5)

#xtrue = np.random.normal(0, 1, (N, D))
xtrue = topogen.sample_sphere(N, D) + np.random.normal(0, 0.0001, (N, D))
print xtrue.shape
print kern_f(xtrue).shape
kern_val = kern_f(xtrue)
obs = lossobj.obs_forward(kern_val)


### 
reload(sgd)
reload(exputil)

ITERS = 100000    

alpha = 0.1
kfold_cv = 10
xinit = np.random.normal(0, 5, (N, D))
res = exputil.run_cv_exp(kern_func, lossobj.cost(), 
               xinit, obs, lossobj.obs_forward, kfold_cv, batch_size=40, 
               alpha=alpha, iters=ITERS, use_adagrad=True,  
               verbose=True, verbose_iter=1000)


# for ri, r in enumerate(res):
#     fpr, tpr, th = sklearn.metrics.roc_curve(r['true_vals'],
#                                              r['est_params'])
#     pylab.plot(fpr, tpr, c='k', alpha=0.3)
# pylab.legend(loc='lower right')
