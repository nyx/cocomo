
import numpy as np
from scipy.spatial.distance import cdist
import sgd
import theano
import theano.tensor as T
import topogen
import sklearn.datasets
import util
import losses
import kernels
import cPickle as pickle
import exputil
import time
import pandas as pd
import seaborn as sns
import os
import sklearn.metrics
sns.set_style('whitegrid')
from ruffus import * 
import cloudpickle
import metrics

OUTDIR = "results"

td = lambda x : os.path.join(OUTDIR, x)

def data_name_to_file(dataname):
    return "data/%s.cleandata.pickle" % dataname

def create_exp(expname, datanames):
    for dataname in datanames:
        
        infile = data_name_to_file(dataname)
        outfile = td("%s.%s.results.pickle" % (expname, dataname))
        
        yield infile, outfile, expname, dataname


EXPERIMENTS = []

def basic_gauss_params():
    for g in create_exp("basic_gauss", 
                        ["retina", "medulla", 'celegans.elec', 
                         'celegans.chem', 
                         'celegans.elec_bin', 'celegans.chem_bin']):
        yield g


@files(basic_gauss_params)
def basic_gauss(infile, outfile, expname, dataname):
    
    filebase = td("%s.%s" % (expname, dataname))
    data = pickle.load(open(infile, 'r'))
    data.keys()
    connection_matrix = (data['synapse_con_mat'] > 0).astype(float)

    obs = connection_matrix
    lossobj = losses.Logistic(0.5)
    
    ITERS = 1000
    allres = []
    exp_index = 0
    for gauss_sigma in [2.0, 4.0]:

        kern_func = kernels.gaussian(gauss_sigma) # , 1.0) # , 3.0)

        kfold_cv = 10

        N = len(obs)
        XINIT_N = 1
        for D in [2 , 4, 8, 16, 32, 48]:
            for xinit in np.random.normal(0, 1, (XINIT_N, N, D)):
                theta_init = np.ones(D)

                res = exputil.run_cv_exp_bfgs_dict(kern_func, lossobj.cost(), 
                                                   xinit, theta_init, obs, 
                                                   lossobj.obs_forward, kfold_cv, 
                                                   iters=ITERS, theta_inference=False, 
                                                   svd_xinit = True, 
                                                   verbose=False, verbose_iter=10)
                exp_index += 1

                exp_index = len(allres)
                call_filename = filebase + ".%04d.call.pickle" % exp_index
                cloudpickle.dump(res, open(call_filename, 'w'), -1)
                
                allres.append({'xinit' : xinit, 
                               'D' : D, 
                               'exp_index' : exp_index,
                               'gauss_sigma' : gauss_sigma, 
                               'call_filename' : call_filename})
    pickle.dump(allres, open(outfile, 'w'))


def synth_params():
    for g in create_exp("synth", 
                        ["block_sym", "block_asym", "latentspace_ring_2d", 
                        "synfire_1d"]):
        yield g


@files(synth_params)
def synth(infile, outfile, expname, dataname):
    
    filebase = td("%s.%s" % (expname, dataname))
    data = pickle.load(open(infile, 'r'))
    data.keys()
    connection_matrix = (data['synapse_con_mat'] > 0).astype(float)

    obs = connection_matrix
    lossobj = losses.Logistic(0.5)
    
    ITERS = 100
    allres = []
    exp_index = 0
    for gauss_sigma in [1.0, 2.0, 4.0]:

        kern_func = kernels.gaussian(gauss_sigma) # , 1.0) # , 3.0)

        kfold_cv = 10

        N = len(obs)
        XINIT_N = 1
        for D in [2 , 4, 8]: # , 16, 32, 48]:
            for xinit in np.random.normal(0, 1, (XINIT_N, N, D)):
                theta_init = np.ones(D)

                res = exputil.run_cv_exp_bfgs_dict(kern_func, lossobj.cost(), 
                                                   xinit, theta_init, obs, 
                                                   lossobj.obs_forward, kfold_cv, 
                                                   iters=ITERS, theta_inference=False, 
                                                   svd_xinit = True, 
                                                   verbose=False, verbose_iter=10)
                exp_index += 1

                exp_index = len(allres)
                call_filename = filebase + ".%04d.call.pickle" % exp_index
                cloudpickle.dump(res, open(call_filename, 'w'), -1)
                
                allres.append({'xinit' : xinit, 
                               'D' : D, 
                               'exp_index' : exp_index,
                               'gauss_sigma' : gauss_sigma, 
                               'call_filename' : call_filename})
    pickle.dump(allres, open(outfile, 'w'))




def medulla_kern_params():
    for g in create_exp("medulla_kern", 
                        ["medulla"]):
        yield g


@files(medulla_kern_params)
def medulla_kern(infile, outfile, expname, dataname):
    
    filebase = td("%s.%s" % (expname, dataname))
    data = pickle.load(open(infile, 'r'))
    data.keys()
    connection_matrix = (data['synapse_con_mat'] > 0).astype(float)

    obs = connection_matrix


    N = len(obs)
    kfold_cv = 10

    
    ITERS = 1000
    allres = []
    exp_index = 0
    for solver in ['L-BFGS-B', 'CG']:
        for sigma in [1.0, 2.0]: # , 2.0, 4.0]:
            for D in [4, 8, 16]:        
                for lossval in [0.5] : # 0.2, 0.5, 0.8]:
                    for theta_inference in [False, True]:
                        lossobj = losses.Logistic(lossval)
                        xinit = np.zeros((N, D))
                        if theta_inference:
                            theta_init = np.zeros(D)
                            kern_func = kernels.rational_quad_os(sigma, alpha=1.0)
                        else:
                            theta_init = np.zeros(D)
                            theta_init[0] = 1.0
                            kern_func = kernels.rational_quad_os(sigma, alpha=1.0) 



                        res = exputil.run_cv_exp_bfgs_dict(kern_func, lossobj.cost(), 
                                                           xinit, theta_init, obs, 
                                                           lossobj.obs_forward, kfold_cv, 
                                                           iters=ITERS, 
                                                           theta_inference=theta_inference, 
                                                           svd_xinit = True, 
                                                           verbose=False, verbose_iter=10)
                        exp_index += 1

                        exp_index = len(allres)
                        call_filename = filebase + ".%04d.call.pickle" % exp_index
                        cloudpickle.dump(res, open(call_filename, 'w'), -1)

                        allres.append({'sigma' : sigma, 
                                       'D' : D, 
                                       'lossval' : lossval, 
                                       'solver' : solver, 
                                       'theta_inference' : theta_inference, 
                                       'exp_index' : exp_index,
                                       'call_filename' : call_filename})
    pickle.dump(allres, open(outfile, 'w'))


@follows(basic_gauss, synth, 
         medulla_kern)
@transform(td("*.call.pickle"), suffix(".call.pickle"), ".res.pickle")
def run_single_exp(infile, outfile):

    indata = pickle.load(open(infile, 'r'))
    # input_matrix = pickle.load(open(indata['data'], 'r'))
    # kern_func = indata['kern_func']
    # lossobj = indata['lossobj']
    # xinit = indata['xinit']
    # iters = indata['iters']
    # theta_init = indata['theta_init']
    # theta_inference = indata['theta_inference']
    # kfold_cv = index['kfold_cv']

    t1 = time.time()
    res = exputil.run_cv_exp_bfgs(*indata[0], **indata[1])

    duration = time.time() - t1
    pickle.dump({'res' : res, 
                 'time' : duration, 
                 'callfile' : infile}, 
                open(outfile, 'w'), -1)

@transform(td("*.pickle"), 
           regex("(.\d\d\d\d).res.pickle"),
           r"\1.summary.pickle")
def compute_stats(infile, outfile):

    print infile, outfile

    data = pickle.load(open(infile, 'r'))
    res = data['res']
    resdata = []
    xvals = []
    thetavals = []
    for cvi, r in enumerate(res):
        true_vals = r['true_vals']
        est_params = r['est_params']
        if (true_vals[0] == true_vals).all(): # undefined
            roc_auc = 0
            pr_auc =0
            max_f1 = 0
        else:
            roc_auc = sklearn.metrics.roc_auc_score(true_vals, est_params)
            pr_auc = sklearn.metrics.average_precision_score(true_vals, est_params)
            max_f1 = metrics.max_fscore(true_vals, est_params)
        xvals.append(r['x_est'])
        thetavals.append(r.get('theta_est', 'None'))
        #thetavals.append(r['theta_est'])
        resdata.append({'cvi' : cvi, 
                        'max_f1' : max_f1, 
                        'roc_auc' : roc_auc, 
                        'pr_auc' : pr_auc}) 
    df = pd.DataFrame(resdata)
    df['filename'] = infile
    df['callfile'] = data['callfile']
    pickle.dump({'df' : df, 'xvals' : xvals, 'thetavals' : thetavals},
                open(outfile, 'w'), -1)

if __name__ == "__main__":
    pipeline_run([basic_gauss, run_single_exp, compute_stats], multiprocess=48)
