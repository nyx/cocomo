"""
Various basic runners and experiments for the mushroom body block model
"""

import numpy as np
import pandas as pd
import cPickle as pickle
import irm
from irm import cvpipelineutil
from ruffus import *


def test_param():
    for chain in range(10):

        yield None, "rosette_20.results.out.%d.pickle" % chain, chain

@files(test_param)
def test_run(infile, outfile, chain):
    neurons_data = pickle.load(open("data/mushroombody.neurondf.pickle", 'r'))
    synapse_data = pickle.load(open("data/mushroombody.synapsesdf.pickle", 'r'))

    rosette_data = pickle.load(open("data/rosette_20.pickle", 'r'))

    bin_mat = (rosette_data['mat'] > 0).astype(np.uint8)

    seed = chain
    np.random.seed(seed)



    N, M = bin_mat.shape
    model_name = "BetaBernoulliNonConj"
    
    init = {'domains' : 
            {'d1' : 
             {'assignment' : np.random.permutation(np.arange(N) % 50), 
              'hps' : {'alpha' : 1.0}}, 
             'd2' : 
             {'assignment' : np.random.permutation(np.arange(M) % 50), 
              'hps' : {'alpha' : 1.0}}}, 
            'relations' : 
            {'R1' : {'hps' : None}}}
    
    data = {'domains' : {'d1' : {'N' : N}, 
                         'd2' : {'N' : M}}, 
            'relations' : {'R1' : {'relation' : ('d1', 'd2'), 
                                   'model' : model_name, 
                                   'data' : bin_mat}}}
            

    HPS = {'alpha' : 1.0, 'beta' : 1.0}
    
    init['relations']['R1']['hps'] = HPS
    ITERS = 100
    slow_anneal = irm.runner.default_kernel_anneal(iterations=int(0.7*ITERS))
    
    kc ={'ITERS' : ITERS, 
         'kernels' : slow_anneal, 
         'cores' : 8}

    s = cvpipelineutil.run_exp_pure(data, init, 'anneal_slow_10',
                                    seed, kc)


    res = s['res']
    scores, state, times, _ = res
    pickle.dump({'res' : res, 
                 'seed' : seed, 
                 'chain' : chain}, 
                open(outfile, 'w'))

if __name__ == "__main__":
    pipeline_run([test_run], multiprocess=10)
