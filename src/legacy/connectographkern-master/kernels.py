import numpy as np
import theano
import theano.tensor as T


"""
notes:

http://people.seas.harvard.edu/~dduvenaud/cookbook/

"""
EPS = np.finfo(float).eps

def gaussian(sigma):
    def kern_func(X, theta):
        translation_vectors = X.reshape((X.shape[0], 1, -1)) - X.reshape((1, X.shape[0], -1))
        euclidean_dist2 = ((translation_vectors) ** 2).sum(2) 
        kern = T.exp(-euclidean_dist2/(2*sigma**2))
        return kern
    return kern_func

def gaussian(sigma):
    def kern_func(X, theta):
        translation_vectors = X.reshape((X.shape[0], 1, -1)) - X.reshape((1, X.shape[0], -1))
        euclidean_dist2 = ((translation_vectors) ** 2).sum(2) 
        kern = T.exp(-euclidean_dist2/(2*sigma**2))
        return kern
    return kern_func

def gaussian_dip(sigma1, sigma2, scale=1.0):
    def kern_func(X, theta):
        translation_vectors = X.reshape((X.shape[0], 1, -1)) - X.reshape((1, X.shape[0], -1))
        euclidean_dist2 = ((translation_vectors) ** 2).sum(2) 
        kern1 = T.exp(-euclidean_dist2/(2*sigma1**2))
        kern2 = T.exp(-euclidean_dist2/(2*sigma2**2))

        return  scale * (kern1 - kern2)
    return kern_func

def neggaussian(sigma):
    """
    1 - gaussian kernel. far away, not close, are similar
    """

    def kern_func(X, theta):
        translation_vectors = X.reshape((X.shape[0], 1, -1)) - X.reshape((1, X.shape[0], -1))
        euclidean_dist2 = ((translation_vectors) ** 2).sum(2) 
        kern = 1.0 - T.exp(-euclidean_dist2/(2*sigma**2))
        return kern
    return kern_func



def linear(sigma):
    def kern_func(X, theta):
        translation_vectors = X.reshape((X.shape[0], 1, -1)) - X.reshape((1, X.shape[0], -1))
        d = ((translation_vectors)).sum(2) 
        kern = d # T.exp(-d/(2*sigma**2))
        return kern
    return kern_func


def rational_quad(sigma, alpha):
    """
    rational quadratic kernel. See
    http://people.seas.harvard.edu/~dduvenaud/cookbook/


    """
    
    def kern_func(X, theta):
        translation_vectors = X.reshape((X.shape[0], 1, -1)) - X.reshape((1, X.shape[0], -1))
        euclidean_dist2 = ((translation_vectors) ** 2).sum(2) 
        kern = (1.0 + euclidean_dist2/(2 * sigma**2 * alpha))**(-alpha)
        return kern
    return kern_func

def periodic(sigma, p):
    """
    periodic kernel. 
    http://people.seas.harvard.edu/~dduvenaud/cookbook/


    """
    def kern_func(X, theta):
        translation_vectors = X.reshape((X.shape[0], 1, -1)) - X.reshape((1, X.shape[0], -1))
        euclidean_dist2 = ((translation_vectors) ** 2).sum(2) 
        euclidean_dist = T.sqrt(euclidean_dist2 + EPS)

        kern = T.exp(-2*T.sin(np.pi * euclidean_dist / p)**2 / sigma**2)
        return kern
    return kern_func


def gabor(g_sigma, g_psi, g_phi):
    def kern_func(X, theta):
        translation_vectors = X.reshape((X.shape[0], 1, -1)) - X.reshape((1, X.shape[0], -1))
        euclidean_dist2 = ((translation_vectors) ** 2).sum(2) 
        euclidean_dist = T.sqrt(euclidean_dist2 + 1e-9)
        kern = T.exp(-euclidean_dist/g_sigma) * T.cos(translation_vectors[:, :, 0]*g_psi + g_phi)
        return kern
    return kern_func



def inner_product():
    """
    Same as the latent eigenvalue model 
    """

    def kern_func(X, theta):
        Xp = theta * X 
        k = (X[ np.newaxis, :, :] * Xp[:, np.newaxis, :]).sum(2)
        return k
    return kern_func

def gauss_os(sigma, bindtheta=None):
    def kern_func(X, theta):
        if bindtheta is not None:
            theta = bindtheta
        translation_vectors = X.reshape((X.shape[0], 1, -1)) - X.reshape((1, X.shape[0], -1))
        d = ((translation_vectors - theta)**2).sum(2) 
        kern = T.exp(-d/(2*sigma**2))
        return kern
    return kern_func


def rational_quad_os(sigma, alpha, bindtheta=None):
    """
    rational quadratic kernel. See
    http://people.seas.harvard.edu/~dduvenaud/cookbook/


    """
    
    def kern_func(X, theta):
        if bindtheta is not None:
            theta = bindtheta
        translation_vectors = X.reshape((X.shape[0], 1, -1)) - X.reshape((1, X.shape[0], -1))
        euclidean_dist2 = ((translation_vectors - theta) ** 2).sum(2) 
        kern = (1.0 + euclidean_dist2/(2 * sigma**2 * alpha))**(-alpha)
        return kern
    return kern_func

def mixture_kernel(kernels):
    def kern_func(X, theta):
        res = 0
        for ki, k in enumerate(kernels):
            res += theta[ki] * k(X, theta)

        return res
    return kern_func

