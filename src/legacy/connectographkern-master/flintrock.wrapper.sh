#!/bin/bash

# deal with the horror that is python3 in a separate env

source activate py3k > /dev/null 2>&1
cd flintrock > /dev/null 2>&1
cd $HOME/projects/flintrock > /dev/null 2>&1 
python $HOME/projects/flintrock/flintrock.py "$@"
# --config flintrock.config.yaml describe frtest14

