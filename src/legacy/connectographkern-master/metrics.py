import numpy as np
from sklearn.metrics import f1_score

def max_fscore(true_vals, params):
    tholds = np.sort(params)[::100]
    max_f1 = 0
    for ti, t in enumerate(tholds):
        s = f1_score(true_vals, params <= t)
        print ti, t, s
        if s > max_f1:
            max_f1 = s
    return max_f1
