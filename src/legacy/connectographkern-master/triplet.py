import numpy as np
import theano
import theano.tensor as T
from theano import pp
from scipy.spatial.distance import pdist, cdist
import sys
import cPickle as pickle
import time
import copy
import scipy.optimize


import copy
import time
def run_sgd_full(kernel_func, log_likelihood_func,
                 xinit, theta_init, obs, obs_valid, alpha = 0.5, 
                 iters = 10000, seed = None, use_adagrad=False, 
                 logfunc = None, batch_size=None, partition_batch=False, 
                 theta_inference = True, theta_alpha= None, 
                 verbose=True, verbose_iter = 100):
    """
    SGD on full model for triplet-embedded losses and obs


    kern_func : a function which returns a theano expression in X

    log_likelihood_func : function of observations x kernel, 
    such as the log likelihood of the poisson

    
    """
    
    N, DIM = xinit.shape

    # now the actual distance function

    X = T.dmatrix('X')
    theta = T.dvector('theta')
    observations = T.matrix('observations', dtype=obs.dtype)
    obsvalid = T.dmatrix('obsvalid')

    kern = kernel_func(X, theta)

    observation_expression = log_likelihood_func(observations, 
                                                 kern)
    obsmodel_elem = observation_expression # * obsvalid 
    obsmodel = T.sum(obsmodel_elem)
    obsmodel_f = theano.function([X, theta, observations, obsvalid], obsmodel, 
                                 on_unused_input='ignore')

    obsmodel_grad = T.grad(obsmodel, X)
    obsmodel_grad_f = theano.function([X, theta, observations, obsvalid], 
                                      obsmodel_grad, 
                                      on_unused_input='ignore')
    
    

    
    if theta_inference:
        obsmodel_grad_theta = T.grad(obsmodel, theta)
        obsmodel_grad_theta_f = theano.function([X, theta, observations, obsvalid], obsmodel_grad_theta)
    
    if seed is not None:
        np.random.seed(seed)

    if theta_alpha is None:
        theta_alpha = alpha

    grad_hists = np.zeros((N, DIM)) # for each param

    
    theta_k = copy.deepcopy(theta_init)
    
    xk = xinit.copy()
    t1 = time.time()
    runlog = []
    for k in range(iters):


        obs_sub = obs
        obs_valid_sub = obs_valid

        grad = obsmodel_grad_f(xk, theta_k, obs_sub, obs_valid_sub)

        if not np.isfinite(grad).all():
            raise ValueError("Gradient is NaN, try reducing step size")
        if use_adagrad:
            grad_norm  = grad**2
            grad_hists += grad_norm
            alpha_k = alpha / np.sqrt(grad_hists)
        else:
            alpha_k = alpha

        xk = xk + alpha_k * grad
            
        if theta_inference:
            theta_grad = obsmodel_grad_theta_f(xk, theta_k, obs, obs_valid)
            theta_k = theta_k  + theta_alpha * theta_grad
        if k % verbose_iter == 0:
            obj = obsmodel_f(xk, theta_k, obs, obs_valid)
            if verbose:
                print k, "obj=", obj, "||grad||=", np.linalg.norm(grad), "||x||=",np.linalg.norm(xk), "%3.2f" % ((time.time() - t1)/60)
            runlog.append((k, obj, time.time()-t1, xk.copy()))

    ret = {'x_est' : xk, 
           'theta_est' : theta_k, 
           'time' : time.time() - t1, 
           'runlog' : runlog}
                          
    return ret



def studentt():
    """
    Student t kernel
    """
    def kern_func(X, theta):
        alpha = T.exp(theta[0])
        translation_vectors = X.reshape((X.shape[0], 1, -1)) - X.reshape((1, X.shape[0], -1))
        euclidean_dist2 =  1.0 + ((translation_vectors) ** 2).sum(2) / alpha
        
       
        a = -(alpha + 1)/2.0
        kern =  euclidean_dist2**a
    
        return kern
    return kern_func
        
        
class STEbase(object):
    """
    Stochastic triplet embedding with student t factored out
    
    """
    def __init__(self):
        pass
    
    def cost(self):
        def log_likelihood_func(obs, kernmat):
            term1 = kernmat[obs[:, 0], obs[:, 1]]
            term2 = kernmat[obs[:, 0], obs[:, 2]]
            #a = -(alpha + 1)/2.0

            p = (T.log(term1) - T.log(term1 + term2))
            return p

        return log_likelihood_func

    def obs_forward(self, x):
        """
        go from kernel val to observations
        """
        raise NotImplementedError()

def get_random_triplets(N, adj, obsmask, observed=True):
    triplets = []
    while len(triplets ) < N:
        i, j, k = np.random.randint(0, adj.shape[0], 3)
        if obsmask[i, j] == observed and obsmask[i, k] == observed:
            if adj[i, j] > adj[i, k]:
                triplets.append((i, j, k))
    return np.array(list(set(triplets)))


def count_pairs(adj):
    """
    How many non-zero comparisons could we possibly have? 
    """
    adj_vals = np.sort(adj.flatten())
    lastval = adj_vals[0]
    count = 0
    sum = 0.0
    for i in range(1, len(adj_vals)):
        if adj_vals[i] > lastval:
            lastval = adj_vals[i]
            count += i
        sum += count
    return sum


def split_rational_quad(sigma, alpha):
    """
    rational quadratic kernel with two dims split
    http://people.seas.harvard.edu/~dduvenaud/cookbook/

    """
    
    def kern_func(X, theta):
        D = X.shape[1]
        Dhalf = D // 2
        translation_vectors = X[:, :Dhalf].reshape((X.shape[0], 1, -1)) - X[:, Dhalf:].reshape((1, X.shape[0], -1))
        euclidean_dist2 = ((translation_vectors) ** 2).sum(2) 
        kern = (1.0 + euclidean_dist2/(2 * sigma**2 * alpha))**(-alpha)
        return kern
    return kern_func



def get_all_pairs(vals, obsmask):
    """
    For a single row, get the coordintes of all points obeying the relation
    Ignore values with obsmask=False
    
    Warning, can take a second on n=1000
    
    """
    vd = {}
    for i, v in enumerate(vals):
        if v not in vd:
            vd[v] = []
        if obsmask[i]:
            vd[v].append(i)
    unique_vals = np.sort(vd.keys())
    res = []
    for ki in range(1, len(unique_vals)):
        for j in range(ki):
            res.append((unique_vals[ki], unique_vals[j]))
    pairs = []
    # perform the lookup to get the actual coordates
    for gt, lt in res:
        for a in vd[gt]:
            for b in vd[lt]:
                pairs.append((a, b))

    # check
    for p1, p2 in pairs:
        assert vals[p1] > vals[p2]
    return np.array(pairs)


def get_all_triplets_mat(m, obsmask):
    alltuples = []
    # rows
    for ri in range(m.shape[0]):
        pairs = get_all_pairs(m[ri], obsmask[ri])
        if len(pairs) > 0:
            tuples = np.zeros((len(pairs), 3), dtype=int)
            tuples[:, 0] = ri
            tuples[:, 1:] = pairs
            alltuples.append(tuples)
    return np.vstack(alltuples)
