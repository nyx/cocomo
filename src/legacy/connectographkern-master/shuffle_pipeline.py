
import simplejson
from ruffus import * 
import os
import pandas as pd
import copy
import cPickle as pickle
import numpy as np
import sklearn.metrics, sklearn.neighbors
import scipy.sparse
import exputil
import json

DATA_DIR = "data"

def td(x):
    return os.path.join(DATA_DIR, x)




def rosette_synth_shuffle(rosette_df):
    # make sure that you only have one datasets worth of rosettes in here
    # 
    rosette_df = rosette_df.copy()
    rosette_df_out = rosette_df[['body ID_pre', 'rosette_id']].copy()
    
    # shuffling the presyn id will preserve:
    #   1. number of pre-synaptic connections for each body id
    #   2. the size of the post-synaptic thingies
    rosette_df_out['body ID_pre'] = np.random.permutation(rosette_df_out['body ID_pre'])
    assert (rosette_df_out['body ID_pre'] != rosette_df['body ID_pre']).any()
    return rosette_df_out


def shuffle_params():
    SHUFFLE_N = 500
    for shuffle_i in range(SHUFFLE_N):
        infile = td("mushroombody.all-MBON.rosette.df.pickle")
        outfile = td("mushroombody.all-MBON.rosette.shuffle_{:04d}.pickle".format(shuffle_i))
        yield infile, outfile, shuffle_i

@files(shuffle_params)
def shuffle_rosettes(infile, outfile, shuffle_i):
    all_mbon_rosettes = pickle.load(open(infile, 'r'))

    neurons_data = pickle.load(open(td("mushroombody.neurondf.pickle"), 'r'))
    neurons_df = neurons_data['neurondf']



    rosette_df = all_mbon_rosettes['rosette_df']


    pre_ids_unique = np.intersect1d(np.unique(rosette_df['body ID_pre']), neurons_df['body ID'])

    rosettes_df = rosette_df[rosette_df['body ID_pre'].isin(pre_ids_unique)]

    np.random.seed(shuffle_i)

    res = []
    for (thold, convergent_filtered, post_id), df in rosettes_df.groupby(['rosette_thold', 'convergent_filtered', 'body ID_post'], as_index=False):
        
        b = rosette_synth_shuffle(df)
        b['body ID_post'] = post_id
        b['rosette_thold'] = thold
        b['convergent_filtered'] = convergent_filtered
        b['shuffle_i'] = shuffle_i
        res.append(b)

    shuffle_df = pd.concat(res).reset_index()
    pickle.dump({'shuffle_df' : shuffle_df, 
                 'shuffle_i' : shuffle_i, 
                 'pre_ids_unique' : pre_ids_unique, 
                 'infile' : infile}, 
                open(outfile, 'w'), -1)


@transform(shuffle_rosettes, suffix(".pickle"), ".counts.pickle")
def counts(infile, outfile):
    d = pickle.load(open(infile, 'r'))
    shuffle_df = d['shuffle_df']
    pre_ids_unique = d['pre_ids_unique']
    pre_ids_to_pos = {k:v for v, k in enumerate(pre_ids_unique)}
    PREN = len(pre_ids_unique)
    MAX_ROSETTE_SIZE = 25


    res = []
    for (thold, 
         convergent_filtered, 
         post_id), df in shuffle_df.groupby(['rosette_thold', 
                                             'convergent_filtered', 
                                             'body ID_post'], as_index=False):
        
        
        conn_mat = np.zeros((PREN, PREN, MAX_ROSETTE_SIZE), dtype=np.uint16)

        for rosette_id, rosette in df.groupby('rosette_id'):
            pre_ids = np.unique(rosette['body ID_pre'])
            pos = [pre_ids_to_pos[pre_id] for pre_id in pre_ids]
            #if len(rosette) > 1
            rosette_size = len(rosette)
            for i in pos:
                for j in pos:
                    if i > j: # ignore same-same, only count once
                        conn_mat[i, j, rosette_size] += 1 

        conn_mat_count = conn_mat[:, :, 2:].sum(axis=2)
        print "conn_mat_count.shape=", conn_mat_count.shape
        for i, v in pd.value_counts(conn_mat_count.flatten()).iteritems():
            res.append({'rosette_thold' : thold,
                        'convergent_filtered' : convergent_filtered,
                        'body ID_post' : post_id, 
                        'rosette_num' : i, 'conn_pairs' : v})    
    counts_df = pd.DataFrame(res)

    pickle.dump({'counts_df' : counts_df, 
                 'infile' : infile}, 
                open(outfile, 'w'), -1)


    # shuffle_counts_by_mbon = pd.DataFrame(res)

    # pt = shuffle_counts_by_mbon.pivot(index='mbon_name', columns='rosette_num', values='conn_pairs').fillna(0)
    # pt = pt.sort_values(0).reset_index()
    # pt['shuffle_i'] = shuffle_i
    # all_shuffle_pivots.append(pt)

@merge(counts, td("mushroombody.all-MBON.rosette.shuffle.counts.pickle"))
def merge_counts(infiles, outfile):
    res = []
    for f in infiles:
       d = pickle.load(open(f, 'r'))['counts_df']
       d['filename'] = f
       res.append(d)
    alldf = pd.concat(res).reset_index()
    pickle.dump(alldf, open(outfile, 'w'), -1)


if __name__ == "__main__":
    pipeline_run([shuffle_rosettes, counts, merge_counts], multiprocess=16)

