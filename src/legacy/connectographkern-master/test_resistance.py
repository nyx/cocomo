from nose.tools import *
from networkx import * 
import resistance
import numpy as np



def test_nonsingular_pinv():
    for N in range(4, 10):
        A = np.random.normal(0, 1, (N, N))
        pinv_np = np.linalg.pinv(A)
        pinv_qrginv = resistance.qrginv(A)
        np.testing.assert_array_almost_equal(pinv_np, pinv_qrginv)

def test_singular_pinv():

    for N in range(4, 10):
        G = resistance.random_binary_tree(N)
        G = resistance.add_random_weights(G)
        A = nx.laplacian_matrix(G).todense()
        pinv_np = np.linalg.pinv(A)
        pinv_qrginv = resistance.qrginv(A)
        np.testing.assert_array_almost_equal(pinv_np, pinv_qrginv)

def test_graph_tree():
    for N in range(4, 8):
        G = resistance.random_binary_tree(N)
        G = resistance.add_random_weights(G)
    
        nx_pl = nx.all_pairs_dijkstra_path_length(G)
        distances = resistance.graph_path_distance(G)

        for n1 in nx_pl.keys():
            for n2 in nx_pl[n1].keys():
                np.testing.assert_almost_equal(distances[(n1, n2)], nx_pl[n1][n2])

def test_graph_random():
    for G in nx.nonisomorphic_trees(40):
        G = resistance.add_random_weights(G)
        
        nx_pl = nx.all_pairs_dijkstra_path_length(G)
        distances = resistance.graph_path_distance(G)
        
        for n1 in nx_pl.keys():
            for n2 in nx_pl[n1].keys():
                np.testing.assert_almost_equal(distances[(n1, n2)], nx_pl[n1][n2])


def test_graph_pinv():
    """
    Compare dijkstra for trees vs pinv
    """

    for N in range(1, 5):
        G = resistance.random_binary_tree(N)
        G = resistance.add_random_weights(G)
        node_order = G.nodes()

        nodes_to_pos = {k : v for v, k in enumerate(node_order)} 

        nx_pl = nx.all_pairs_dijkstra_path_length(G)

        # try conductivity 
        A = nx.laplacian_matrix(resistance.resistance_to_conductance(G), 
                                nodelist = node_order).todense()

        pinv_np = np.linalg.pinv(A)
        
        R = resistance.compute_resistance(pinv_np)

        for n1 in nx_pl.keys():
            for n2 in nx_pl[n1].keys():
                Rv = R[nodes_to_pos[n1], nodes_to_pos[n2]]
                Rd = nx_pl[n1][n2]
                np.testing.assert_almost_equal(Rv, Rd)


def test_graph_path_distance_fast():
    for N in range(4, 8):
        G = resistance.random_binary_tree(N)
        G = resistance.add_random_weights(G)
    
        nx_pl = nx.all_pairs_dijkstra_path_length(G)
        dist_array, n2p  = resistance.graph_path_distance_fast(G)
        distances = resistance.dist_mat_to_dict(dist_array, n2p)

        for n1 in nx_pl.keys():
            for n2 in nx_pl[n1].keys():
                np.testing.assert_almost_equal(distances[(n1, n2)], nx_pl[n1][n2])
