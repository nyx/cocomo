import matplotlib.pyplot as plt
import numpy as np
from numpy.random import default_rng

from networkx import DiGraph, adjacency_matrix

RNG = default_rng(0)

def to_graph(conn):
    g = DiGraph(conn)
    for e in g.edges:
        g.edges[e]['weight'] = 2
    second_order_edge_list = []
    for i in g.nodes:
        for j in g.neighbors(i):
            for k in g.neighbors(j):
                second_order_edge_list.append((i,k))
    for e in second_order_edge_list:
        g.add_edge(*e, weight=1)
    return g

def ring(N):
    conn = np.zeros((N,N))
    conn[range(N-1), range(1,N)] = 1
    conn[N-1,0] = 1
    th = np.linspace(0, 2*np.pi, N, endpoint=False).reshape(-1,1)
    x = np.cos(th)
    y = np.sin(th)
    p = np.hstack([x,y])
    return to_graph(conn), p

def double_ring(N):
    conn = np.zeros((N,N))
    conn[range(N-1), range(1,N)] = 1
    conn[N-1,0] = 1
    conn = conn + conn.transpose()
    th = np.linspace(0, 2*np.pi, N, endpoint=False).reshape(-1,1)
    x = np.cos(th)
    y = np.sin(th)
    p = np.hstack([x,y])
    return to_graph(conn), p

def quad_grid(N, M):
    x, y = np.meshgrid(np.arange(N), np.arange(M))
    p = np.hstack([x.reshape(-1,1), y.reshape(-1,1)])
    conn = np.zeros((N*M, N*M))
    for i in range(N):
        for j in range(M):
            if j < M-1:
                conn[j*N+i, (j+1)*N+i] = 1
            if i < N-1:
                conn[j*N+i, j*N+i+1] = 1
    return to_graph(conn), p

def quad_torus(N, M):
    th1, th2 = np.meshgrid(np.linspace(0, 2*np.pi, N, endpoint=False), np.linspace(0, 2*np.pi, M, endpoint=False))
    th1 = th1.reshape(-1,1)
    th2 = th2.reshape(-1,1)
    cx, cy = np.cos(th1), np.sin(th1)
    drx, dry, drz = .2*np.cos(th2)*cx, .2*np.cos(th2)*cy, .2*np.sin(th2)
    x, y, z = cx + drx, cy + dry, drz
    p = np.hstack([x, y, z])
    conn = np.zeros((N*M, N*M))
    for i in range(N):
        for j in range(M):
            conn[j*N+i, ((j+1)%M)*N+i] = 1
            conn[j*N+i, j*N+((i+1)%N)] = 1
    return to_graph(conn), p

def hex_grid(N, M):
    def increment():
        while True:
            yield 0, 1
            yield .5*np.sqrt(3), .5
            yield 0, 1
            yield -.5*np.sqrt(3), .5
    x = np.arange(M)*np.sqrt(3)
    y = np.zeros(M)
    x_list = [x]
    y_list = [y]
    for j, (dx, dy) in zip(range(1,N), increment()):
        x = x + dx
        y = y + dy
        x_list.append(x)
        y_list.append(y)
    p = np.vstack([np.hstack(x_list), np.hstack(y_list)]).transpose()
    conn = np.zeros((N*M, N*M))
    for i in range(N):
        for j in range(M):
            if i < N-1:
                conn[i*M+j,(i+1)*M+j] = 1
                if i%4 == 1 and j>0:
                    conn[i*M+j,(i+1)*M+j-1] = 1
                if i%4 == 3 and j<M-1:
                    conn[i*M+j,(i+1)*M+j+1] = 1
    return to_graph(conn), p

def hex_torus(N, M):
    assert(N%4 == 0)
    assert(M%2 == 1)
    def increment():
        while True:
            yield 0, 1
            yield .5*np.sqrt(3), .5
            yield 0, 1
            yield -.5*np.sqrt(3), .5
    x = np.arange(M)*np.sqrt(3)
    y = np.zeros(M)
    x_list = [x]
    y_list = [y]
    for j, (dx, dy) in zip(range(1,N), increment()):
        x = x + dx
        y = y + dy
        x_list.append(x)
        y_list.append(y)
    th1 = 2*np.pi*np.hstack(x_list).reshape(-1,1)/(M*np.sqrt(3))
    th2 = 2*np.pi*np.hstack(y_list).reshape(-1,1)/(N*.75)
    cx, cy = np.cos(th1), np.sin(th1)
    drx, dry, drz = .5*np.cos(th2)*cx, .5*np.cos(th2)*cy, .5*np.sin(th2)
    x, y, z = cx + drx, cy + dry, drz
    p = np.hstack([x, y, z])    

    conn = np.zeros((N*M, N*M))
    for i in range(N):
        for j in range(M):
            conn[i*M+j,((i+1)%N)*M+j] = 1
            if i%4 == 1:
                conn[i*M+j,((i+1)%N)*M+((j-1)%M)] = 1
            if i%4 == 3:
                conn[i*M+j,((i+1)%N)*M+((j+1)%M)] = 1
    return to_graph(conn), p

def synfire_chain(N, M):
    conn = np.block([[np.ones((M,M)) if j == i+1 else np.zeros((M,M)) for i in range(N)] for j in range(N)])
    y, x = np.meshgrid(np.arange(M), 2*np.arange(N))
    x = x.reshape(-1,1)
    y = y.reshape(-1,1)
    p = np.hstack([x,y])
    return to_graph(conn), p

def random_graph(N):
    conn = (RNG.random(size=(N,N)) > .9).astype(int)
    p = RNG.normal(size=(N,3))
    return to_graph(conn), p

def plot(graph, p):
    conn = adjacency_matrix(graph)
    if p.shape[1] == 2:
        plt.figure()
        plt.plot(p[:,0], p[:,1], 'o')
        for i, j in np.argwhere(conn):
            plt.arrow(p[i,0], p[i,1], p[j,0]-p[i,0], p[j,1]-p[i,1], length_includes_head=True)
    if p.shape[1] == 3:
        ax = plt.subplot(projection='3d')
        ax.plot(p[:,0], p[:,1], p[:,2], 'o')
        x, y, z, u, v, w = [], [], [], [], [], []
        for i, j in np.argwhere(conn):
            x.append(p[i,0])
            y.append(p[i,1])
            z.append(p[i,2])
            u.append(p[j,0]-p[i,0])
            v.append(p[j,1]-p[i,1])
            w.append(p[j,2]-p[i,2])
        ax.quiver(x, y, z, u, v, w)
    plt.axis('equal')
    plt.show()



if __name__ == '__main__':
    N = 24
    M = 12
    g, p = quad_grid(N,M)
    plot(g, p)

    N = 24
    M = 12
    g, p = quad_torus(N,M)
    plot(g, p)

    N = 18
    M = 12
    g, p = hex_grid(N,M)
    plot(g, p)

    N = 16
    M = 13
    g, p = hex_torus(N,M)
    plot(g, p)

    N = 12
    M = 8
    g, p = synfire_chain(N,M)
    plot(g, p)

    N = 25
    g, p = random_graph(N)
    plot(g, p)