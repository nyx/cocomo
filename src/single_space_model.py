import torch.nn as nn
import torch
from networkx import spectral_layout

device = 'cuda' if torch.cuda.is_available() else 'cpu'

class Model(nn.Module):
    def __init__(self, n_nodes, dim, kernel, activation):
        super().__init__()
        self.n_nodes = n_nodes
        self.dim = dim
        self.x = nn.Embedding(n_nodes, dim)
        self.kernel= kernel
        self.activation = activation

    def init_embedding(self, graph, node_mapping = None):
        if node_mapping is None:
            node_mapping = {'id_to_key':{i:i for i in range(graph.order())},'key_to_id':{i:i for i in range(graph.order())}}
        init_embedding = spectral_layout(graph, dim=self.dim)
        init_embedding = [torch.Tensor(init_embedding[node_mapping['id_to_key'][i]]) for i in range(len(graph))]
        init_embedding = torch.stack(init_embedding, dim=0)
        init_embedding = init_embedding/init_embedding.std()
        self.x.weight.data = init_embedding

    def apply_kernel(self, node1, node2):
        vec1, vec2 = self.x(node1.to(device)), self.x(node2.to(device))
        return self.kernel(vec1, vec2)
    
    def log_likelihood(self, node1, node2):
        return self.activation(self.apply_kernel(node1, node2))
    
    def center_rescale_embedding(self):
        with torch.no_grad():
            self.x.weight.data = self.x.weight.data - self.x.weight.data.mean(dim=0, keepdim=True)
            self.x.weight.data = self.x.weight.data/self.x.weight.data.std()

    def triplet_loss(self, e_pos, e_neg):
        k_pos = self.apply_kernel(*e_pos)
        k_neg = self.apply_kernel(*e_neg)
        return k_pos/(k_pos + k_neg)