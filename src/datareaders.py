import pandas as pd
from pathlib import Path
from networkx import DiGraph, draw_kamada_kawai

datadir = Path(__file__)
datadir = datadir.parents[1] / "data"

def load_data(dataset:str):
    '''
    dataset : 'connections' | 'coordinates' | 'neurons' | 'synapse_coordinates'
    '''
    df = pd.read_csv(datadir / (dataset+'.csv.gz'))
    return df

def load_graph(neuropil=None):
    '''
    Load the connections graph into a NetworkX DiGraph structure.
    neuropil : None | str
        optional filtering per neuropil
    '''
    graph = DiGraph()
    df = load_data('connections')
    if neuropil is not None:
        df = df[df['neuropil'] == neuropil]
    graph = DiGraph()
    for i, row in df[['pre_root_id', 'post_root_id', 'syn_count']].iterrows():
        u, v, w = row
        graph.add_edge(u, v, weight=w, edge_id=i)

    df = load_data('neurons')
    if neuropil is not None:
        df = df[df['neuropil'] == neuropil]
    id_to_key = df['root_id'].reset_index(drop=True).to_dict()
    key_to_id = {v:k for k,v in id_to_key.items()}
    node_mapping = {'id_to_key':id_to_key, 'key_to_id':key_to_id}
    return graph, node_mapping

if __name__ == '__main__':
    graph, _ = load_graph('LOP_R')
    print(graph.order(), graph.size())
