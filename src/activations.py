import torch.nn as nn
import torch
from spline import Spline

class ParametricActivation(nn.Module):
    def __init__(self):
        super().__init__()
        self.log_a = nn.Parameter(0)
        self.log_b = nn.Parameter(0)
        self.c = nn.Parameter(0)
        self.log_k = nn.Parameter(0)
    

    def forward(self, x):
        a = torch.exp(self.log_a)
        b = torch.exp(self.log_b)
        k = torch.exp(self.log_k)
        return 1/(1+torch.exp(k*(a/x - b/(1-x) + self.c)))